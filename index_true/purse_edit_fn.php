<?
if (isset($_POST['go_c'])){
     if (!isset($_POST[status])) $_POST[status]=2; else $_POST[status]=1;

      $array=array(
        'title'=>$_POST['title'],
        'descr'=>$_POST['descr'],
        'id_user'=>$_SESSION['user_id'],
        'value'=>$_SESSION['value'],
        'currency'=>$_POST['currency'],
        'vis'=>$_POST['status']
        );
      $id_new_purse=$db->insertdata('purse_name',$array);

      if (!empty($_POST['value_start'])){

            $array=array(
            'title'=>'',
            'date_create'=>time(),
            'id_user'=>$_SESSION['user_id'],
            'to_user'=>0,//$_POST['to_user'],
            'value_credit'=>0,
            'value_debet'=>$_POST['value_start'],
            'credit'=>0,
            'status'=>1,
            'start_date'=>time(),
            'end_date'=>time(),
            'id_purse'=>$id_new_purse,
            'id_category'=>0
            );


        $id_bill=$db->insertdata('purse_bill',$array);

        $purse->user_level_set(2); // бонус за создание записи
      }
      $_SESSION['message_type']='success';
      $_SESSION['message_icon']='fa fa-check';
      $_SESSION['message']='<b>Поздравляем!</b> Вы успешно создали кошелёк!';
      $purse->user_level_set(3);
       }

  if (isset($_POST['go_s'])){
    if (!isset($_POST[status])) $_POST[status]=2; else $_POST[status]=1;
   	$array=array(
      'title'=>$_POST['title'],
      'descr'=>$_POST['descr'],
      // 'value'=>$_SESSION['value'],
      'currency'=>$_POST['currency'],
      'vis'=>$_POST['status']
      );
      $_SESSION['message_type']='success';
      $_SESSION['message_icon']='fa fa-check';
      $_SESSION['message_success']='<b>Поздравляем!</b> Вы успешно изменили кошелёк! <a href="/">Перейти к списку кошельков</a>';
      if($_POST['reset_amount']==1){
        $info_purse=$purse->mypurse_one($_GET['id_purse']);
        $amount=$info_purse['debet']-$info_purse['credit'];

        if ($amount!==$_POST['value_start']) { /// если поле изменилось делаем сброс

          $array=array(
            'title'=>'',
            'date_create'=>time(),
            'id_user'=>$_SESSION['user_id'],
            'to_user'=>0,//$_POST['to_user'],
            'value_credit'=>$amount,
            'value_debet'=>0,
            'credit'=>1,
            'status'=>1,
            'start_date'=>time(),
            'end_date'=>time(),
            'id_purse'=>$_GET['id_purse'],
            'id_category'=>0
          );
          $id_bill=$db->insertdata('purse_bill',$array);
        if ($_POST['value_start']>0){
          $array=array(
            'title'=>'',
            'date_create'=>time(),
            'id_user'=>$_SESSION['user_id'],
            'to_user'=>0,//$_POST['to_user'],
            'value_credit'=>0,
            'value_debet'=>$_POST['value_start'],
            'credit'=>0,
            'status'=>1,
            'start_date'=>time(),
            'end_date'=>time(),
            'id_purse'=>$_GET['id_purse'],
            'id_category'=>0
          );
            $id_bill=$db->insertdata('purse_bill',$array);
        }

        }
        $_SESSION['message_type']='success';
        $_SESSION['message_icon']='fa fa-check';
        $_SESSION['message_success']='Вы успешно изменили кошелёк и сбросили сумму! <a href="/">Перейти к списку кошельков</a>';

      }

     $db->Update('purse_name',$array,' WHERE id='.$_GET['c']);
 }

   if (isset($_POST['go_d'])){

      $q="SELECT COUNT(id) FROM purse_bill WHERE id_purse={$_GET['c']} AND vis>0";
      $row=$db->getOne($q);

      if ($row>0) {
          $_SESSION['message_type']='danger';
          $_SESSION['message_icon']='fa fa-ban';
          $_SESSION['message']='<b>Ошибка!</b> Нужно удалить все записи привязанные к кошельку! <a href="/?p=purse_view&c='.$_GET['c'].'">Перейти к записям кошелька</a>'; }
      else {
       	$array=array(
            'vis'=>0
            );

          $db->Update('purse_name',$array,' WHERE id='.$_GET['c']);
          $_SESSION['message_type']='success';
          $_SESSION['message_icon']='fa fa-check';
          $_SESSION['message']='<b>Поздравляем!</b> Вы успешно удалили кошелёк! <a href="/">Перейти к списку кошельков</a>';
     }
 }
?>