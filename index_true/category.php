<?
if ($_SESSION[user_id]>0)
{

  $SmartyVar['template_full']='no'; // подгружать стили и скрипты заново или использовать готовый index
   $SmartyVar['template']='category.html';
  $smarty->assign('path_title','Мои категории');
  $smarty->assign('path_descr','Статистика и управление категориями');
  $smarty->assign('path_link','Мои категории');




  // создание или редактирование категорий


          if (isset($_POST['go_c'])){
               //if (!isset($_POST[status])) $_POST[status]=2; else $_POST[status]=1;

             	$array=array(
                  'title'=>$_POST['title'],
                  'descr'=>$_POST['descr'],
                  'icon'=>'fa-shopping-cart',
                  'color'=>$_POST['color'],
                  'id_user'=>$_SESSION['user_id'],
                  'vis'=>$_POST['status']
                  );
                $db->insertdata('purse_user_category',$array);

                $_SESSION['message_type']='success';
                $_SESSION['message_icon']='fa fa-check';
                $_SESSION['message']='<b>Поздравляем!</b> Вы успешно создали категорию!';
                 }

            if (isset($_POST['go_s'])){
                //if (!isset($_POST[status])) $_POST[status]=2; else $_POST[status]=1;

             	$array=array(
                  'title'=>$_POST['title'],
                  'descr'=>$_POST['descr'],
                  'color'=>$_POST['color'],
                  'vis'=>$_POST['status']
                  );
                $_SESSION['message_type']='success';
                $_SESSION['message_icon']='fa fa-check';
                $_SESSION['message_success']='<b>Поздравляем!</b> Вы успешно изменили категорию!';

               $db->Update('purse_user_category',$array,' WHERE id='.$_POST['id_category'].' AND id_user='.$_SESSION['user_id']);
           }

             if (isset($_POST['go_d'])){


                 	$array=array(
                      'vis'=>0
                      );

                    $db->Update('purse_user_category',$array," WHERE id={$_GET['c']} AND id_user='{$_SESSION[user_id]}'");
                    $_SESSION['message_type']='success';
                    $_SESSION['message_icon']='fa fa-check';
                    $_SESSION['message']='<b>Поздравляем!</b> Вы успешно удалили категорию!';

           }


          if (isset($_GET['c'])){

                 $q="SELECT * FROM purse_user_category WHERE id='{$_GET['c']}' AND id_user='{$_SESSION['user_id']}'";

                 $row=$db->getRow($q);
                 if (count($row)==0){
                    $_SESSION['message_type']='danger';
                    $_SESSION['message_icon']='fa fa-ban';
                    $_SESSION['message']='<b>Ошибка</b> Не верно указана категория или такой не существует!';

                 }
                 $smarty->assign('category_one', $row);

          }


    if (isset($_GET[filter_date_from]) || isset($_GET[filter_date_to])) // устанавливаем фильтр по дате
    {
        $purse->filter=1;
        $purse->filter_date_from=strtotime($_GET[filter_date_from]);
        $purse->filter_date_to=strtotime($_GET[filter_date_to]);
    }
 //   if ($_GET['c']) $purse->filter_purse=$_GET[c];  // устанавливаем кошелек - исправить на вывод нескольких кошельков

/* Для статистики категорий */
    $row=$purse->category_stats();
    $smarty->assign('category_all', $row);
/* ------------------------ */
    $row=$purse->mypurse_all();
    $smarty->assign('purse_all', $row);  // подгружаем кошельки

}
else
{
  $SmartyVar['template']='index_enter.html';
  $SmartyVar['template_full']='yes'; // подгружать стили и скрипты заново или использовать готовый index
}
?>