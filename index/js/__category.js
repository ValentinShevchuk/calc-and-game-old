<script>
$(function(){
    var thisCategory=0;
    $(".switch-toggle .loading").hide();
    
    $(".visible").click(function(){
        var thisis=$(this);
      //  var idRecord=$(thisis).data('id_category');
        $(thisis).parent().find(".loading").show();
        var visible=0;
        if ($(this).hasClass('text-muted')) { $(this).removeClass('text-muted');  visible=1; }
        else  { $(this).addClass('text-muted');  visible=2; }
        
         

        
        var param= new Array();
        param.push({ 
            'dataTd':visible, 
            'dataId':$(thisis).data('id_category'), 
             'dataTdName':'vis', 
             'table':'purse_user_category',
             'json':'1'});
        
        $.ajax({
        url: '/ajax2.php',
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
                    data: { object: 'MyPurse', action: 'AnyFieldEdit2', param: param},
        
        success: function(data)
        {
            $(thisis).parent().find(".loading").hide();
        },
        });     

    });
    $(".edit").click(function(){
        thisCategory=$(this);
        $("[name='title']").val($(this).data("title"));
        $("[name='descr']").val($(this).data("descr"));
        $("[name='id_category']").val($(this).data("id"));
        
        var icon=$(this).data("icon");
        
        $('.modal-flip-vertical').modal('show');
    });
    
    $(".click-save").click(function(){
        

        var param= new Array();
        param.push({ 
            'title': $("[name='title']").val(), 
            'descr': $("[name='descr']").val(), 
            'id': $("[name='id_category']").val(), 
            'event':'save',
             'json':'1'});
        
        $.ajax({
        url: '/ajax2.php',
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
                    data: { object: 'MyPurse', action: 'categoryEdit', param: param},
        
        success: function(data)
        {
            $('.modal-flip-vertical').modal('hide');
            $(thisCategory).data("title",$("[name='title']").val()).parent().parent().find('.category_title').html($("[name='title']").val());
            $(thisCategory).data("descr",$("[name='descr']").val()).parent().parent().find('.category_descr').html($("[name='descr']").val());
            
            //$(thisCategory).data("title",$("[name='title']").val());
        },
        
        });          
    });
});
</script>