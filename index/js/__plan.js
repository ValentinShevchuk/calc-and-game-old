<script>
Number.prototype.toMonthName = function() {
var month = ['Январь','Февраль','Март','Апрель','Май','Июнь',
'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
return month[this];
};
$(function(){
var plan_month=2, plan_year=2016; //{* ЗДЕСЬ мы грузим стартовый месяц МАРТ например*}
var CREDIT=0, DEBET=0, BALANCE=0, BILL, INDEX=0;//{* Балансы для пересчета CAPS переменные которые уходят в массив и на сервак *}

function recalc() //{* функция которая пересчитывает всю таблицу, и вносит в каждый месяц остаток*}
{
    CREDIT=0; DEBET=0;
    BALANCE=0;

    BILL = new Array();
    INDEX=0;
    
    $(".month").each(function(){
            var MONTH=$(this).find('.month-info').data('month');
            var YEAR=$(this).find('.month-info').data('year');
            CREDIT=0; DEBET=0;
            $(this).find('.month-info th').html(MONTH.toMonthName()+" "+YEAR);
                        
        $(this).find(".month-bill").each(function(){
            var CREDIT_temp=parseFloat($(this).find("[data-name='credit']").html());
            var DEBET_temp=parseFloat($(this).find("[data-name='debet']").html());
            var TITLE_temp=$(this).find("[data-name='title']").html();

            if (!isNaN(CREDIT_temp))
            { 
               if (CREDIT_temp>0) CREDIT-=CREDIT_temp; else CREDIT+=CREDIT_temp;
            }
            
            if (!isNaN(DEBET_temp))
            { 
               if (DEBET_temp>0) DEBET+=DEBET_temp; else DEBET-=DEBET_temp;
            } 
            //CREDITall--;
            if  (!isNaN(DEBET_temp) || !isNaN(CREDIT_temp)) 
            BILL.push({ 'title':TITLE_temp, 'debet':DEBET_temp,  'credit':CREDIT_temp, 'month':MONTH, 'year':YEAR});
          
        });
        
        BALANCE=BALANCE+parseFloat(DEBET+CREDIT);
        $(".month-bill").data('balance',Math.round(BALANCE,2));
  
        $(this).find('.month-bill-end td:eq(1)').html(Math.round(BALANCE,2));   
        console.log(BALANCE);
        INDEX++;
    });
    
    $(".all-the-rest").html(Math.round(BALANCE,2));
    
 
}


function recordsFieldSend(thisis, dataId, dataTd, dataTdName)
{
         /*{*
    // отправляем только одно поле после редактирования
    // срабатывает когда потерян фокус или нажат enter
    *}*/
    var dataSend="object=Jobs&action=TaskFieldEdit&json=1&dataTd="+dataTd+"&dataId="+dataId+"&dataTdName="+dataTdName;
    console.log(dataSend);
    $.ajax({
    url: '/ajax.php',
    type: 'POST',
    dataType: 'jsonp',
    jsonp: 'callback',
    jsonpCallback: "jsonpCallback",
    data: dataSend,
    
    success: function(data)
    {
        /*
        var str='';
        data['row'].forEach(function(row2, i, arr) {     
            var date_create=new Date(row2['date_create']*1000); 
            var date_update=new Date(row2['date_update']*1000);
            $("#records tbody").append( "<tr data-id='"+row2['id']+"'><td>" + row2['id']+"</td><td>" + row2['title']+"</td><td>" + row2['value_debet']+"</td> <td>" + row2['value_credit']+"</td></tr>");
                    });     
                       
         $("td").on("dblclick",function(){ recordsEdit($(this)); });
         */
        if (data['res']=='OK') $(thisis).html(dataTd); // если все ок, то все удаляется и возвращается текст письма
        if (data['res']=='NEW'){ $(thisis).html(dataTd); $(thisis).parent().data('id',data['newId']).find('td:eq(0)').html(data['newId']);}
    },
    });     
}
function recordsEdit(thisis)
{
    
     /*{*
    // редактирование одного поля
    Получаем ID поля, 
    получаем содержимое поля
    полчаем название mysql поля
    получаем тип - тип в данном случае это либо Инпут либо Дата(календарь) - решение так себе но на первое время сойдет
    
    как только поле вставилось вызывается .select чтобы вставился фокус
    
    как только фокус потерян отправлям что в поле в функцию ajax  recordsFieldSend() со всеми параметрами
    *}*/
    var dataId=thisis.parent().data('id');
    var dataTd=thisis.html();
    var dataTdName=thisis.data('name');
    var dataType=thisis.data('type');

    $(thisis).html("<input value='"+dataTd+"' class='sendToAjax col-md-11 col-xs-11 col-sm-11'>");

    $(thisis).find('input').focus().select();
    
    $('.sendToAjax').on('focusout',function(){
        dataTd=$(this).val();
        $(thisis).html(dataTd);
        recalc();
        //$(thisis).prepend("<i class='fa fa-refresh fa-spin '></i>");
        //recordsFieldSend(thisis, dataId, dataTd, dataTdName);
    });

    $('body').on('keypress','.sendToAjax',function(e){ console.log(e.which);
        if (e.which=='13') {
            dataTd=$(this).val();
            $(thisis).html(dataTd);
            recalc();
       
            //$(thisis).prepend("<i class='fa fa-refresh fa-spin '></i>");
            //recordsFieldSend(thisis, dataId, dataTd, dataTdName);  
        }
    });    
    
}   
function tdClick(thisis)
{
    /*{*
        Функция не меняется универсальная. 
        Проверяется если Input в поле нет то вызывается условие после else.
        Поле появляется только на второй клик
        dblclick не используется так как не работает на мобиле, этот метод тоже барахлит НУЖНО проверить потом
        
        как только сработал второй клик вызывается отдельная функция которая вставит поле recordsEdit($(thisis))
        *}*/
    if ($(thisis).is(':has(input)')){ }else 
            {
                var click=$(thisis).data('click');
                //console.log(click);
                if (click==0) { $("td[data-name]").data('click',0); $(thisis).data('click',1);}
                if (click==1)  { $(thisis).data('click',0); recordsEdit($(thisis)); }
            }
 
}  
$("body").on("click","td[data-name]",function(){  tdClick(this);});    

$("body").on("click", ".newRecord",function(){ 
   /* {*
    добавляем новое поле в таблицу на странице шаблона для TD 
        <tr data-id="0 ID записи">
        <tb 
            data-name="НАЗВАНИЕ поля в базе" 
            data-click="0 - ОТСЛЕЖИВАЕТ нажаите "
        >
        Там где установим название поля, будет редактироваться
       *} */      
   
    var panel='<div class="btn-group pull-right" role="group" aria-label="First group"><button type="button" class="btn btn-default btn-icon tooltip-primary newRecord" title="Новая строка"><span class="fa fa-plus"></span></button><button type="button" class="btn btn-default btn-icon tooltip-primary click-delete-record" title="Удалить запись"><i class="fa fa-times"></i></button></div>';
    
    $(this).parent().parent().parent().after("<tr class='month-bill' data-id=0><td data-name='title' data-click=0></td><td data-name='credit' data-click=0 class='text-danger'></td><td data-name='debet' data-click=0 class='text-success'></td><td>"+panel+"</td></tr>");
    // del $("td[data-name]").on("click",function(){ tdClick(this);});   
     recalc();
});

$("body").on("click", ".newMonth",function(){
    plan_month= $(".month:last").find(".month-info").data("month");
    plan_year= $(".month:last").find(".month-info").data("year");
    
    if (plan_month==11) { plan_month=0; plan_year++;} else plan_month++;
    $(".month:eq(0)").clone().appendTo("table");
    $(".month:last").find(".month-info th").html(plan_month.toMonthName()+" "+plan_year);
    $(".month:last").find(".month-info").data( 'month',plan_month);
    
    $(".month:last").find(".month-info").data("month",plan_month);
    $(".month:last").find(".month-info").data("year",plan_year );
    recalc();
});
$('body').on("click",".click-delete-record",function(){
     $(this).parent().parent().parent().hide();
});
$(".click-save-plan").click(function(){
    message('Идет сохранение...');
         $.ajax({
            url: '/ajax.php',
            type: 'POST',
            dataType: 'jsonp',
            jsonp: 'callback',
            jsonpCallback: "jsonpCallback",
            data: { object: 'MyPurse', action: 'PlanSave', json:'1', array: BILL},
            success: function(dat){
                console.log(22);
                //message('Планирование успешно сохранено');
            }
        });
});
recalc();
});
</script>