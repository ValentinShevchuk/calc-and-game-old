<script>
$(function(){
    var dataSend='';
    var direction='';
    var turn_now='down';
    var backgroundPosition='';
    var x,y,z;
    var clickDisable=0;
    var fight; //{* таймер битвы *}   
    var id_fight; //{* id текущей битвы*}
    var indicator_heart=-2;       
    var elevator=0;
    var elevator_i=-1;
    var move=false;
    $(".blood").hide();
    
LoadData();
    
function LoadData(){
           
var result=_AJAX('Game','MapLoad5x7_v2',{ 'json':'1'});
move=false;
if (result['pole_new']==1){    
    move=true;
    //$(".obj").remove();
    result['pole'].forEach(function(row, i, arr) {
        img='';
        
        // поверхность и подземелье
        if (row['y']<=0) class_='aboveground'; else  class_='underground';
        
        if (row['id_block']>0)  class_='breed '; 
        if (row['id_block']>=1 && row['id_block']<=5) class_+="block block"+row['id_block'];          
        if (row['id_block']>7) class_+="block  block"+row['id_block'];   
        
        // тип деревьев
        if (row['id_block']==6) class_+="aboveground block6";  
        if (row['id_block']==7) class_+="aboveground block7";  
        
        class_+=' xyz'+row['x']+"_"+row['y']+"_"+row['z'];
        
        // создаем блок с координатами
        if (parseFloat(row['dm'])<parseFloat(row['dm_full'])) { var str_dm="<span class='badge'>"+row['dm']+"</span>";} else { var str_dm=''; }
        
        img="<div class=' "+class_+"' data-xyz_id='"+row['id']+"'>"+str_dm+"</div>";  
        // добавляем человека
        
        //if (row['obj']==0 && row['gamer']>0) img+="<span class=' user'></span>";        //   fa fa-male    
        {if isset($smarty.get.fi4a)}if (((i>=6 && i<=8) || (i>=11 && i<=13) || (i>=16 && i<=18)) || row['y']<2) { }    else img="<div class='breed' style='background:#000'></div>";{/if}
       
        if (row['x']==row['x_min']-1 || row['x']==parseInt(row['x_max'])+1) img=img+"<img src='/img/game/tape.png' class='breed'>";

        $(".map_field:eq("+i+")").html(img);  

    }); 
    
    // добавляем пользователя
    $(".map_field:eq(12) div").append("<span class='user'></span>");  
    // поворачиваем игрока
    $(".user").css({ 'background-position':backgroundPosition});   
}

if (result['objects_new']==1){    
    $(".obj").remove();
    result['objects'].forEach(function(row2, i, arr) {

        if (row2['id_build']>0){  $(".xyz"+row2['x']+"_"+row2['y']+"_"+row2['z']).append("<span class='obj  build build1_"+row2['id_build']+"' data-id='"+row2['id_build']+"'></span>");    }
                  
        if (row2['obj']>0) $(".xyz"+row2['x']+"_"+row2['y']+"_"+row2['z']).append("<div class='breed object"+row2['obj']+" obj'></div>");
        if (row2['obj']==0 && row2['gamer']>0) $(".xyz"+row2['x']+"_"+row2['y']+"_"+row2['z']).append("<span class='user_other obj'></span>");        
        if (row2['id_monster']>0){ 

            $(".xyz"+row2['x']+"_"+row2['y']+"_"+row2['z']).append("<span class='obj  monster monster"+row2['monster_type']+"' data-id='"+row2['id_monster']+"'></span>");   
             }                 
    }); 
    
    $(".monster").parent().each(function(){ 
        var count_monster=$('.monster',this).length;
        if (count_monster>1)   $(this).append("<span class='badge obj' title='Кол-во монстров'>"+ count_monster+"</span>");
        
    });
}
if (result['inventory_new']==1){    
    
    /**/
   // $(".inv_block, .inv_object").remove(); 
    $(".inventory_shop").find("div").remove();
    $(".inv_object_fast  span").html(0); 
    $(".inventory").html(result['inventory_html']);
    result['inventory'].forEach(function(row) {
        var obj_block='';
        if (row['id_block']>0){ obj_block=' block'+row['id_block']; data_id=row['id_block'];}
        if (row['id_object']>0) { obj_block=' inv_object object'+row['id_object']; data_id=row['id_object'];}
        
     
   //    if (row['id_object']==2) $(".inventory").append("<div class='col-xs-1 btn btn-primary  breed_btn"+obj_block+"' data-id='"+data_id+"'><span class='fa fa-battery-4' style='font-size:30px; margin-top:10px; margin-left:-20px;float:left'></span><span class='badge bg-danger'>"+row['count']+"</span></div>");     else 
/*   if (row['id_object']>0) $(".inventory").append("<div class=' col-xs-1"+obj_block+"' data-id='"+data_id+" ' style='background: url(/img/game/objects/"+row['id_object']+".png);  background-size:100% 100%; width:64px; height:64px;' ><span class='badge bg-danger'>"+row['count']+"</span></div>");
   else 
       
       $(".inventory").append("<div class='col-xs-1 btn btn-primary breed_btn inv_block"+obj_block+"' data-id='"+data_id+"'><span class='badge bg-danger'>"+row['count']+"</span></div>");
*/

        
        if (row['id_object']>0){ } 
        else $(".inventory_shop").append(""+
            "<div class=' col-xs-12 col-sm-4 col-md-4 col-lg-4 no-m no-p '>"+ 
                "<div class='row'>"+
                    "<div class='col-xs-3 col-sm-4 col-md-4'>"+
                        "<div class='btn breed_btn "+obj_block+" inv_block_shop' data-id='"+data_id+"'></div>"+
                    "</div>"+
                    "<div class='col-xs-9  col-sm-8 col-md-8'>"+row['title']+" <span style='color:#000'>"+
                        row['count']+"шт</span><br><span style='color:#000'>Цена: "+row['price']+
                         "</span><br><div class='btn btn-danger light btn-xs inv_block_shop  p-l-5 p-r-5' data-id='"+data_id+"'>-1</div> "+
                        "<div class='btn btn-danger light btn-xs inv_block_shop_all p-l-5 p-r-5 ' data-id='"+data_id+"'>Всё</div>"+
                    "</div>"+
                "</div>"+
            "</div>");
        
        if (row['id_object']==1) { $(".inv_object_fast.object"+row['id_object']+" span").html(row['count']);}
                     
    }); 
        	if ($(".field_12").find('.object8').length > 0 || $(".field_12").find('.object9').length > 0) { 
        	$(".put-to-box").show(); 
        	$(".put-to-box-block").show(); 
        	//$(".remove-inventory").hide();
    	} else 
    	{ 
        	$(".put-to-box").hide(); 
        	$(".put-to-box-block").hide(); 
        	//$(".remove-inventory").show();
        }
}
if (result['indicator_new']==1){    
    $(".monitor_alert").remove();
    var str; 
    str="<span class='fa fa-battery-0 text-danger'></span>";     
    if (result['indicator']['oxygen_percent']>10) str="<span class='fa fa-battery-1 text-danger'></span>"; 
    if (result['indicator']['oxygen_percent']>25) str="<span class='fa fa-battery-2 text-warning'></span>"; 
    if (result['indicator']['oxygen_percent']>50) str="<span class='fa fa-battery-3 text-warning''></span>";
    if (result['indicator']['oxygen_percent']>75) str="<span class='fa fa-battery-4 text-success'></span>";
    if (result['indicator']['oxygen_percent']<0)result['indicator']['oxygen_percent']=0;
    $(".indicator_oxigen").html(str+" <span style='color:#fff'>"+result['indicator']['oxygen_percent']+"%</span>");
    
    if (result['indicator']['oxygen_percent']<20) $(".map_field:eq(12) div").append("<span class='fa fa-battery-1 text-danger monitor_alert'></span>");
    
    str="<span class='fa fa-heart text-danger'></span>";  
    if (result['indicator']['heart_percent']>5) str="<span class='fa fa-heart text-danger'></span>"; 
    if (result['indicator']['heart_percent']>25) str="<span class='fa fa-heart text-warning'></span>"; 
    if (result['indicator']['heart_percent']>50) str="<span class='fa fa-heart text-warning'></span>";
    if (result['indicator']['heart_percent']>75) str="<span class='fa fa-heart text-success'></span>";
    $(".indicator_heart").html(str+" <span style='color:#fff'>"+result['indicator']['heart_percent']+"%</span>");   
    
    if ((indicator_heart>result['indicator']['heart'] && indicator_heart>-1)){
        $('.blood').show();
        var blood=setInterval(function () { $('.blood').hide(); clearInterval(blood);   }, 1000);

    }
    indicator_heart=result['indicator']['heart']; 
   $(".indicator_money").html("<span class='fa fa-btc text-primaty'></span><span> "+result['indicator']['money']+"</span>"); 
   $(".indicator_online").html("<span class='fa fa-users'></span><span style='color:#fff'> "+result['indicator']['online']+"</span>");   
   
    str="<span class='fa fa-shopping-basket text-success'></span>";     
    if (result['indicator']['inventory_percent']>10) str="<span class='fa  fa-shopping-basket text-success'></span>"; 
    if (result['indicator']['inventory_percent']>50) str="<span class='fa  fa-shopping-basket text-warning''></span>";
    if (result['indicator']['inventory_percent']>75) str="<span class='fa  fa-shopping-basket text-danger'></span>";
    if (result['indicator']['inventory_percent']<0) result['indicator']['inventory_percent']=0;
    $(".indicator_inventory").html(str+" <span style='color:#fff'>"+result['indicator']['inventory']+"/"+result['indicator']['inventory_full']+"</span>");   
    if (parseFloat(result['indicator']['inventory_full'])<=parseFloat(result['indicator']['inventory'])) $(".indicator_inventory").css('background-color',"#ff0000"); else $(".indicator_inventory").css('background-color',"");
        //console.log(str);
    //message(str);
}
if (result['message_new']>0) { 
    $(".badge-chat").html(result['message_new']);
    $(".badge-chat").show();
} else  $(".badge-chat").hide();
//if (result['error']==0) LoadData();
  
}
setInterval(function () {     LoadData();}, 5000);

function UserTurn(thisis, direction)
{
        if (direction=='left'){ backgroundPosition=' 0px -42px'; $(thisis).css({ 'background-position':backgroundPosition}); } 
        if (direction=='right'){ backgroundPosition=' 0px -82px'; $(thisis).css({ 'background-position':backgroundPosition});} 
        if (direction=='up'){ backgroundPosition=' 0px -120px'; $(thisis).css({ 'background-position':backgroundPosition}); }
        if (direction=='down'){ backgroundPosition='0px 0px'; $(thisis).css({ 'background-position':backgroundPosition}); }
}

function walk()
{
    var load="<span class='fa fa-gavel fa-spin'style=' color:#f1f0f0;font-size:50px; position: absolute; z-index: 4'></span>";
    
    var xyz_id=0;
    
    if (direction=='up') {        
        if($(".map_field:eq("+(12-5)+")") .find("div").is(".breed:not(.obj)")) {
            $(".map_field:eq("+(12-5)+")").append(load);
            xyz_id=$(".map_field:eq(2)").find(".block11").attr('data-xyz_id');
            badge=parseFloat($(".field_7").find("span.badge").html());
        }
        
             
    }
    if (direction=='down') {  
        if($(".map_field:eq("+(12+5)+")").find("div").is(".breed:not(.obj)")) $(".map_field:eq("+(12+5)+")").append(load); 


        
    }
    if (direction=='left') {      
        if($(".map_field:eq("+(12-1)+")").find("div").is(".breed:not(.obj)"))
        { 
            $(".map_field:eq("+(12-1)+")").append(load); 
            
            xyz_id=$(".map_field:eq(6)").find(".block11").attr('data-xyz_id');
            badge=parseFloat($(".field_11").find("span.badge").html());
          if (elevator_i>-1) clearInterval(elevator);
        }
    }
    if (direction=='right'){     
        if($(".map_field:eq("+(12+1)+")") .find("div").is(".breed:not(.obj)")){
        $(".map_field:eq("+(12+1)+")").append(load);
        xyz_id=$(".map_field:eq(8)").find(".block11").attr('data-xyz_id');
        badge=parseFloat($(".field_13").find("span.badge").html());
        }
        if (elevator_i>-1) clearInterval(elevator);
    }                


    $.ajax({
        url: '/ajax.php',
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        data: dataSend,
        
        success: function(data)
        {
           // map_reload(data);
            if (data['message']!=0)  message(data['message']);
            $(".indicator_compass").html("<span class='fa fa-compass  text-primaty' style=' '></span> "+data['x']+":"+data['y']);
            $(".fa-gavel").remove();
            //x=data['x']; y=data['y'];
            clickDisable=0;
            LoadData();
            if (xyz_id>0)if (move==true){
            message('Осторожно, падение булыжника');
            var DropBlock= setInterval(function(){
            var result=_AJAX('Game','DropBlock',{ json:1, 'xyz_id':xyz_id });
            if (result['error']==0) { LoadData(); }
            clearInterval(DropBlock);
        }, 4000);
    }
        },
    });       
} 
function PreWalk(direction_){
    
        direction=direction_;
    if (direction==turn_now && clickDisable==0){ 
        clickDisable=1;    
        //console.log(bill);
        dataSend="object=Game&action=walk&direction="+direction+"&json=1";
        walk();
    } 
    else  
    { 
        turn_now=direction; 
/*        
        if (direction=='left'){ backgroundPosition=' 0px -72px'; $(".user").css({ 'background-position':backgroundPosition}); } 
        if (direction=='right'){ backgroundPosition=' 0px -144px'; $(".user").css({ 'background-position':backgroundPosition});} 
        if (direction=='up'){ backgroundPosition=' 0px -215px'; $(".user").css({ 'background-position':backgroundPosition}); }
        if (direction=='down'){ backgroundPosition='0px 0px'; $(".user").css({ 'background-position':backgroundPosition}); }
*/
UserTurn('.user', direction);
}
}
$(".walk, .map_field:eq(11), .map_field:eq(13), .map_field:eq(7), .map_field:eq(17) ").click(function(){
    
PreWalk($(this).data('direction'));
    
});

$(".click-set-stairs").click(function(){
    
    var result=_AJAX('Game','SetObject',{ json:1, id_object:1, direction:turn_now });
    if (result['error']==0) { 
        //$(".map_field:eq(12)").append("<div class='breed stairs obj'></div>");
        $('#inventory').modal('hide');
        LoadData();
        }
});


$('body').on('click', '.inv_block_shop', function(){
    var id_block=$(this).data('id');

        var result=_AJAX('Game','SellBlock',{ json:1, id_block:id_block });  LoadData(); RefreshShop();
        //if (result['error']==0){ LoadData(); } //$('#inventory').modal('hide');       

});

$('body').on('click', '.inv_block_shop_all', function(){
    var id_block=$(this).data('id');
    var result=_AJAX('Game','SellBlock',{ json:1, id_block:id_block, all:1 }); LoadData(); RefreshShop();
        //if (result['error']==0){ LoadData(); } //$('#inventory').modal('hide');       

});


$('body').on('click', '.inv_block', function(){
    var id_block=$(this).data('id');

        var result=_AJAX('Game','SetBlock',{ json:1, id_block:id_block, direction:turn_now, x:x, y:y  });
        if (result['error']==0){  $('#inventory').modal('hide'); LoadData(); }

});

$('body').on('click', '.inv_object,  .inv_object_fast', function(){
    var shop=$(this).parent().data('shop');
    var id_object=$(this).data('id');
    var id_record=$(this).data('id_record');
        var result=_AJAX('Game','UseObject',{ json:1, id_object:id_object, direction:turn_now, x:x, y:y, id_record: id_record  });
        $('#inventory').modal('hide'); 
        if (result['error']==0){ LoadData(); 
        if (id_object==6){
            var boom = setInterval(function(){
                var result=_AJAX('Game','Boom',{ json:1 });
                if (result['error']==0) { LoadData();  $(".map").effect( 'bounce',  500 );}
                clearInterval(boom);
                
            }, 4000);
         }              
        }

});
$('body').on('click', '.use-magic', function(){

    var id_object=$(this).data('id');
    var id_record=$(this).data('id_record');
    var magic=$(this).data('magic');   
    
    var result=_AJAX('Game','UseMagic',{ json:1, id_object:id_object, id_record: id_record, magic: magic});
    if (result['error']==0){ LoadData();}

});
$('body').on('click', '.get-out-box', function(){
    var id_object=$(this).data('id');
    var id_record=$(this).data('id_record');  
    var all=$(this).data('all');  
        var result=_AJAX('Game','GetObjectOutChest',{ json:1, id_object:id_object, id_record: id_record, all:all });
       // $('#inventory').modal('hide'); 
        if (result['error']==0){  $(".chest-open").click(); LoadData();  }

}); 
$('body').on('click', '.get-out-box-block', function(){
    var id_block=$(this).data('id');
    var all=$(this).data('all');  
    var result=_AJAX('Game','GetBlockOutChest',{ json:1, id_block:id_block, all: all });
       // $('#inventory').modal('hide'); 
    if (result['error']==0){  $(".chest-open").click(); LoadData();   
}

});  
$('body').on('click', '.put-to-box', function(){
    var id_object=$(this).data('id');  
    var id_record=$(this).data('id_record');  
    var all=$(this).data('all');  
    var result=_AJAX('Game','PutObjectToChest',{ json:1, id_object:id_object, id_record:id_record, all: all });
    // $('#inventory').modal('hide'); 
    if (result['error']==0){ LoadData();     }

});
$('body').on('click', '.put-to-box-block', function(){
    var id_block=$(this).data('id');
var all=$(this).data('all');  
        var result=_AJAX('Game','PutBlockToChest',{ json:1, id_block:id_block, all: all  });
       // $('#inventory').modal('hide'); 
        if (result['error']==0){ LoadData();    
              }

});
$('body').on('click', '.remove-inventory', function(){
    if (clickDisable==0){ 
        clickDisable=1;   
    var id_object=$(this).data('id_object');
    var id_block=$(this).data('id_block');
    var all=$(this).data('all');
    var result=_AJAX('Game','RemoveItemInventory',{ json:1, id_object:id_object, id_block: id_block, all:all });
    // $('#inventory').modal('hide'); 
    if (result['error']==0){ LoadData(); clickDisable=0;}
    }
}); 

$("body").on('click','.click-take',function(){
    
    var xyz_id=0;    
    var elevator_win=0;
    if ($(".field_12").find('.object10').length > 0 ) { 
        
        var result=_AJAX('Game','GetElevatorInfo',{ json:1});
        if ( result['error']==0){ 
            $(".elevator_list").html('');
            result['list'].forEach(function(row, i, arr) {
                $(".elevator_list").append("<div class=' btn btn-primary elevator-go col-xs-12' data-y='"+row['y']+"'>"+row['y']+"-й этаж</div>");
            });
            $('#elevator').modal('show'); 
        }
        
    } 
    else{
        //message('Осторожно, падение булыжника');
        if (direction=='up') {  xyz_id=$(".field_7").find(".block11").attr('data-xyz_id'); }
        if (direction=='left') {   xyz_id=$(".field_6").find(".block11").attr('data-xyz_id');}
        if (direction=='right'){  xyz_id=$(".field_8").find(".block11").attr('data-xyz_id');}
    
        if (xyz_id>0){
            message('Осторожно, падение булыжника');
            var DropBlock= setInterval(function(){
                var result=_AJAX('Game','DropBlock',{ json:1, 'xyz_id':xyz_id });
                if (result['error']==0) { LoadData(); }
                clearInterval(DropBlock);
            }, 4000);
        }    
        var result=_AJAX('Game','TakeObject',{ json:1, direction:turn_now});  
        LoadData();
    
    }


});
$("#fight").on('hidden.bs.modal', function (e) {
$("#fight_info").html("<span class='fa fa-spin fa-spinner'></span>");
    clearInterval(fight);  
});
$("body").on('click','.click-kick',function(){
    if (clickDisable==0){ 
        clickDisable=1;   
    //var eq_div=0;
    //if (turn_now=='left') eq_div=11;
    //$(".map_field:eq("+eq_div+")").find(".monster").addClass("monster_kick");
    var result=_AJAX('Game','KickToMonster',{ json:1, direction:turn_now}); 
    if (result['error']>=0) { 
        //   $(".map_field:eq("+eq_div+")").find(".monster").removeClass("monster_kick"); 
        clickDisable=0;  LoadData();
        if (result['open']==1){ 
            id_fight=result['id_fight'];
            fight=setInterval(function () {
                var res_fight=_AJAX('Game','GetFightPlayerList',{ json:1, html:1, id_fight:result['id_fight']}); 
                if ( res_fight['error']==0) $("#fight_info").html(res_fight['html']); 
                if ( res_fight['error']==1) { 
                    $('#fight').modal('hide');   
                    clearInterval(fight);  
                } 
            }, 1000);
            $('#fight').modal('show');                
        }
    }
     
}
});

$("body").on('click','.fight-kick-monster',function(){
    message(1);
    if (clickDisable==0){ 
        clickDisable=1; 
        //$(this).html('<span class="fa fa-spin fa-spinner"></span>');
        var result=_AJAX('Game','KickMonsterInFight',{ json:1, id_monster: $(this).data('id_monster'), id_fight:id_fight});         
        if (result['error']==0){ clickDisable=0; }
    }
});


$("body").on('click','.click-object-buy',function(){
    var result=_AJAX('Game','BuyObject',{ json:1, id_object: $(this).data('id')});  LoadData(); 
});

$("body").on('click','.emergency_call',function(){
    var result=_AJAX('Game','EmergencyCall',{ json:1}); 
    if (result['error']==0){ $(".emergency_price").html(result['emergency_price']);  $('#inventory').modal('hide');  LoadData(); }
});

$("html").keydown(function(e){
    if (chat_show==0){
        if(e.keyCode== 65){ PreWalk('left');}
        if(e.keyCode== 68){ PreWalk('right');}
        if(e.keyCode== 87){ PreWalk('up');}
        if(e.keyCode== 83){ PreWalk('down');}
        
        if(e.keyCode== 81){ $(".click-kick").click();} 
        if(e.keyCode== 69){ $(".click-take").click();}  //   message(e.keyCode);
    }
});
$(".win-open").on('click',function(){
	if ($(".field_12").find('.object8').length > 0 || $(".field_12").find('.object9').length > 0) { 
    	$(".put-to-box").show(); 
    	$(".put-to-box-block").show(); 
    	//$(".remove-inventory").hide();
	} else 
	{ 
    	$(".put-to-box").hide(); 
    	$(".put-to-box-block").hide(); 
    	//$(".remove-inventory").show();
    }
//$(".field_11").find('.object8');
//console.log($(this).data('target'));
   // $("#inventory").modal('close');
$($(this).data('window')).modal('show');
    $($(this).data('window')).on('hidden.bs.modal', function (e) {
        $("#inventory").modal('hide');
})

    $('#Tabs a:first').tab('show');
    //message('проверка');
});

$('body').on('click', '.click-menu', function(){
    $('#menu').modal('show'); 

});
$('body').on('click', '.click-menu-item', function(){
    $('#menu').modal('hide'); 
    var win=$(this).data('window');
    $(win).modal('show'); 

});


//------------ help
{if $gamer_info.help>0}
$('#help').modal('show'); 
{if $gamer_info.help==2}$('#TabsHelp a:last').tab('show');{/if}
{/if} 
//-----------------
// --------- task
function GetTaskEasyList()
{
    var result=_AJAX('Game','GetTaskEasyList',{ json:1 });
    if (result['error']==0) $(".task-list").html(result['html']);
    //else GetTaskEasyList();
}
$('#task').on('shown.bs.modal', function(){
    GetTaskEasyList();
});
/**/
$("body").on('click',".click-task-accept", function(){
    var id_task=$(this).parent().data('id_task');
    var result=_AJAX('Game','AcceptTaskEasy',{ json:1, id_task: id_task });
    if (result['error']==0) GetTaskEasyList();
});
$("body").on('click',".click-task-reset", function(){
    var id_task=$(this).parent().data('id_task');
    var result=_AJAX('Game','ResetTaskEasy',{ json:1, id_task: id_task });
    if (result['error']==0) GetTaskEasyList();
});
$("body").on('click',".click-task-done", function(){
    var id_task=$(this).parent().data('id_task');
    var id_type=$(this).parent().data('id_type');
    var result=_AJAX('Game','DoneTaskEasy',{ json:1, id_task: id_task, id_type: id_type });
    if (result['error']==0) GetTaskEasyList();
});
//------
//----stat
$("body").on('click',".indicator_online, a[href='#stat']", function(){
    $("#stat .modal-dialog .modal-content").html('<span class="fa fa-spin fa-spinner"></span>');
    var result=_AJAX('Game','GetStatAll',{ json:1 });
    if (result['error']==0) $("#stat .modal-dialog .modal-content").html(result['stat_html']);
});
//------
$("body").on('click','.chest-open', function () {
   
    var result=_AJAX('Game','GetChestObjectList',{ json:1 });
    if (result['error']==0) $("#chest").html(result['inventory_html']);
    else  $("#chest").html('Пусто либо нет доступа, воспользуйтесь отмычкой');
})
$("body").on('click','.elevator-go', function () {
    var floor=$(this).data('y');
    var result=_AJAX('Game','WalkElevator',{ json:1, floor: floor }); 
    if (result['error']==0) { LoadData();  $('#elevator').modal('hide');  }    
});
/*
$("body").on('click','.elevator-down', function () {
    if (elevator_i==-1)
    if ($(".field_12").find('.object10').length > 0 ) {                        
        elevator_i=0;
        elevator=setInterval(function(){            
            elevator_i++;   
            $(".elevator-floor").html(elevator_i);
            if (elevator_i==20) {  
                var result=_AJAX('Game','WalkElevator',{ json:1, direction:2 }); 
                if (result['error']==0) { LoadData(); $('#elevator').modal('hide');  }
                clearInterval(elevator); 
                elevator_i=-1;
            }
            
        }, 500);
    
}    
});
*/ 
//-----chat

var interval_getNewMessage=0;
var chat_show=0;
$(".click-chat").click(function(){
 
    if (chat_show==0) $(".loading-wrap").show();
    $('#chat').modal('show'); 
     var result=_AJAX('Game','SetPVPReady',{ json:1,value:1 });
     
    if (chat_show==0) interval_getNewMessage=setInterval(function(){            
        var chat=_AJAX('Game','GetMessageList',{ json:1}); 
        if (chat['error']==0) { $(".chat-items").prepend(chat['html']);  $(".loading-wrap").hide();} 
        if (chat['error']==1) {  $(".loading-wrap").hide();} 
        }, 1000); 
  chat_show=1;
 });
 
$('#chat').on('hidden.bs.modal', function (e) {
      clearInterval(interval_getNewMessage);
      chat_show=0;
       var result=_AJAX('Game','SetPVPReady',{ json:1,value:0 });
       $('.pvp-ambush').html("<span class='fa fa-hand-rock-o   fa-2x'></span> Воевать</a>"); $('.pvp-ambush').data('ambush',0); 
});

$(".click-message").click(function(){
    var text=$("[name='message']").val();
    $("[name='message']").val('');
    if (text.length>0){ 
        var chat=_AJAX('Game','CreateMessage',{ json:1, text:text});     
        if (chat['error']==0) $(".click-chat").click();
    } else message('Нельзя отправить пустую строку');
});

$("[name='message']").keydown(function(e){
    if(e.keyCode== 13){ $(".click-message").click(); }
});

$("body").on('click','.inventory-view',function(){
    $(".item-info").toggle();
});
//------    

//-------shop

function RefreshShop(){
    var result=_AJAX('Game','RefreshShop',{ json:1 });
    if (result['error']==0){
        $(".modal-content.shop").html(result['html']);
  
    }
      
}
$('#shop').on('shown.bs.modal', function(){
    RefreshShop();
});

//-------------
//pvp ----
var pvpTarget=0;
$("body").on('click','.pvp-ambush', function () {   
    var value=$(this).data('ambush');
    var result=_AJAX('Game','SetPVPAmbush',{ json:1, value: value});
    
    if (result['error']==0) {
        if (value==1){ $(this).html("<span class='fa fa-hand-rock-o   fa-2x'></span> Воевать</a>"); $(this).data('ambush',0); }
        else{ $(this).html("<span class='fa fa-home   fa-2x'></span> Спрятаться</a>"); $(this).data('ambush',1);}
    }
})

$("body").on('click','.pvp-attack', function () {   
    message('Атака!');
    var result=_AJAX('Game','AttackPVP',{ json:1, target:  pvpTarget});
    
    if (result['error']==0) {
    }
})

$("body").on('click','.pvp-target', function () {   
    if (pvpTarget==$(this).data('id_user'))  { 
        message('Цель снята'); 
        $(".pvp-attack").html("<span class='fa fa-bomb fa-2x'></span> Бросить бомбу");
    }
    else 
    { 
        pvpTarget=$(this).data('id_user');  message('Цель выбрана');
        $(".pvp-attack").html("<span class='fa fa-bomb fa-2x'></span> Бросить бомбу в #"+pvpTarget);
    }
    
   
})
//----------

});

function _AJAX(_object, _action, _param){
    _param['json']='1';       
    var res;
        $.ajax({
        url: '/ajax2.php',
        async:false,
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
                    data: { object: _object, action: _action, param: _param},
        
        success: function(data)
        {  
          //  alert(data['res']);
            //return data;

            if ("msg" in data)message(data['msg']);
            if ("error_msg" in data)message(data['error_msg']);
            res=data;
        },
    });  
    return res;  
}

 $(function()  {
    $('.carousel').carousel();
    
});      
</script>