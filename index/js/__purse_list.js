<script>
$(function(){
    $(".switch-toggle .loading").hide();

    $(".visible").change(function(){
        var thisis=$(this);
        var idRecordPurse=$(thisis).data('id_purse');
        $(thisis).parent().find(".loading").show();
        var visible=0;
        if ($(this).prop('checked')) visible=1;
        else  visible=2;
        
        var dataTd=visible, dataId= idRecordPurse, dataTdName='vis'; 
        
        var dataSend="object=MyPurse&action=purseFieldEdit&json=1&dataTd="+dataTd+"&dataId="+dataId+"&dataTdName="+dataTdName;
        
       // console.log(dataSend);
        
        $.ajax({
        url: '/ajax.php',
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        data: dataSend,
        
        success: function(data)
        {
            $(thisis).parent().find(".loading").hide();
        },
        });     

    });
});
</script>