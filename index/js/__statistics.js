<script>
    {*     $(function()  {

        //Theme Setting, change color of charts
        var mainColor = '#4E8EF7';
        var secondaryColor = '#34495e';
        
        initChart();

        function initChart()  {           
            
{if isset($smarty.get.type)} 
          var data = [{ data: [{foreach from=$bill_all item=row name=foo}[{$smarty.foreach.foo.index+1}, {$row[1]}],{/foreach}],
            label: "Доходы "
          },
            { data: [{foreach from=$bill_all item=row name=foo}[{$smarty.foreach.foo.index+1}, {$row[2]}],{/foreach}],
            label: "Расходы"
          }],

          options = { 
            xaxis: {
              ticks: [[0,''],{foreach from=$bill_all item=row name=foo}['{$smarty.foreach.foo.index+1}', '{$row[0]}'],{/foreach}]
            },

            series: {
				bars: {
					show: true,
					barWidth: 0.6,
					align: "center"
				},
              shadowSize: 0
            },
            grid: {
              hoverable: true,
              clickable: true,
              color: '#bbb',
              borderWidth: 1,
              borderColor: '#eee'
            },
            colors: [mainColor,secondaryColor]
          },
          plot;
{else}        
          var data = [{ data: [{foreach from=$bill_all item=row name=foo}[{$smarty.foreach.foo.index+1}, {$row[1]}],{/foreach}],
            label: "Доходы "
          },
            { data: [{foreach from=$bill_all item=row name=foo}[{$smarty.foreach.foo.index+1}, {$row[2]}],{/foreach}],
            label: "Расходы"
          }],
          options = { 
            xaxis: {
              ticks: [{foreach from=$bill_all item=row name=foo}['{$smarty.foreach.foo.index+1}', '{$row[0]}'],{/foreach}]
            },

            series: {
              lines: {
                show: true, 
              },
              points: {
                show: true,
                radius: '3.5'
              },
              shadowSize: 0
            },
            grid: {
              hoverable: true,
              clickable: true,
              color: '#bbb',
              borderWidth: 1,
              borderColor: '#eee'
            },
            colors: [mainColor,secondaryColor]
          },
          plot;
{/if}  
          plot = $.plot($('.line-placeholder'), data, options);


          $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #95a4b8",
            padding: "4px",
            "font-size": "12px",
            color: "#fff",
            "border-radius": "4px",
            "background-color": "#95a4b8",
            opacity: 0.90
          }).appendTo("body");

          $(".chart-placeholder").bind("plothover", function (event, pos, item) {
            var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
            $("#hoverdata").text(str);
          
            if (item) {
              var x = item.datapoint[0],
                y = item.datapoint[1];
              
                $("#tooltip").html(item.series.label+ " : " + y)
                .css({ top: item.pageY+5, left: item.pageX+5})
                .fadeIn(200);
            } else {
              $("#tooltip").hide();
            }
          });
        }
});
*}
$(function () {
    $('#graph').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Расходы и доходы в период {if ($smarty.get.filter_date_from!='')} с {$smarty.get.filter_date_from}{/if}  {if ($smarty.get.filter_date_to!='')} по {$smarty.get.filter_date_to}{/if} '
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [{foreach from=$bill_all item=row name=foo} '{$row[0]}',{/foreach}],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Валюта: рубли',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' руб'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Доходы',
            data: [{foreach from=$bill_all item=row name=foo}{$row[1]},{/foreach}]
        }, {
            name: 'Расходы',
            data: [{foreach from=$bill_all item=row name=foo}{$row[2]},{/foreach}]
        }]
    });
});
</script>        