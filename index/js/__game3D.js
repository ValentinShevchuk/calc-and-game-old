<script>
$(function(){
    var dataSend='';
    var direction='';
    var turn_now='down';
    var backgroundPosition='';
    var x,y,z;
    var clickDisable=0;
    

    
    LoadData();
    
function LoadData(){
           
var result=_AJAX('Game','MapLoad5x7_v2_3D',{ 'json':'1'});

if (result['pole_new']==1){    
    
    //$(".obj").remove();
    result['pole'].forEach(function(row, i, arr) {
        img='';
        
        // поверхность и подземелье
       // if (row['y']<=0) class_='aboveground'; else  class_='underground';
        
        if (row['id_block']>0)  class_='breed '; 
        if (row['id_block']>=1 && row['id_block']<=5) class_+="  block"+row['id_block'];          
        if (row['id_block']>=10) class_+="  block"+row['id_block'];   
        
        // тип деревьев
        if (row['id_block']==6) class_+=" block6";  
        if (row['id_block']==7) class_+=" block7";  
        
        // создаем блок с координатами
        img="<div class='"+class_+"' data-x='"+row['x']+"' data-y='"+row['y']+"' data-z='"+row['z']+"' dm='"+row['dm']+"'></div>";  
        // добавляем человека
        
        //if (row['obj']==0 && row['gamer']>0) img+="<span class=' user'></span>";        //   fa fa-male    
        {if isset($smarty.get.fi4a)}if (((i>=6 && i<=8) || (i>=11 && i<=13) || (i>=16 && i<=18)) || row['y']<2) { }    else img="<div class='breed' style='background:#000'></div>";{/if}
        $(".map_field:eq("+i+")").html(img);  

    }); 
    
    // добавляем пользователя
    $("[data-x][data-y]:eq(32)").append("<span class='user'></span>");  
    // поворачиваем игрока
    $(".user").css({ 'background-position':backgroundPosition});   
}

if (result['objects_new']==1 && 1==2){    
    $(".obj").remove();
    result['objects'].forEach(function(row2, i, arr) {
          if (row2['id_build']>0){   $("[data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").append("<span class='obj  build build1_"+row2['id_build']+"' data-id='"+row2['id_monster']+"'></span>");    }
        
        if (row2['obj']==1) $("[data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").append("<div class='breed stairs obj'></div>");
        if (row2['obj']==0 && row2['gamer']>0) $("[data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").append("<span class='user_other obj'></span>");        
        if (row2['id_monster']>0){ 
            var object_count=parseInt($("[data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").data('object-count'))|| 0;  object_count++; //console.log(object_count);
            $("[data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").attr('data-object-count',object_count);
            if (object_count==1) $("[data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").append("<span class='obj  monster monster"+row2['monster_type']+"' data-id='"+row2['id_monster']+"'></span>");    }                 
    }); 
    
    $("[data-object-count]").each(function(){
        if ($(this).attr('data-object-count')>1)
         $(this).append("<span class='badge obj' title='Кол-во монстров'>"+$(this).attr('data-object-count')+"</span>");
        
    });
}
if (result['inventory_new']==1){    
    $(".inv_block, .inv_object").remove(); 
    
    result['inventory'].forEach(function(row) {
        var obj_block='';
        if (row['id_block']>0){ obj_block=' inv_block block'+row['id_block']; data_id=row['id_block'];}
        if (row['id_object']>0) { obj_block=' inv_object object'+row['id_object']; data_id=row['id_object'];}
        
     
   //    if (row['id_object']==2) $(".inventory").append("<div class='col-xs-1 btn btn-primary  breed_btn"+obj_block+"' data-id='"+data_id+"'><span class='fa fa-battery-4' style='font-size:30px; margin-top:10px; margin-left:-20px;float:left'></span><span class='badge bg-danger'>"+row['count']+"</span></div>");     else 
   if (row['id_object']>0) $(".inventory").append("<div class=' col-xs-1"+obj_block+"' data-id='"+data_id+" ' style='background: url(/img/game/objects/"+row['id_object']+".png);  background-size:100% 100%; width:64px; height:64px;' ><span class='badge bg-danger'>"+row['count']+"</span></div>");
   else 
       
       $(".inventory").append("<div class='col-xs-1 btn btn-primary breed_btn"+obj_block+"' data-id='"+data_id+"'><span class='badge bg-danger'>"+row['count']+"</span></div>");
        
        if (row['id_object']>0){ } else $(".inventory_shop").append("<div class='col-xs-1 btn btn-primary breed_btn "+obj_block+"' data-id='"+data_id+"'><span class='badge bg-danger'>"+row['count']+"</span><br><span class='badge bg-danger'>Цена: <br>"+row['price']+"</span></div>");
        if (row['id_object']==1) { $(".inv_object_fast.object"+row['id_object']+" span").html(row['count']);}
                     
    }); 
}
if (result['indicator_new']==1){    
    var str; 
    str="<span class='fa fa-battery-0 text-danger'></span>";     
    if (result['indicator']['oxygen_percent']>10) str="<span class='fa fa-battery-1 text-danger'></span>"; 
    if (result['indicator']['oxygen_percent']>25) str="<span class='fa fa-battery-2 text-warning'></span>"; 
    if (result['indicator']['oxygen_percent']>50) str="<span class='fa fa-battery-3 text-warning''></span>";
    if (result['indicator']['oxygen_percent']>75) str="<span class='fa fa-battery-4 text-success'></span>";
    if (result['indicator']['oxygen_percent']<0)result['indicator']['oxygen_percent']=0;
    $(".indicator_oxigen").html(str+" <span style='color:#fff'>"+result['indicator']['oxygen_percent']+"%</span>");
    
    str="<span class='fa fa-heart text-danger'></span>";  
    if (result['indicator']['heart_percent']>5) str="<span class='fa fa-heart text-danger'></span>"; 
    if (result['indicator']['heart_percent']>25) str="<span class='fa fa-heart text-warning'></span>"; 
    if (result['indicator']['heart_percent']>50) str="<span class='fa fa-heart text-warning'></span>";
    if (result['indicator']['heart_percent']>75) str="<span class='fa fa-heart text-success'></span>";
    $(".indicator_heart").html(str+" <span style='color:#fff'>"+result['indicator']['heart_percent']+"%</span>");   
   
   $(".indicator_money").html("<span class='fa fa-btc text-primaty'></span><span style='color:#fff'> "+result['indicator']['money']+"</span>"); 
 $(".indicator_online").html("<span class='fa fa-users'></span><span style='color:#fff'> "+result['indicator']['online']+"</span>");   
        //console.log(str);
    //message(str);
}
//if (result['error']==0) LoadData();
  
    }
setInterval(function () { 
  //  LoadData();
    
    
}, 5000);

function walk()
{
    var load="<span class='fa fa-gavel fa-spin'style=' color:#f1f0f0;font-size:50px; position: absolute; z-index: 4'></span>";
    
    if (direction=='up') {        if($(".map_field:eq("+(12-5)+")") .find("div").is(".breed:not(.obj)")) $(".map_field:eq("+(12-5)+")").append(load); }
    if (direction=='down') {  if($(".map_field:eq("+(12+5)+")").find("div").is(".breed:not(.obj)")) $(".map_field:eq("+(12+5)+")").append(load); }
    if (direction=='left') {      if($(".map_field:eq("+(12-1)+")")  .find("div").is(".breed:not(.obj)")) $(".map_field:eq("+(12-1)+")").append(load); }
    if (direction=='right'){     if($(".map_field:eq("+(12+1)+")") .find("div").is(".breed:not(.obj)"))$(".map_field:eq("+(12+1)+")").append(load); }

    $.ajax({
        url: '/ajax.php',
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        data: dataSend,
        
        success: function(data)
        {
           // map_reload(data);
            if (data['message']!=0)  message(data['message']);
            $(".indicator_compass").html("<span class='fa fa-compass  text-primaty' style=' '></span> "+data['x']+":"+data['y']);

            //x=data['x']; y=data['y'];
            clickDisable=0;
            LoadData();
        },
    });       
} 
function map_reload(map)
{
    var data=map;
 $("#infoxy").html("x:"+data['x']+"; y:"+data['y']+";");
           // $("[type='checkbox']").prop('checked',false);   

    data['pole'].forEach(function(row2, i, arr) {
        img='';
        if (row2['y']<=0) class_='aboveground'; else  class_='underground';
        if (row2['id_block']>0)  class_='breed '; 
        if (row2['id_block']==1) class_+="  block1";          
        if (row2['id_block']==2) class_+=" block2";  
        if (row2['id_block']==3) class_+=" block3";  
        if (row2['id_block']==4) class_+=" block4";  
        if (row2['id_block']==5) class_+=" block5";  
        if (row2['id_block']==6) class_+="aboveground block6";  
        if (row2['id_block']==7) class_+="aboveground block7";  
        img="<div class='"+class_+"' data-x='"+row2['x']+"' data-y='"+row2['y']+"'></div>";  
         if (row2['obj']==0 && row2['gamer']>0) img+="<span class=' user'></span>";               
        var xy_obj=img;         
        $(".map_field:eq("+i+")").html(xy_obj);
        $(".user").css({ 'background-position':backgroundPosition});  
    });
           
        data['objects'].forEach(function(row2, i, arr) {
        
        if (row2['obj']==1) $(".map_field [data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").append("<div class='breed stairs obj'></div>");
        if (row2['obj']==0 && row2['gamer']>0)
        $(".map_field [data-x='"+row2['x']+"'][data-y='"+row2['y']+"']").append("<span class=' user obj'></span>");             
        $(".user").css({ 'background-position':backgroundPosition}); 
          
       }); 
    
}
$(".walk ").click(function(){
    
    direction=$(this).data('direction');
    if (direction==turn_now && clickDisable==0){ 
        clickDisable=1;    
        //console.log(bill);
        dataSend="object=Game&action=walk&direction="+direction+"&json=1";
        walk();
    } 
    else  
    { 
        turn_now=direction; 
/*        
        if (direction=='left'){ backgroundPosition=' 0px -72px'; $(".user").css({ 'background-position':backgroundPosition}); } 
        if (direction=='right'){ backgroundPosition=' 0px -144px'; $(".user").css({ 'background-position':backgroundPosition});} 
        if (direction=='up'){ backgroundPosition=' 0px -215px'; $(".user").css({ 'background-position':backgroundPosition}); }
        if (direction=='down'){ backgroundPosition='0px 0px'; $(".user").css({ 'background-position':backgroundPosition}); }
*/
        if (direction=='left'){ backgroundPosition=' 0px -42px'; $(".user").css({ 'background-position':backgroundPosition}); } 
        if (direction=='right'){ backgroundPosition=' 0px -82px'; $(".user").css({ 'background-position':backgroundPosition});} 
        if (direction=='up'){ backgroundPosition=' 0px -120px'; $(".user").css({ 'background-position':backgroundPosition}); }
        if (direction=='down'){ backgroundPosition='0px 0px'; $(".user").css({ 'background-position':backgroundPosition}); }
    }
    
});

$(".click-set-stairs").click(function(){
    
    var result=_AJAX('Game','SetObject',{ json:1, id_object:1, direction:turn_now });
    if (result['error']==0) { 
        //$(".map_field:eq(12)").append("<div class='breed stairs obj'></div>");
        $('#inventory').modal('hide');
        LoadData();
        }
});

$('body').on('click', '.inv_block', function(){
    var shop=$(this).parent().data('shop');
    var id_block=$(this).data('id');
    if (shop==1){
        var result=_AJAX('Game','SellBlock',{ json:1, id_block:id_block });
        //if (result['error']==0){ LoadData(); } //$('#inventory').modal('hide');       
    }
    else{
        var result=_AJAX('Game','SetBlock',{ json:1, id_block:id_block, direction:turn_now, x:x, y:y  });
        if (result['error']==0){  $('#inventory').modal('hide'); LoadData(); }
    }
});

$('body').on('click', '.inv_object,  .inv_object_fast', function(){
    var shop=$(this).parent().data('shop');
    var id_object=$(this).data('id');
    if (shop==1){
        var result=_AJAX('Game','SellObject',{ json:1, id_object:id_object, direction:turn_now, x:x, y:y  });
        if (result['error']==0){ LoadData(); } $('#inventory').modal('hide');         
    }
    else{
        var result=_AJAX('Game','UseObject',{ json:1, id_object:id_object, direction:turn_now, x:x, y:y  });
        if (result['error']==0){ LoadData(); } $('#inventory').modal('hide'); 
    }
});

$("body").on('click','.click-take',function(){
    var result=_AJAX('Game','TakeObject',{ json:1, direction:turn_now});  LoadData();
});
$("body").on('click','.click-kick',function(){
    if (clickDisable==0){ 
        clickDisable=1;   
    var eq_div=0;
    if (turn_now=='left') eq_div=11;
     $(".map_field:eq("+eq_div+")").find(".monster").addClass("monster_kick");
    var result=_AJAX('Game','KickToMonster',{ json:1, direction:turn_now});  LoadData();
     if (result['error']>=0) { 
         $(".map_field:eq("+eq_div+")").find(".monster").removeClass("monster_kick"); 
         clickDisable=0;   }
     
     }
});

$("body").on('click','.click-object-buy',function(){
    var result=_AJAX('Game','BuyObject',{ json:1, id_object: $(this).data('id')});  LoadData();
});

$(".win-open").on('click',function(){
//console.log($(this).data('target'));
    $($(this).data('window')).modal('show');
    //message('проверка');
});});
function _AJAX(_object, _action, _param){
    _param['json']='1';       
    var res;
        $.ajax({
        url: '/ajax2.php',
        async:false,
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
                    data: { object: _object, action: _action, param: _param},
        
        success: function(data)
        {  
          //  alert(data['res']);
            //return data;

            if ("msg" in data)message(data['msg']);
            if ("error_msg" in data)message(data['error_msg']);
            res=data;
        },
    });  
    return res;  
}
</script>
