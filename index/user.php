<?
if ($_SESSION[user_id]>0)
{
    $SmartyVar['template']='user.html';
    $SmartyVar['template_full']='no'; // подгружать стили и скрипты заново или использовать готовый index

  if (isset($_POST['go_s']))
  {
    $arr=array(
      'mobile_phone'=>$_POST['mobile_phone'],
      'email'=>$_POST['email'],
      'work'=>$_POST['work'],
      'city'=>$_POST['city']
    );
    $db->update('user',$arr,"WHERE id='{$_SESSION[user_id]}'");

    $_SESSION['message_type']='success';
    $_SESSION['message_icon']='fa fa-check';
    $_SESSION['message']='<b>Поздравляем!</b> Вы успешно изменили информацию!';
  }
  if (isset($_POST['go_s_pass'])) // сохранение пароля
  {
    if (md5($_POST['pass_old'])==$_SESSION['user_password']) {
      $arr=array(
        'password'=>md5($_POST['pass_new'])
      );
      $db->update('user',$arr,"WHERE id='{$_SESSION[user_id]}'");

      $_SESSION['user_password']=md5($_POST['pass_new']);

      $_SESSION['message_type']='success';
      $_SESSION['message_icon']='fa fa-check';
      $_SESSION['message']='<b>Поздравляем!</b> Вы успешно изменили пароль!';
    }
    else
    {
      $_SESSION['message_type']='danger';
      $_SESSION['message_icon']='fa fa-ban';
      $_SESSION['message']='<b>Ошибка!</b> Пароль не верный!';
    }
  }
  $q="SELECT * FROM user WHERE id='{$_SESSION['user_id']}'";
  $smarty->assign('info',$db->getRow($q));

  $smarty->assign('path_title','Все платежи');
  $smarty->assign('path_descr','Просмотр платежей');
  $smarty->assign('path_link','Просмотр платежей');
}
else
{
  $SmartyVar['template']='index_enter.html';
  $SmartyVar['template_full']='yes'; // подгружать стили и скрипты заново или использовать готовый index
}
?>