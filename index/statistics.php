<?
if ($_SESSION[user_id]>0)
{
  $SmartyVar['template_full']='no'; // подгружать стили и скрипты заново или использовать готовый index
  $smarty->assign('path_title','Статистика расходов');
  $smarty->assign('path_descr','Статистика расходов по кошельку и категориям');
  $smarty->assign('path_link' ,'Статистика расходов');

/* Для фильтра */
  $row=$purse->mypurse_all();
  $smarty->assign('purse_all', $row);
/*--------------------------------------*/

/* Для статистики категорий */
    $row=$purse->category_bill();
    print_arr($row, 1, 'Категории и траты'); // функция в index 
    $smarty->assign('category_all', $row);
/* ------------------------ */

/* Расчет по месяцам */
$arr=array();

//$purse->sql_lite=1;
$row2=$purse->stats_bill_all();

$month_year='';
$sum=0;
$sum2=0;
$max_debet=0;
$max_credit=0;
//echo '-------'.$temp_MY=date('Y-m',$row[start_date]);
foreach ($row2 as $row)
{

    $temp_MY=date('Y-m',$row[start_date]);
    //echo $temp_MY."";
    if ($month_year!==$temp_MY)
    {
        if ($month_year!=='') {
           //echo " сброс".$month_year.'  '.$sum.'  '.$sum2;
           $arr[]=array($month_year,$sum,$sum2); }
        $month_year=$temp_MY;
        $sum=$row['value_debet'];
        $sum2=$row['value_credit'];

    }
    else
    {
        $sum+=$row['value_debet'];
        $sum2+=$row['value_credit'];
    }
    //echo ' = '.$sum.' | '.$sum2;
    //echo "<br>";
if ($max_debet<$row['value_debet'])   $max_debet=$row['value_debet']; 
if ($max_credit<$row['value_credit'])   $max_credit=$row['value_credit'];
}


 $arr[]=array($temp_MY,$sum,$sum2);
$smarty->assign('bill_all', $arr);
$smarty->assign('max_debet',$max_debet);
$smarty->assign('max_credit',$max_credit);
/*------------------*/

    $SmartyVar['template']='stats.html';

}
else
{
  $SmartyVar['template']='index_enter.html';
  $SmartyVar['template_full']='yes'; // подгружать стили и скрипты заново или использовать готовый index
}
//exit;
?>