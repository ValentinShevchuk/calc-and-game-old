<?
include('inside_stats.php');
ms();
include('config.php');

function print_arr($arr=array(),$exit=0, $title='')
{
    // функция для декоровтивного вывода массивов и переменных во время тестирования
    // чтобы функция вывела на экран должен быть параметр test
    // $exit = 0 - не вызывает exit в конце функции
    // $title - поснение что и почему вывод
    if (isset($_GET['test'])){  
        if ($title!=='') echo "<br><strong>{$title}</strong></br>";
        echo "<hr><pre>";
        if (is_array($arr)) print_r($arr);
        echo "</pre></hr>";
        if ($exit==1) exit;
    }
}

$smarty->assign('GET',$_SERVER[QUERY_STRING]); // передаем строку запроса в smarty определять если переданые парамемтры по get
mS('Запуск классов');
$purse=New MyPurse();
$forForm=New ForForm();
$enterSite=new Enter();

//$purse->user_start(100);

$SmartyVar['title']='mypurse v1.1';
/*
if(!empty($_GET['lang'])){
    if (isset($_GET['lang'])) $_SESSION['lang']=$_GET['lang'];
    if(count(explode("&",$_SERVER[QUERY_STRING]))=="1"){
	    Header("Location: /".$address_language);
	}else{Header("Location: /?".$address_language);}
}
*/
if (isset($_POST['enter']) || isset($_GET['enter'])){
    $error=$enterSite->enterTaskMobileNew($_POST['login'],$_POST['password']);   
    if ($error==0) $smarty->assign('message',array('type'=>'danger','text'=>'Ошибка входа')); 
}

if ($_SESSION[user_id]>0) $smarty->assign('user_level',$purse->user_level_get());

if (isset($_GET['exit'])){
  $enterSite->logoutTaskMobile();
  //header('location:'.$_SERVER['HTTP_REFERER']);
  header('location:index.php');
  exit;
}
// set language
if (!isset($_SESSION['lang'])||empty($_SESSION['lang']))
    $_SESSION['lang'] = 'en';

if (isset($_GET['lang']))  $_SESSION['lang']=$_GET['lang'];

if (isset($_GET['c']))$_GET['c']=(int)$_GET['c'];

if (file_exists('js/__'.$_GET['p'].'.js')) { $smarty->assign('js_file','js/__'.$_GET['p'].".js");}

$page = empty($_GET['p'])?"index_main":$_GET['p']; // если нет страницы, то грузится главная
if (file_exists($page.".php")) require($page.".php"); else require("index_main.php");


$smarty->assign("SmartyVar", $SmartyVar); // option to template
$smarty->assign('label',$text);  // label on other languages
$smarty->assign('url_this',$url_this);  // current url
$smarty->assign('langs',$langs);  // current language
  $smarty->assign('referer_url',$_SERVER['HTTP_REFERER']);
  
if ($SmartyVar['template_full']=='yes')
{
    // если переменная содержит yes то страница html грузится без дополнительных вставок.
    //Приминительно к: логин, регистрация, статичная страница или инфа без регистрации
    $smarty->display($SmartyVar['template']);
}
else $smarty->display('index.html');  // иначе грузится шаблон

//me();

if ($_SESSION['user_id']>0) $purse->user_level_set();
//echo "<pre>";
//print_r($load_stats_name);
$_SESSION['alert_one']++;
?>