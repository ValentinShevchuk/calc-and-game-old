<?php
/**
 * class exportXLS
 */
class exportXLS {
	
	var $filename;
		
	var $dataset;
	
	var $titleset;
	
	var $titlebgcolor;
	
	var $needColtitle;

        /**
         * initialize function
         * @param <string> $filename            is the xls file's name
         * @param <array> $dataset              is the output DataSet
         * @param <array> $dataset2            is the output Account DataSet
         * @param <array> $dataset3            is the output Transaction DataSet
         * @param <boolean> $needColtitle       is to control the title display
         * @param <array> $titleset             is the title array
         * @param <string> $titlebgcolor        is the HEXcolor , as : #09EEFF
         */
	public function __construct($filename='',$dataset = array(),$needColtitle = false,$titleset=array(),$titlebgcolor = '#CECECE',$account_arr=array(),$needColtitle2 = false,$titleset2=array(),$trans_arr=array(),$needColtitle3 = false,$titleset3=array(),$type='profile')
	{ //doc

		if(empty($filename)){
			$this->filename = date('dmY',time()).'_outexcel.xls';
		}else{
			
			if(strpos($filename,".xml")){
				$this->filename = str_replace('.xml','',$filename).'.xml';
			}else{
				$this->filename = str_replace('.xls','',$filename).'.xls';
			}			
		}
		
		$this->dataset = is_array($dataset)? $dataset : array();
		$this->dataset2 = is_array($account_arr)? $account_arr : array();
		$this->dataset3 = is_array($trans_arr)? $trans_arr : array();

		$this->needColtitle = $needColtitle;
		$this->needColtitle2 = $needColtitle2;
		$this->needColtitle3 = $needColtitle3;
		
		$this->type = $type;

		if($this->needColtitle === true){			
			$this->titleset = is_array($titleset) ? $titleset : array();
			$this->titlebgcolor = 'FF'.str_replace('#','',$titlebgcolor);
		}

		if($this->needColtitle2 === true){			
			$this->titleset2 = is_array($titleset2) ? $titleset2 : array();
			$this->titlebgcolor2 = 'FF'.str_replace('#','',$titlebgcolor2);
		}

		if($this->needColtitle3 === true){			
			$this->titleset3 = is_array($titleset3) ? $titleset3 : array();
			$this->titlebgcolor3 = 'FF'.str_replace('#','',$titlebgcolor3);
		}

		
	}
	
	/**
         * output and download the xml file
         */
	public function outputXLS($path=''){ //doc
		require_once($path.'../include/phpexcel/PHPExcel.php');
		require_once($path.'../include/phpexcel/PHPExcel/IOFactory.php');
		ob_clean();
		$obj_excel = new PHPExcel();
		$results = $this->dataset;
		$results2 = $this->dataset2;
		$results3 = $this->dataset3;
		//print_r($results);exit;
		//$results = iconv("gbk","utf-8",$results); //The escape character set
		if($this->needColtitle == true){//title1
			$titleArr = $this->titleset;
			$row_num = 1;

			foreach($titleArr as $key=>$item){
				if($this->type=='statistical'){
				
				}else{
					$obj_excel->getActiveSheet()->getColumnDimension(chr(65+$i))->setWidth(20);
				}				
				$obj_excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(20);
				$obj_excel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
				$col_value = chr(65+($i++)).$row_num;				
				$obj_excel->getActiveSheet()->setCellValue($col_value,$item, false);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->getStartColor()->setARGB($this->titlebgcolor);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				if($i==4 && $this->type=='profile') {
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}
				if(($i==1) && $this->type=='account_report'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+7).($row_num));
					$obj_excel->getActiveSheet()->getStyle($col_value)->getFont()->setSize(20);
				}
				
				if(($i==1) && $this->type=='statistical'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+22).($row_num));
					$obj_excel->getActiveSheet()->getStyle($col_value)->getFont()->setSize(20);
				}

			}
		}
		
		if(!empty($results))//array1
		{
			$row_num = ($this->needColtitle === true) ? 2 : 1;
			foreach($results as $index=>$row){
				$i = 0;
				foreach($row as $key=>$item){
					$col_value = chr(65+($i++)).($row_num);
					$obj_excel->getActiveSheet()->setCellValueExplicit($col_value,$item, PHPExcel_Cell_DataType::TYPE_STRING);
					$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					if($i==4 && $this->type=='profile') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
						$i++;
					}
					if(($i==4||$i==5) && $this->type=='transaction') {
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}
					if(($i==6||$i==7) && $this->type=='accounts') {
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}
				}
				$row_num++;
			}
		}
		
		if($this->needColtitle2 == true){//title2
			$titleArr2 = $this->titleset2;
			$row_num++;
			$i = 0;
			foreach($titleArr2 as $key=>$item){
				if($this->type=='statistical'){
				
				}else{
					$obj_excel->getActiveSheet()->getColumnDimension(chr(65+$i))->setWidth(25);
				}
				
				$col_value = chr(65+($i++)).$row_num;
				$obj_excel->getActiveSheet()->setCellValue($col_value,$item, false);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->getStartColor()->setARGB($this->titlebgcolor);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				if($i==2 && $this->type=='profile') {
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}
				if(($i==6||$i==8) && $this->type=='transaction') {
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}
				if(($i==6) && $this->type=='accounts') {
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}
				if(($i==1) && $this->type=='account_report'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}
				/*if(($i==1) && $this->type=='statistical'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}*/
			}
		}
		
		if(!empty($results2))//array2
		{
			$row_num++;
			foreach($results2 as $index=>$row){
				$i = 0;
				foreach($row as $key=>$item){
					$col_value = chr(65+($i++)).($row_num);
					$obj_excel->getActiveSheet()->setCellValueExplicit($col_value,$item, PHPExcel_Cell_DataType::TYPE_STRING);
					$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					if($i==2 && $this->type=='profile') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
						$i++;
					}
					if(($i==6||$i==8) && $this->type=='transaction') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
						$i++;
					}
					if(($i==6) && $this->type=='accounts') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
						$i++;
					}
					if($i==6 && $this->type=='profile'){
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}
					if(($i==8||$i==9) && $this->type=='transaction'){
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					}
					if(($i==12||$i==13) && $this->type=='transaction'){
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}					
					if(($i==9||$i==10||$i==11) && $this->type=='accounts') {
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}
					if($i==5 && $this->type=='accounts') {
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					}
					if($i==1 && $this->type=='statistical') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i+22)).($row_num));
						$i++;
					}
				}
				$row_num++;
			}
		}
		
		if($this->needColtitle3 == true){//title3
			$titleArr3 = $this->titleset3;
			$row_num++;
			$i = 0;
			foreach($titleArr3 as $key=>$item){
				$col_value = chr(65+$i++).$row_num;
				if($this->type=='statistical'){
				
				}else{
					$obj_excel->getActiveSheet()->getColumnDimension(chr(65+$i))->setWidth(25);
				}
				
				$obj_excel->getActiveSheet()->setCellValue($col_value,$item, false);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->getStartColor()->setARGB($this->titlebgcolor);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				if($i==3 && $this->type=='profile') {
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
					$i++;
				}
				if(($i==3) && $this->type=='account_report'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+3).($row_num));
					$i+=4;
					
				}
				if(($i==2) && $this->type=='statistical'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+17).($row_num));
					$i+=18;
				}
				if(($i==21) && $this->type=='statistical'){
					$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+2).($row_num));
					$i+=3;
				}
			}
		}
		
		if(!empty($results3))//array3
		{
			$row_num++;
			foreach($results3 as $index=>$row){
				$i = 0;
				foreach($row as $key=>$item){
					$col_value = chr(65+($i++)).($row_num);
					$obj_excel->getActiveSheet()->setCellValueExplicit($col_value,$item, PHPExcel_Cell_DataType::TYPE_STRING);
					$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					if($i==3 && $this->type=='profile') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$i++;
					}
					if(($i==5 || $i==6 || $i==7) && $this->type=='profile'){
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					}
					if($i==3 && $this->type=='account_report') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+3).($row_num));
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$i+=4;
					}
					if($i==1 && $this->type=='statistical') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)).($row_num));
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$i++;
					}
					if($i==3 && $this->type=='statistical') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+16).($row_num));
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$i+=17;
					}
					if($i==21 && $this->type=='statistical') {
						$obj_excel->getActiveSheet()->mergeCells(chr(65+($i-1)).($row_num).":".chr(65+($i)+2).($row_num));
						$obj_excel->getActiveSheet()->getStyle($col_value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$i+=3;
					}
				}
				$row_num++;
			}
		}
		
		$objwriter = PHPExcel_IOFactory::createWriter($obj_excel,'Excel5');
		//$objwriter = new PHPExcel_Writer_Excel2007($obj_excel); 
		
		header('Content-Disposition: attachment; filename='.$this->filename);
		header("Content-Type:application/vnd.ms-excel;charset=utf-8");
		header("Cache-Control:max-age=0");
		$objwriter->save('php://output');
		
	}

}

?>