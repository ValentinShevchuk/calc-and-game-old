<?php
class User_Action extends database {
	private $info;

	public function __construct(){
        parent::__construct();
	    $this->reset_info();
	    if (!isset($_SESSION['site_user']))   $_SESSION['site_user']=0;
		if (!isset($_SESSION['site_access'])) $_SESSION['site_access']=-3;

	}

   	public function enter($login,$pass,$table='user'){

      $q="SELECT * FROM ".$table." WHERE password='".md5($pass)."' AND id='$login'";
      $row = $this->db->getRow($q);

      if (count($row)>0){
        $this->info['site_login'] = $row['login'];
        $this->info['site_fio'] = $row['fio'];
        $this->info['site_user'] = $row['id'];
        $this->info['site_access'] = $row['access'];
        $this->set_sess();
      }
	}

    public function enterMortgage($login,$pass,$table='user'){

      $q="SELECT * FROM ".$table." WHERE password='".md5($pass)."' AND id='$login'";
      $row = $this->db->getRow($q);

      if (count($row)>0){
        $this->info['site_login'] = $row['login'];
        $this->info['site_fio'] = $row['fio'];
        $this->info['site_user'] = $row['id'];
        $this->info['site_access'] = $row['access'];
        $this->info['active_account'] = 0;
        $this->set_sess();
        return $row['id'];

        }
	}

	public function login($row){
		$this->info['site_login'] = $row['login'];
		$this->info['site_fio'] = $row['fio'];
		$this->info['site_user'] = $row['id'];
		$this->info['site_access'] = $row['access'];
		$this->info['login_time'] = date("d.m.Y H:i:s");
		$this->set_sess();
	}
	public function logout(){
		unset($_SESSION['left_menu']);
	   	unset($_SESSION['balance']);
		unset($_SESSION['modules_access']);
		$this->reset_info();
		$this->set_sess();
	}
	
	private function reset_info(){
	  $this->info['site_user']=0;
	  $this->info['site_access']=0;
	  $this->info['site_login']=0;
	  $this->info['site_fio']=0;
	}
	private function set_sess(){
		foreach($this->info as $key=>$value){
			$_SESSION[$key] = $value;
		}
	}
	function get_sess(){
		$this->reset_info();
		foreach($this->info as $key=>$value){
			if(!isset($_SESSION[$key])){
				continue;
			}
			$this->info[$key] = $_SESSION[$key];
		}
		return $this->info;
	}
}
?>