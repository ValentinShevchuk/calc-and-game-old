<?php
function GetHtmlOption($s,$f=1)
{
	global $_SESSION,$db,$langs; $option='';
	$arr=$s;
	//var_dump($arr);
	$s=$arr['s'];
	$a=$arr['a'];
	$na=$arr['na'];  //not show
	if (strtolower($s)=='currency'){
        $q="SELECT * FROM `currency_list` order by id"; $row=$db->getAll($q);
	    foreach ($row as $row){
	    	if ($a==$row[id]) {
	    		$check=' selected=selected';
	    	}else {
	    		$check='';
	    	}
			if ($na!=$row['id']){
				$option.="<option value='{$row[id]}' $check>".lang($row['ccy'])."</option>";
			}
	    }
	}else{
	    if (is_integer($s)) { 
	    	$q="SELECT * FROM  constants WHERE parent='{$s}'"; $row=$db->getall($q);
	    } else {
	    	$q="SELECT id FROM  constants WHERE LOWER(title)=LOWER('{$s}')";
	    	$row=$db->getOne($q);
	    	$q="SELECT * FROM  constants WHERE parent='{$row}' AND vis>0 order by value";
	    	$row=$db->getall($q);      //   echo $q;
	    }
        
	    foreach ($row as $row){
	    	if ($a==$row[value]) {
	    		$check=' selected=selected';
	    	}else {
	    		$check='';
	    	}
			if ($na!=$row['value']) {
				$option.="<option value='{$row[value]}' $check>".lang($row['title'])."</option>";
			}
	    }
	}
	//  if ($f==1) echo $option;
	return $option;
}

function GetHtmlOptionString($params){
	global $db;
	$s = trim($params['s']);
	$sid = intval($params['sid']);
	if(strtolower($s)=='currency_cn'){
		$q="SELECT ccy_cn FROM  currency_list WHERE `id`='{$sid}' limit 1";	
	}else if (strtolower($s)=='currency'){
	    $q="SELECT ccy FROM  currency_list WHERE `id`='{$sid}' limit 1";	
	}else{
	    $q="SELECT id FROM  constants WHERE LOWER(title)=LOWER('{$s}')";
	    $fid=$db->getOne($q);
	    $q="SELECT title FROM  constants WHERE parent='{$fid}' AND `value`='{$sid}' limit 1";
	}
	$row=$db->getOne($q);      //   echo $q;
	return lang($row);
}

function GetAccountName($params){
	global $db;
	$s = trim($params['s']);
	if($s>0){
		$q="SELECT full_account FROM accounts_client WHERE id={$s} limit 1";
		$row=$db->getOne($q);      //   echo $q;
	}else{
		$row="";
	}
	return $row;
}

function FormatSpecialDate($params){
	$date_str = $params['date'];
	$index = $params['index'];	
	$tmp_arr = explode(' - ',$date_str);
	
	return $tmp_arr[$index];
}

function GetFullAcounts($params){
	global $db;
	$id = $params['id'];
	if($id>0){
		$q = "SELECT full_account FROM `accounts_client` WHERE `id` = '{$id}' LIMIT 1 ";
		$full_account=$db->getOne($q);
	}else{
		$full_account="";
	}
	return $full_account;
}

function GetCountryCityName($params){
	global $db;
	$s = trim($params['s']);
	$a = trim($params['a']);
	$lang = trim($params['l']);
	if(!empty( $s)){ 
	    if($a=='country'){
		    $q="SELECT name_en,name_ch,code FROM geo_{$a} WHERE id={$s} limit 1";
	        $row=$db->getRow($q);      //   echo $q;
	        if(!empty($lang)){
	        	$row = $row['name_'.$lang]." - ".$row['code'];
	        }else{
	        	$row = $row['name_en']." - ".$row['code'];	
	        }
			
		}else{
		    $q="SELECT name_en FROM geo_{$a} WHERE id={$s} limit 1";
	        $row=$db->getOne($q);      //   echo $q;
		}
	}
	return $row;
}

function GetDecorationMoney($number){
    $number=implode($number);
    $number=sprintf("%01.2f", $number);
    $pos = strripos($number,'-');
    if ($pos === false)
        $pref='';
    else{
        $pref='-';
        $number=substr($number,1);
    }
    $str = explode('.',$number);
    $number='';
    $mod=strlen($str[0])%3;
    for($i=0;$i<=strlen($str[0])-1;$i++){
        if ($i%3==$mod && $i!==0){$number.="&nbsp;";}
        $number.=$str[0][$i];
    }
    if ($str[1]=='')$str[1]='00';
    if ($number=='')$number='0';
    $number.='.'.$str[1];
	//return sprintf("%01.2f", $pref.$number);
	return $pref.$number;
}

function GetManager($params){
	global $db;
	$s = trim($params['s']);
	if((int)$s){
		$q="SELECT names,surname FROM user WHERE id={$s} limit 1";
		$managerRow=$db->getRow($q);      //   echo $q;
		if($managerRow){
			return $managerRow['names']." ".$managerRow['surname'];
		}else{
			return $s;
		}
	}else{
		return $s;
	}
}

function GetCountryCityCN($params){
	global $db;
	$s = trim($params['s']);
	$a = trim($params['a']);
	if(!empty($s)){
		if($a=='country'){
			$q="SELECT `name_ch` FROM `geo_country` WHERE name_en LIKE '".$s."'  LIMIT 1";
		}else if($a=='city'){
			$q="SELECT `name_cn` FROM `geo_city` WHERE name_en LIKE '".$s."' AND `country_id`='88' LIMIT 1";
		}
		$row=$db->getOne($q);
	    if (!empty($row)) return $row;
	    else return $s;
	}
}

function GetCountryRU($params){
	global $db;
	$s = trim($params['s']);
	$q="SELECT `name_ru` FROM `geo_country` WHERE name_en LIKE '".$s."'  limit 1";
	$row=$db->getOne($q);      //   echo $q;
    if (count($row)>0) return $row;
    else return $s;

}
function GetCityRU($params){
	global $db;
	$s = trim($params['s']);
	$q="SELECT `name_ru` FROM `geo_city` WHERE name_en LIKE '".$s."'  limit 1";
	$row=$db->getOne($q);      //   echo $q;
	if (count($row)>0) return $row;
    else return $s;
}

function GetFileUrl($params){
	global $db;
	$id_files = trim($params['s']);
	if ($id_files>0) {
        $id_folder= $id_files/100;
        $id_folder=(int)$id_folder;
        $img=$db->getRow("SELECT * FROM files WHERE id={$id_files}");
        return "/file.php?f={$id_folder}/$img[id].$img[ext]";
    }
}

function GetFullAccountInfo($params){
	global $db;
	$s = trim($params['s']);
    if (!empty($s)){
	    $q="SELECT t1.full_account,t2.title,t1.value FROM accounts_client t1 LEFT JOIN constants t2 ON t2.parent='41' AND t2.value=t1.type_accounts WHERE t1.id={$s} limit 1";
	    $row=$db->getRow($q);      //   echo $q;
	    return lang($row['title']).' '.$row['full_account'].' ('.GetDecorationMoney((array)$row['value']).')';
    }
}

function GetClientInfo($params){
	global $db;
	$s = trim($params['s']);
	$a = strtolower(trim($params['a']));
	if($a=='') $a='names';
	$q="SELECT `{$a}` FROM `client_personal` WHERE `id_user`='{$s}' LIMIT 1";
	$row=$db->getOne($q);      //   echo $q;
	return $row;
}

function UcfirstWord($params) {
    $string = trim($params['s']);
    $e ='utf-8';
    if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) {
        $string = mb_strtolower($string, $e);
        $upper = mb_strtoupper($string, $e);
        preg_match('#(.)#us', $upper, $matches);
        $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
    } else {
        $string = ucfirst($string);
    }
    return $string;
}

function CheckAccountOnLoan($params){
    global $db;
	$id = $params['id'];
    if (!empty($id)){
	    $q = "SELECT id FROM `loan_main` WHERE `id_account` = '".(int)$id."' AND type=3 AND vis=2 LIMIT 1 ";
	    $id_account=$db->getOne($q);
        if ($id_account>0) return 'color_red_tr';
    }
}
function GetUserContract($params){
	global $db;
	$id = (int)$params['id'];
	$q="SELECT `id_contract` FROM `client_personal` WHERE id_user='".$id."'";
	$row=$db->getOne($q);      //   echo $q;
	if (count($row)>0) return $row;
    else return 0;
}
function GetTaskInfo($params){
	global $db;
	$s = trim($params['s']);
	$sid = intval($params['sid']);
	if(strtolower($s)=='parent'){
		$q="SELECT title FROM jobs WHERE `id`='{$sid}' limit 1";	
	}else if (strtolower($s)=='group'){
	    $q="SELECT title FROM user_group WHERE `id`='{$sid}' AND vis>0 limit 1";	
	}else{
	    $q="SELECT id FROM  constants WHERE LOWER(title)=LOWER('{$s}')";
	    $fid=$db->getOne($q);
	    $q="SELECT title FROM  constants WHERE parent='{$fid}' AND `value`='{$sid}' limit 1";
	}
	$row=$db->getOne($q);      //   echo $q;
	return ($row);
}
function CountDateAgo($params){
	$time=trim($params['s']);
	$lang=trim($params['a']);
    $t=time()-$time;
    $f=array(
        '31536000'=>array('en'=>'years','cn'=>'年'),
        '2592000'=>array('en'=>'months','cn'=>'个月'),
        '604800'=>array('en'=>'weeks','cn'=>'星期'),
        '86400'=>array('en'=>'days','cn'=>'天'),
        '3600'=>array('en'=>'hours','cn'=>'小时'),
        '60'=>array('en'=>'mins','cn'=>'分钟'),
        '1'=>array('en'=>'secs','cn'=>'秒')
    );
    foreach ($f as $k=>$v)    {
        if (0 !=$c=floor($t/(int)$k)) {
            return $c." ".$v[$lang];
        }
    }
}
?>