<?php
/**
 * generate and get the print image
 * @global <type> $db
 * @param <type> $template
 * @return <type> array
 */
function getPdfstring($template='',$imgPath='pdf'){
    global $db;

    $template = trim($template);
    
    if(empty($template)){
        return array('htmlstring'=>'No template parameter!!');exit;
    }
    
    $sql = 'select * from pdf_standard where template=\''.$template.'\' order by id asc';
    $result = $db->getAll($sql);

    if($result === false){
        return array('htmlstring'=>'No parameters in template!!');exit;
    }

    //generate the color palette
    $img=@imagecreatetruecolor(678,960) or die('Failed to create the color palette');
    $white = imagecolorallocate($img,255,255,255);
    imagefilledrectangle($img,0,0,678,960,$white);

    $drawtype = array(
        0=>'text',
        1=>'date',
        2=>'checkbox',
        3=>'image',
        4=>'recbg',
        5=>'line',
        6=>'dtext',
		7=>'lineArray',
    );
    foreach($result as $item){
        switch($item['type'])
        {
            case $drawtype[0]:      //text
                $r = hexdec($item['color'][0].$item['color'][1]);
                $g = hexdec($item['color'][2].$item['color'][3]);
                $b = hexdec($item['color'][4].$item['color'][5]);

                $fontcolor = imagecolorallocate($img,$r,$g,$b);
                $fontfamily = "Fonts/tahoma.ttf";
                $fontsize = $item['fontsize']-2;
				$fontstring = isset($_POST[$item['desc']]) ? $_POST[$item['desc']] : $item['desc'];
				
				@imagettftext($img,$fontsize,0,$item['xposition'],$item['yposition'],$fontcolor,$fontfamily,$fontstring);
				break;				

            case $drawtype[6]:      //dynamic text
                $r = hexdec($item['color'][0].$item['color'][1]);
                $g = hexdec($item['color'][2].$item['color'][3]);
                $b = hexdec($item['color'][4].$item['color'][5]);

                $fontcolor = imagecolorallocate($img,$r,$g,$b);
                $fontfamily = "Fonts/tahoma.ttf";
                $fontsize = $item['fontsize']-2;
                $fontstring = str_replace('******', time(), $item['desc']);

                @imagettftext($img,$fontsize,0,$item['xposition'],$item['yposition'],$fontcolor,$fontfamily,$fontstring);
                break;
							
            case $drawtype[1]:      //date
                //var_dump($item);
                $dst_x = $item['xposition'];
                $dst_y = $item['yposition'];
                $src_x = 0;
                $src_y = 0;
                $src_width = $item['width'];
                $src_height = $item['height'];
                $pct = 100; //透明度
                $img_first = imagecreatefrompng($item['imageurl']) or die('Failed to open the image file!!');
                imagecopymerge($img, $img_first, $dst_x, $dst_y, $src_x, $src_y, $src_width, $src_height, $pct);
                
				if(isset($_POST[$item['desc']])){
					$time_val = date('dmY',$_POST[$item['desc']]);
					$fontcolor = imagecolorallocate($img,0,0,0);
					$fontfamily = "Fonts/tahoma.ttf";
					$fontsize = 9;
					
					for($i=0;$i<strlen($time_val);$i++){
						$fontstring = $time_val[$i];
						if($i == 0){
							$f_x =  $dst_x + 4;
						}else if($i == 1){
							$f_x =  $dst_x + 20;
						}else if($i == 2){
							$f_x =  $dst_x + 54;
						}else if($i == 3){
							$f_x =  $dst_x + 70;
						}else if($i == 4){
							$f_x =  $dst_x + 104;
						}else if($i == 5){
							$f_x =  $dst_x + 120;
						}else if($i == 6){
							$f_x =  $dst_x + 136;
						}else{
							$f_x =  $dst_x + 150;
						}
						$f_y = $dst_y + 13;
						@imagettftext($img,$fontsize,0,$f_x,$f_y,$fontcolor,$fontfamily,$fontstring);
					}
                }
				break;
				            
			case $drawtype[2]:      //checkbox
                //var_dump($item);
                $dst_x = $item['xposition'];
                $dst_y = $item['yposition'];
                $src_x = 0;
                $src_y = 0;
                $src_width = $item['width'];
                $src_height = $item['height'];
                $pct = 100; //透明度
				$img_url = '';
				
				if(!empty($item['desc'])){
					$post_arr = explode('##',$item['desc']);
					$post_key = $post_arr[0];
					$post_value = $post_arr[1];
					
					if( intval($_POST[$post_key]) === intval($post_value) ){
						$img_url = 'images/checkbox_selected.png';
					}else{
						$img_url = $item['imageurl'];
					}
					
					$img_first = imagecreatefrompng($img_url) or die('Failed to open the image file!!');
					imagecopymerge($img, $img_first, $dst_x, $dst_y, $src_x, $src_y, $src_width, $src_height, $pct);
				}
				
				break;
            case $drawtype[3]:      //image
                //var_dump($item);
                $dst_x = $item['xposition'];
                $dst_y = $item['yposition'];
                $src_x = 0;
                $src_y = 0;
                $src_width = $item['width'];
                $src_height = $item['height'];
                $pct = 100; //透明度
                $img_first = imagecreatefrompng($item['imageurl']) or die('Failed to open the image file!!');
                imagecopymerge($img, $img_first, $dst_x, $dst_y, $src_x, $src_y, $src_width, $src_height, $pct);
                break;

            case $drawtype[4]:      //recbg
                $r = hexdec($item['color'][0].$item['color'][1]);
                $g = hexdec($item['color'][2].$item['color'][3]);
                $b = hexdec($item['color'][4].$item['color'][5]);

                $bgcolor = imagecolorallocate($img,$r,$g,$b);
                $x1 = $item['xposition'];
                $y1 = $item['yposition'];
                $x2 = $x1+$item['width'];
                $y2 = $y1+$item['height'];
                imagefilledrectangle($img,$x1,$y1,$x2,$y2,$bgcolor);
                break;
            case $drawtype[5]:      //line
                $r = hexdec($item['color'][0].$item['color'][1]);
                $g = hexdec($item['color'][2].$item['color'][3]);
                $b = hexdec($item['color'][4].$item['color'][5]);

                $bgcolor = imagecolorallocate($img,$r,$g,$b);
                $x1 = $item['xposition'];
                $y1 = $item['yposition'];
                $x2 = $x1+$item['width'];
                $y2 = $y1+$item['height'];
                imagefilledrectangle($img,$x1,$y1,$x2,$y2,$bgcolor);

                $fontcolor = imagecolorallocate($img,0,0,0);;
                $fontfamily = "Fonts/tahoma.ttf";
                $fontsize = 9;
                $fontstring = $_POST[$item['desc']];

                @imagettftext($img,$fontsize,0,$x1+5,$y1-2,$fontcolor,$fontfamily,$fontstring);
                break;
				
			case $drawtype[7]:		//lineArray
                $r = hexdec($item['color'][0].$item['color'][1]);
                $g = hexdec($item['color'][2].$item['color'][3]);
                $b = hexdec($item['color'][4].$item['color'][5]);

                $bgcolor = imagecolorallocate($img,$r,$g,$b);
                $x1 = $item['xposition'];
                $y1 = $item['yposition'];
                $x2 = $x1+$item['width'];
                $y2 = $y1+$item['height'];
                imagefilledrectangle($img,$x1,$y1,$x2,$y2,$bgcolor);

                $fontcolor = imagecolorallocate($img,0,0,0);;
                $fontfamily = "Fonts/tahoma.ttf";
                $fontsize = 9;
				
				$str_arr = isset($_POST[$item['desc']]) ? $_POST[$item['desc']] : array();
				$fontstring = '';
				
				foreach($str_arr as $row){
					$fontstring .= $row.'   ';
				}
                
                @imagettftext($img,$fontsize,0,$x1+5,$y1-2,$fontcolor,$fontfamily,$fontstring);
                break;
				
				break;
            default:
				//something to done
                break;
        }
    }
	
	$day_time = time();
	$path = str_replace("/", "\\", $_SERVER['DOCUMENT_ROOT']);
    $imgpath = $path.$imgPath.'\\'.$template.'_'.$day_time.'.png';
	$relimgPath = $imgPath.'/'.$template.'_'.$day_time.'.png';
	
    //header("content-type:image/png");
    imagepng($img,$imgpath);
    imagedestroy($img);
    
	if(file_exists($imgPath) === false){
		return array('htmlstring'=>'Failed to generate the png file');exit;
	}

	$newPdf = new pdf();
	$html = '<img src="'.$relimgPath.'" border="0" />';

	$pdfPath = $path.$imgPath.'\\'.$template.'_'.$day_time.'.pdf';
	$newPdf->savePDF($pdfPath,$html);
	
	if(file_exists($pdfPath) === false){
		return array('htmlstring'=>'Failed to generate the pdf file',);exit;
	}
	
	$relpdfPath = $imgPath.'/'.$template.'_'.$day_time.'.pdf';
	$pdfString = $newPdf->openExistPDF($relpdfPath);
	
    return array('htmlstring'=>$pdfString,);
}