<?
class Jobs extends dbSaybel {

    public $archive=0;
    public $state=0; // состояние задачи
    public $my=0; // my task
    
    function insertItem($vis) // событие над задачей. Вызывается когда происходит какое либо событие. режактирование создание просмотр и т.д.
    {
      global $db;
      $q="INSERT INTO `jobs_act` (`id` ,`id_task`, `time_act` ,`id_type` ,   `id_user` , `vis` )
     VALUES (NULL ,'{$this->post['id_task']}', '".time()."', '{$vis}', '{$this->session[child]}', '1')";
     $this->db->query($q);
    }

    public function setRating() // установка рейтинга автором
    {

      $q="INSERT INTO `jobs_rating` VALUES (NULL ,'{$_POST['id_task']}', 1,'{$_POST['score']}', '".$this->session[child]."','".time()."', '1') ON DUPLICATE KEY UPDATE score='{$_POST['score']}', date_time='".time()."', id_user='".$this->session[child]."'";
      $this->db->query($q);
      echo "jAlert('Ok');";

    }


    public function setTaskPosition() // ручная смена позиции 
    {

         $q="UPDATE jobs SET position='{$this->post[position]}' WHERE id='{$this->post[id_task]}'";
         $this->db->query($q);

    }


    public function setProgress() // Ручная установка прогресса задачи
    {
        if ($_POST[id_user]==$_SESSION[user_id]){ $q="UPDATE jobs SET progress='{$this->post[progress]}' WHERE id='{$this->post[id_task]}'";
         $this->db->query($q);
         $this->insertItem(24);echo "jAlert('".lang('Successfully')."',2);";                 }
         else {echo "jAlert('".lang('Error!')."',1);";}
    }


      public function setCheck() // установка контрольных точек. Для задач
      {

         $q="UPDATE jobs_check SET value='{$this->post[id_comm]}', id_user='{$_SESSION[user_id]}', date_create='".time()."' WHERE id='{$this->post[id_task]}'";
       //  echo "UPDATE jobs_check SET value='$this->post[id_comm]' WHERE id='{$this->post[id_task]}'"
         $this->db->query($q);

        $q__="INSERT INTO `jobs_act` (`id` ,`id_task`,`id_check`, `time_act` ,`id_type` ,   `id_user` , `vis` )
        VALUES (NULL ,'{$this->post[id_task2]}','{$this->post[id_task]}', '".time()."', '22', '{$_SESSION['child']}', '1')";
        $this->db->query($q__);

    }
    public function setDifficulty() // установка сложности
    {

      $q="INSERT INTO `jobs_rating` VALUES (NULL ,'{$_POST['id_task']}', 2,'{$_POST['score']}', '".$this->session[child]."','".time()."', '1') ON DUPLICATE KEY UPDATE score='{$_POST['score']}', date_time='".time()."', id_user='".$this->session[child]."'";
      $this->db->query($q);
      echo "jAlert('Ok');";

    }

    public function GetTaskOnline($ajax=0) // получение задачи которые выполняются сейчас
    {
     /*    $q="SELECT *,t2.title ptitle, t1.title, t1.id
         FROM jobs t1, jobs_project t2, week_user t3, jobs_act t4
         WHERE
         t1.vis=2 AND t1.id_project=t2.id AND
         t1.id_user=t3.id AND
         (t4.id_task=t1.id AND t4.id_type=2) GROUP BY t4.id_task ORDER by t4.vis";*/

    $q="
        SELECT *,t2.title ptitle, t1.title, t1.id , CONCAT(t3.surname,' ',t3.names) login
        FROM
            jobs t1,
            jobs_project t2,
            user t3,
            jobs_act t4
        WHERE
            t1.vis=2 AND
            t1.id_project=t2.id AND
            t1.id_user=t3.id AND
            (t4.id_task=t1.id AND t4.id_type=2)
        GROUP BY t4.id_task ORDER by t4.vis";
         //echo $q;
    $row=$this->db->getAll($q);

    foreach ($row as $key=>$value)
    {
     $time_all=time()-$row[$key]['time_act'];
     $row[$key]['time_m']=round($time_all/60,2)%60;
     $row[$key]['time_h']=round(($time_all/60/60)%24);
     $row[$key]['time_d']=round($time_all/60/60/24);

     if ($row[$key]['date_recomended']>0){
        $date_start=$row[$key]['date_start'];
        $date_recomended=$row[$key]['date_recomended'];
        $date_now=time();
        $row[$key]['progress']=round((($date_now-$date_start)*100)/($date_recomended-$date_start));

     }

    }

       if ($ajax==0)  return $row;

    }

    public function GetTaskEvents($id_task=0,$ajax=0) // показать все события над задачей
    {
        $q9="SELECT *, CONCAT(w2.surname,' ',w2.names) login FROM jobs_act w1, user w2 WHERE w1.id_task={$id_task} AND w2.id=w1.id_user ORDER by w1.time_act DESC";
         //echo $q9;
        $row9=$this->db->getAll($q9);

        foreach ($row9 as $key2=>$val2)
        {
          if ($row9[$key2]['id_type']=='1')  $row9[$key2]['id_type']=lang('Added');
          if ($row9[$key2]['id_type']=='2')  $row9[$key2]['id_type']=lang('Started');
          if ($row9[$key2]['id_type']=='3')  $row9[$key2]['id_type']=lang('Reopened');
          if ($row9[$key2]['id_type']=='4')  $row9[$key2]['id_type']=lang('Finished');

          if ($row9[$key2]['id_type']=='0')  $row9[$key2]['id_type']=lang('Closed');
          if ($row9[$key2]['id_type']=='5')  $row9[$key2]['id_type']=lang('Posted');
          if ($row9[$key2]['id_type']=='7')  $row9[$key2]['id_type']=lang('Accepted');
          if ($row9[$key2]['id_type']=='10')  $row9[$key2]['id_type']=lang('Deleted');
          if ($row9[$key2]['id_type']=='11')  $row9[$key2]['id_type']=lang('Restored');

          if ($row9[$key2]['id_type']=='20') $row9[$key2]['id_type']=lang('Pause');

          if ($row9[$key2]['id_type']=='21')  $row9[$key2]['id_type']=lang('Abandoned');
          if ($row9[$key2]['id_type']=='22')  $row9[$key2]['id_type']=lang('Exhibited checkpoint');
          if ($row9[$key2]['id_type']=='23')  $row9[$key2]['id_type']=lang('Added comment');

          if ($row9[$key2]['id_type']=='24')  $row9[$key2]['id_type']=lang('Set progress');
        }
        $row=$row9;

         if ($ajax==0)  return $row;
    }


    public function GetTaskComments($id_task=0,$ajax=0) // Полчение комментариев
    {
        $q="SELECT *, CONCAT(w2.surname,' ',w2.names) login FROM jobs_comm w1, user w2 WHERE w1.id_task='{$id_task}' AND w2.id=w1.id_user ORDER by w1.date_create DESC";
        $row=$this->db->getAll($q);

        foreach($row as $key=>$val){
			if ($val['attach_files']>0){
				$q="SELECT * FROM files WHERE id='{$val['attach_files']}'";
				$row[$key]['attach_files']=$this->db->getRow($q);
			};
		}


         if ($ajax==0)  return $row;
    }

    public function GetTaskCheck($id_task=0,$ajax=0) // получение контрольных точек для задачи
    {
        $q="SELECT * FROM jobs_check WHERE id_task='{$id_task}' AND vis=1 ORDER BY id";
        $row=$this->db->getAll($q);


         if ($ajax==0)  return $row;
    }

    public function GetTaskInProject($ajax=0) // Показать все задачи для проекта
    {

    $q="SELECT
        COUNT(IF (w1.state =1, w1.state, NULL ) ) AS c1,
        COUNT(IF (w1.state =2, w1.state, NULL ) ) AS c2,
        COUNT(IF (w1.state =3, w1.state, NULL ) ) AS c4,
        COUNT(IF (w1.vis >0, w1.vis, NULL ) ) AS c,


        w2.title, w2.id
        FROM jobs w1, site w2
        WHERE w1.id_project=w2.id GROUP by w2.id";
       // echo $q;
	$row=$this->db->getAll($q);


       if ($ajax==0)  return $row;

    }

    public function GetTaskList($ajax=0) // показать список задач
    {


    if (isset($_GET['project']) && $_GET['project']>0) $project=" AND w1.id_project='{$_GET['project']}'";
    if ($this->archive==0) $events.=" AND w1.vis>0"; else  $events.=" AND w1.vis=0";
    if ($this->state!==0)  $events.=" AND w1.state=".$this->state;
    if ($this->my!==0)  $events.=" AND w1.id_user=".$this->session[child];

            $q="SELECT *, w1.id, w1.vis, w1.id_files, w1.title,  w1.descr,
            w1.create_child, w1.create_admin, w1.id_user,  CONCAT(w2.surname,' ', w2.names) fio, w6.title project,w1.child, w1.state states, w1.parent task_parent
                FROM
                    user_group_list w5,
                    jobs w1
                    LEFT JOIN user w2 ON  w2.id=w1.id_user

                    LEFT JOIN site w6 ON  w1.id_project=w6.id


                WHERE ( w5.id_group=w1.id_group AND w5.id_user='$_SESSION[child]' AND w5.vis=1 ) $project $events ORDER by w1.state,w1.type_task,w1.position  ";
               // echo $q;
        	$row=$this->db->getAll($q);


          foreach($row as $key=>$val) {
                            $vis=lang($vis);


            $row[$key]['col']='color1';
            if ($row[$key]['vis']==1)  { $row[$key]['vis1']='New'; $row[$key]['col']='color1';}
            if ($row[$key]['vis']==2)  { $row[$key]['vis1']='In the process';$row[$key]['col']='color1';}
            if ($row[$key]['vis']==4)  { $row[$key]['vis1']='re-opening';$row[$key]['col']='color1'; }
            if ($row[$key]['vis']==3)  { $row[$key]['vis1']='Finished';$row[$key]['col']='color2';}
            if ($row[$key]['vis']==0)  { $row[$key]['vis1']='Closed';$row[$key]['col']='color0';}
            if ($row[$key]['vis']==20) { $row[$key]['vis1']='Pause';$row[$key]['col']='color0';}

           // if ($row9[$key2]['id_type']=='21')  $row[$key]['row9'][$key2]['id_type']=lang('Снять задачу');

            $row[$key]['scores']='not yet rated';
            $row[$key]['deffs']='not yet rated';
            /*
            $q9="SELECT score FROM jobs_rating w1 WHERE type_score=1 AND id_task='{$row[$key][id]}'";
            $score=$this->db->getOne($q9);
            if ($score=='-1') $row[$key]['scores']=lang('bad');
            if ($score=='0' ) $row[$key]['scores']=lang('normally');
            if ($score=='1' ) $row[$key]['scores']=lang('excellent');

            $q9="SELECT score FROM jobs_rating w1 WHERE type_score=2 AND id_task='{$row[$key][id]}'";
            $deff=$this->db->getOne($q9);
            if ($deff=='3') $row[$key]['deffs']=lang('сложно');
            if ($deff=='2') $row[$key]['deffs']=lang('средне');
            if ($deff=='1') $row[$key]['deffs']=lang('легко');
             */


         if ($row[$key][id_user]=='0') $row[$key]['classOn'][9]='On';

         if ($row[$key][id_user]>'0'){

           if ($row[$key][id_user]==$_SESSION[child])
             {
               if ($row[$key][id_user]!=='0')  $row[$key]['classOn'][21]='On';
               if ($row[$key]['vis']=='2'){$row[$key]['classOn'][2]='On'; $row[$key]['classOn'][20]='On';}
               if ($row[$key]['vis']=='1' || $row[$key]['vis']=='20')$row[$key]['classOn'][1]='On';
               if ($row[$key]['vis']=='3')$row[$key]['classOn'][3]='On';
              }

            }
            if ($_SESSION[child]==$row[$key][create_child] OR $_SESSION[child]==$row[$key][create_admin] )
              if ($row[$key]['vis']>'0') $row[$key]['classOn'][0]='On';

           if ($row[$key][id_user]==$_SESSION[child] OR $_SESSION[child]==$row[$key][create_child] OR $_SESSION[child]==$row[$key][create_admin] ) if ($row[$key]['vis']=='3')$row[$key]['classOn'][3]='On';


             if ($_SESSION['child']==$row[$key]['create_child'] OR $_SESSION['child']==$row[$key]['create_admin']){
                  if ($row[$key][id_user]!=='0')  $row[$key]['classOn'][21]='On';
                 if ($row[$key]['vis']==0) { $row[$key]['classOn'][4]='On'; }
                 if ($row[$key]['vis']==1 OR $row[$key]['vis']==1)  $row[$key]['classOn'][0]='On';
                 if ($row[$key]['vis']=='2'){$row[$key]['classOn'][2]='On'; $row[$key]['classOn'][20]='On';}
                 $row[$key]['classOn'][10]='On';
              }
        }
        if ($ajax==0)  return $row;
    }

 public function GetTaskOne($id_task=0,$ajax=0) // открыть одну задачу - конкретную
    {



    if (isset($_GET['project']) && $_GET['project']>0) $project=" AND w1.id_project='{$_GET['project']}'";


            $q="SELECT *, w1.id, w1.vis, w1.id_files, w3.title title_parent , w1.title, w3.id id_parent, w1.descr,
            w1.create_child, w1.create_admin, w1.id_user, CONCAT(w4.surname,' ',w4.names)  w4fio,  CONCAT(w2.surname,' ',w2.names) fio, w6.title project,w1.child, w1.progress, w1.date_start, w1.date_recomended
                FROM
                    user_group_list w5,
                    jobs w1
                    LEFT JOIN user w2 ON  w2.id=w1.id_user
                    LEFT JOIN jobs w3 ON  w1.parent=w3.id
                    LEFT JOIN user w4 ON  w4.id=w1.create_child
                    LEFT JOIN site w6 ON  w1.id_project=w6.id


                WHERE w1.id='$id_task' AND ( w5.id_group=w1.id_group AND w5.id_user='$_SESSION[child]' AND w5.vis=1 )  $project ORDER by  w1.vis,w1.position ";
               // echo $q;
        	$row=$this->db->getAll($q);


          foreach($row as $key=>$val) {
                            $vis=lang($vis);

    if ($row[$key]['date_recomended']>0){
        $date_start=$row[$key]['date_start'];
        $date_recomended=$row[$key]['date_recomended'];
        $date_now=time();
        $row[$key]['progress']=round((($date_now-$date_start)*100)/($date_recomended-$date_start));

     }

                if ($row[$key]['id_files']=='0'){}else {
                  $files=img_to_path($row['id_files'],'',3);
                  if (count($files)>0) foreach ($files as $files) $row[$key]['id_files'].=" <li><a href='file.php?f={$files[0]}'>{$files[1]}</a></li> ";

                 }



            $row[$key]['col']='color1';
            if ($row[$key]['vis']==1)  { $row[$key]['vis1']='New'; $row[$key]['col']='color1';}
            if ($row[$key]['vis']==2)  { $row[$key]['vis1']='In the process';$row[$key]['col']='color1';}
            if ($row[$key]['vis']==4)  { $row[$key]['vis1']='re-opening';$row[$key]['col']='color1'; }
            if ($row[$key]['vis']==3)  { $row[$key]['vis1']='Finished';$row[$key]['col']='color2';}
            if ($row[$key]['vis']==0)  { $row[$key]['vis1']='Closed';$row[$key]['col']='color0';}
            if ($row[$key]['vis']==20) { $row[$key]['vis1']='Pause';$row[$key]['col']='color0';}

           // if ($row9[$key2]['id_type']=='21')  $row[$key]['row9'][$key2]['id_type']=lang('Снять задачу');

            $row[$key]['scores']='Нет оценки';
            $row[$key]['deffs']='Нет оценки';
           /*
            $q9="SELECT score FROM jobs_rating w1 WHERE type_score=1 AND id_task='{$row[$key][id]}'";
            $score=$this->db->getOne($q9);
            if ($score=='-1') $row[$key]['scores']=lang('плохо');
            if ($score=='0' ) $row[$key]['scores']=lang('нормально');
            if ($score=='1' ) $row[$key]['scores']=lang('отлично');

            $q9="SELECT score FROM jobs_rating w1 WHERE type_score=2 AND id_task='{$row[$key][id]}'";
            $deff=$this->db->getOne($q9);
            if ($deff=='3') $row[$key]['deffs']=lang('сложно');
            if ($deff=='2') $row[$key]['deffs']=lang('средне');
            if ($deff=='1') $row[$key]['deffs']=lang('легко');
           */


         if ($row[$key][id_user]=='0') $row[$key]['classOn'][9]='On';

         if ($row[$key][id_user]>'0'){

           if ($row[$key][id_user]==$_SESSION[child])
             {
               if ($row[$key][id_user]!=='0')  $row[$key]['classOn'][21]='On';
               if ($row[$key]['vis']=='2'){$row[$key]['classOn'][2]='On'; $row[$key]['classOn'][20]='On';}
               if ($row[$key]['vis']=='1' || $row[$key]['vis']=='20')$row[$key]['classOn'][1]='On';
               if ($row[$key]['vis']=='3')$row[$key]['classOn'][3]='On';
              }

            }
            if ($_SESSION[child]==$row[$key][create_child] OR $_SESSION[child]==$row[$key][create_admin] )
              if ($row[$key]['vis']>'0') $row[$key]['classOn'][0]='On';

           if ($row[$key][id_user]==$_SESSION[child] OR $_SESSION[child]==$row[$key][create_child] OR $_SESSION[child]==$row[$key][create_admin] ) if ($row[$key]['vis']=='3')$row[$key]['classOn'][3]='On';


             if ($_SESSION['user_id']==$row[$key]['create_child'] OR $_SESSION['user_id']==$row[$key]['create_admin']){

                 if ($row[$key]['id_user']!=='0')  $row[$key]['classOn'][21]='On';
                 if ($row[$key]['vis']==0) { $row[$key]['classOn'][4]='On'; }
                 if ($row[$key]['vis']==1 OR $row[$key]['vis']==1)  $row[$key]['classOn'][0]='On';
                 if ($row[$key]['vis']=='2'){ $row[$key]['classOn'][2]='On'; $row[$key]['classOn'][20]='On'; }
                 if ($row[$key]['vis']=='20'){ $row[$key]['classOn'][1]='On'; }
                 $row[$key]['classOn'][10]='On';
              }


        $q9="SELECT *, CONCAT(w2.surname,' ',w2.names) login, CONCAT(w2.surname,' ',w2.names) fio FROM jobs_act w1, user w2 WHERE w1.id_task={$row[$key]['id']} AND w2.id=w1.id_user ORDER by w1.time_act DESC";
         //echo $q9;
        $row9=$this->db->getAll($q9);
        $row[$key]['row9']=$row9;
        foreach ($row9 as $key2=>$val2)
        {


          if ($row9[$key2]['id_type']=='1')  $row[$key]['row9'][$key2]['id_type']=lang('Added');
          if ($row9[$key2]['id_type']=='2')  $row[$key]['row9'][$key2]['id_type']=lang('Started');
          if ($row9[$key2]['id_type']=='3')  $row[$key]['row9'][$key2]['id_type']=lang('Reopened');
          if ($row9[$key2]['id_type']=='4')  $row[$key]['row9'][$key2]['id_type']=lang('Finished');

          if ($row9[$key2]['id_type']=='0')  $row[$key]['row9'][$key2]['id_type']=lang('Closed');
          if ($row9[$key2]['id_type']=='5')  $row[$key]['row9'][$key2]['id_type']=lang('Posted');
          if ($row9[$key2]['id_type']=='7')  $row[$key]['row9'][$key2]['id_type']=lang('Accepted');
          if ($row9[$key2]['id_type']=='10')  $row[$key]['row9'][$key2]['id_type']=lang('Deleted');
          if ($row9[$key2]['id_type']=='11')  $row[$key]['row9'][$key2]['id_type']=lang('Restored');

          if ($row9[$key2]['id_type']=='20')  $row[$key]['row9'][$key2]['id_type']=lang('Pause');

          if ($row9[$key2]['id_type']=='21')  $row[$key]['row9'][$key2]['id_type']=lang('Abandoned');

          if ($row9[$key2]['id_type']=='22')  $row[$key]['row9'][$key2]['id_type']=lang('Exhibited checkpoint');
          if ($row9[$key2]['id_type']=='23')  $row[$key]['row9'][$key2]['id_type']=lang('Added comment');
        }

        $q10="SELECT *, CONCAT(w2.surname,' ',w2.names) login, CONCAT(w2.surname,' ',w2.names) fio FROM jobs_comm w1, user w2 WHERE w1.id_task={$row[$key]['id']} AND w2.id=w1.id_user ORDER by w1.date_create DESC";
        $row[$key]['comment']=$this->db->getAll($q10);

        }

        if ($ajax==0)  return $row;
    }



    public function addMessage() // добавить сообщение
    {

      $q="INSERT INTO `jobs_comm` (`id` ,
`id_task` ,
`id_user` ,
`body` ,
`parent` ,
`vis` ,
`date_create`
)
VALUES (NULL , '{$_POST[id_task]}', '{$_SESSION[child]}', '{$_POST[body]}', '{$_POST[parent]}', '1', '".time()."')";
      $this->db->query($q);

        $q__="INSERT INTO `jobs_act` (`id` ,`id_task`, `time_act` ,`id_type` ,   `id_user` , `vis` )
        VALUES (NULL ,'{$_POST[id_task]}', '".time()."', '23', '{$_SESSION['child']}', '1')";
        $this->db->query($q__);

      echo "jAlert('Ok',2);";

    }

    public function setAction() // операция над задачей. В самом конце функции передается дополнительная запись, фиксация события над операцией
    {
      $vis=array(
        '4'=>array(0,0),
        '10'=>array(0,10),
        '1'=>array(1,11),
        '2'=>array(0,7),
        '3'=>array(2,2),
        '9'=>array(2,3),
        '4'=>array(3,4),
        '20'=>array(20,20),
        '21'=>array(21,21)
      ); // 24 - progress

      $id_task=$this->post[id_task];

      /*
    <li><a href='#' class='task4 task_comm {$row.classOn.4}' val='1'>{lang s='Восстановить'}</a></li>
    <li><a href='#' class='task0 task_comm {$row.classOn.0}' val='10'>{lang s='Удалить'}</a></li> +
    <li><a href='#' class='task2 task_comm {$row.classOn.2}' val='4'>{lang s='Закончить'}</a></li>
    <li><a href='#' class='task1 task_comm {$row.classOn.1}' val='3'>{lang s='Начать'}</a></li> +
    <li><a href='#' class='task3 task_comm {$row.classOn.3}' val='9'>{lang s='Начать заново'}</a></li> +
    <li><a href='#' class='task9 task_comm {$row.classOn.9}' val='2'>{lang s='Принять'}</a></li> +

    <li><a href='#' class='task11 task0{$row.id} task_comm {$row.classOn.11}' val='11'>{lang s='Пауза'}</a></li>

    */

    $q="SELECT * FROM jobs WHERE id='$id_task'";
    $task_row=$this->db->getRow($q);
    $id_user=$task_row['id_user'];
    $task_child=$task_row['id_user'];
   // $task_child=$task_row['id_user'];
    $error=0;

  if ($this->post[id_comm]==1 ){
            $q="UPDATE jobs SET vis='{$vis[$this->post[id_comm]][0]}', state='3' WHERE id='{$this->post[id_task]}'";
            $this->db->query($q);
            echo " $('.task4{$id_task}').hide(); $('.task0{$id_task}').show(); jAlert('".lang('Successfully')."',2);"; } // reset

  if ($this->post[id_comm]==3 ){
      $q="SELECT COUNT(id) count_id, date_start FROM jobs WHERE id_user='{$this->session['child']}' AND vis=2";
      $row__=$this->db->getRow($q);
      $count=$row__['count_id'];

      $date_start=$task_row['date_start'];

        if ($count>0) { echo "jAlert('".lang('To start new task please close the current')."',1);"; $error=1;}
        else
        {
          if ($date_start=='0') $q="UPDATE jobs SET vis='{$vis[$this->post[id_comm]][0]}', state='2', date_start='".time()."' WHERE id='{$this->post[id_task]}'";
          else $q="UPDATE jobs SET vis='{$vis[$this->post[id_comm]][0]}', state='2' WHERE id='{$this->post[id_task]}'";

          $this->db->query($q);
          echo "$('.task1{$id_task}').hide(); $('.task2{$id_task}').show(); $('.task20{$id_task}').show(); jAlert('".lang('Successfully')."3',2);";
        }
      }  // start

  if ($this->post[id_comm]==9 ){
         $q="SELECT COUNT(id) FROM jobs WHERE id_user='{$this->session['child']}' AND vis=2";
      $count=$this->db->getOne($q);
        if ($count>0) {echo "jAlert('".lang('To start new task please close the current')."',1);"; $error=1;}
        else
        {  $q="UPDATE jobs SET vis='{$vis[$this->post[id_comm]][0]}', state='2' WHERE id='{$this->post[id_task]}'"; $this->db->query($q); echo "$('.task3{$id_task}').hide(); $('.task2{$id_task}').show();$('.task20{$id_task}').show();  jAlert('".lang('Successfully')."',2);";
        } }   // restart

  if ($this->post[id_comm]==10){

      if ($task_row[id_user]==0 OR $task_row[vis]==3 ){  $q="UPDATE jobs SET vis='{$vis[$this->post[id_comm]][0]}', state='4' WHERE id='{$this->post[id_task]}'"; $this->db->query($q); echo " $('.task0{$id_task}').hide(); $('.task4{$id_task}').show(); jAlert('".lang('Successfully')."',2);";  }
        else { echo "jAlert('".lang('Finish or leave the task to remove it')."',1);"; $error=1;}   } // delete


  if ($this->post[id_comm]==20) // pause
      {

                $q="UPDATE jobs SET vis='{$vis[$this->post[id_comm]][0]}', state='2' WHERE id='{$this->post[id_task]}'"; $this->db->query($q);
                echo "$('.task2{$id_task}').hide(); $('.task20{$id_task}').hide(); $('.task1{$id_task}').show();  jAlert('".lang('Successfully')."',2);";
        
      }

  if ($this->post[id_comm]==21) // kick
      {
        if ($task_row[vis]!=='2') {
        $q="UPDATE jobs SET vis='1', id_user=0 WHERE id='{$this->post[id_task]}'"; $this->db->query($q);
         echo "$('.task21{$id_task}, .task4{$id_task}, .task3{$id_task}, .task1{$id_task}').hide();  $('.task9{$id_task}').show();
         jAlert('".lang('Successfully')."',2);";
         }
        else
         { $error=1;
           echo " jAlert('".lang("Task can not be removed while it's running")."',1);";
         }
      }


  if ($this->post[id_comm]==2){  // accept


            if ($id_user==0){
              $q="UPDATE jobs SET id_user='{$this->session['child']}' WHERE id='{$id_task}'";
              $this->db->query($q);
              //echo "$()";

               echo "$('.task9{$id_task}').hide(); $('.task1{$id_task}, .task22{$id_task}').show(); jAlert('".lang('Successfully')."',2);";

            }
            else { $error=1; echo " jAlert('".lang('Error! The task is already running someone')."',1);"; //error
          }
        }

  if ($this->post[id_comm]==4){  // end

    //  $q="SELECT COUNT(id) FROM jobs WHERE id_task='{$id_task}' vis=1"; // Нельзя закрыть задачу если у нее не закрыты подзадачи
      $q="SELECT COUNT(id) FROM jobs_check WHERE id_task='{$id_task}' AND value=0 AND vis=1";
      $count=$this->db->getOne($q);
      if ($count>0) { $error=1; echo "jAlert('".lang('Error. Complete all Checkpoints')."',1);"; }
      else {
        $q="UPDATE jobs SET vis=3, state='3', date_finish='".time()."' WHERE id='{$this->post[id_task]}'";
        $this->db->query($q);
        echo "$('.task2{$id_task}').hide();$('.task20{$id_task}').hide(); $('.task3{$id_task}').show();
        jAlert('".lang('Successfully')."4',2);";
      }
  }
  if ($error==0) $this->insertItem($vis[$this->post['id_comm']][1]);
}


}
?>