<?
class Game extends database
{
/*
.block1{ background-position-x: -192px; background-position-y: 0px } /* земля  с травой
.block2{ background-position-x: -128px; background-position-y: 0px } /* заемля 
.block3{ background-position-x: -64px; background-position-y: 0px } /* камни 1
.block4{ background-position-x: 0px; background-position-y: -64px } /* камни 2*
.block5{ background-position-x: -128px; background-position-y: -128px } /* уголь *

.block6{ background-position-x: -510px; background-position-y: -192px } /* дерево*
.block7{ background-position-x: -574px; background-position-y: -256px } /* елка *

.block25{ background-position-x: -64px; background-position-y: -128px } /* металл *
.block50{ background-position-x: -256px; background-position-y: -128px } /*  золото *
.block100{ background-position-x: 0px; background-position-y: -192px } /* бриллиант *
*/    
    public $blocks;
    public $stat;
    public $smarty;
    // объекты, грузятся в construct
    public $objects=array();
    // направление игрока, в случае если передается слово, заменять цифрами
    public $direction=Array( 
        'up'=>1, 'left'=>2,'right'=>3,'down'=>4);
     // список монстров   
    public $monsters;
    // игрок и XYZ
    public $xyz;
        
    // Параметры Вниамание если какой из типов не активен! то удары берутся друг у друга. Например, если активна только лопата, а дубина не активна то у лопаты берется параметр и плюсуется к удару. И наоборот. Можно купить одно из двух и с ним бегать. 
    // Грузится массив. id_record, id_object, object_type, power_kick, power_dig
            
    // Параметры удара орудием труда
    public $power_dig;   
    // Параметра удара оружием
    public $power_kick;   

    // Значения ударов    
    public $kick_dm=0;
    public $dig_dm=0; 
 
    // События для случайных задач    
    public $task_events; 
    public function __construct(){
                 
        parent :: __construct();
        $this->xyz=$this->getRow("SELECT * FROM  map_objects_xy t1, gamer t2  WHERE (t1.id_user='{$_SESSION['user_id']}' AND t1.id_object=0) AND t1.vis>0 AND t2.id_user=t1.id_user");
        $this->blocks=$this->getAll("SELECT * FROM game_block WHERE vis>0 ");   
        $this->objects=$this->getAll("SELECT *, life_full as life FROM game_object WHERE vis>0 ORDER by type");  
        $this->monsters=$this->getAll("SELECT * FROM monster WHERE vis>0 AND y<='{$this->xyz['y']}'");        
   //     print_r($this->monster); exit;
        //----выгружаем активную лопату
        $power_dig=$this->getRow("SELECT * FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND object_act=1 AND object_type=6");
        $power_kick=$this->getRow("SELECT * FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND object_act=1 AND object_type=7");
        
        if (empty($power_dig)) 
        { 
            if (!empty($power_kick)) 
            { 
                $this->power_dig=$power_kick; 
                $this->dig_dm=$power_kick['power_dig']; 
            } 
        } else 
        { 
            $this->power_dig=$power_dig;
            $this->dig_dm=$this->power_dig['power_dig'];  
        }
            
        if (empty($power_kick)) 
        {  
            if (!empty($power_dig))
            { 
                $this->power_kick=$power_dig; 
                $this->kick_dm=$power_dig['power_kick']; 
            } 
        } else {
            $this->power_kick=$power_kick; 
            $this->kick_dm=$this->power_kick['power_kick']; 
        }
 /*       
echo " удар".$this->kick_dm; 
echo " копать".$this->dig_dm;   exit; */ 
        
        $this->smarty = new Smarty();

        $this->smarty->template_dir = $_SERVER['DOCUMENT_ROOT'].'/s_templates/';
        $this->smarty->compile_dir = $_SERVER['DOCUMENT_ROOT'].'/s_templates_c/';
        
        $this->task_events=array(
            1=>array(
                'id'=>1,
                'title'=>'Пройди дистанцию ', 
                'post_fix'=>' метр.',
                'field'=>'distance', 
                'max_random'=>1000),
            2=>array(
                 'id'=>2,
                'title'=>'Увеличить максимум глубины ', 
                'post_fix'=>'метр.',
                'field'=>'deep', 
                'max_random'=>10),
            3=>array(
                 'id'=>3,
                'title'=>'Ударить монстра', 
                'post_fix'=>'раз(a)',
                'field'=>'kick_monster', 
                'max_random'=>1000),
            4=>array(
                 'id'=>4,
                'title'=>'Убить монстра', 
                'post_fix'=>'раз(a)',
                'field'=>'kill_monster', 
                'max_random'=>100)
,
            5=>array(
                 'id'=>5,
                'title'=>'Установить блок', 
                'post_fix'=>'шт',
                'field'=>'block_set', 
                'max_random'=>50) 
,
            6=>array(
                 'id'=>6,
                'title'=>'Разбить блок', 
                'post_fix'=>'раз(a)',
                'field'=>'block_break', 
                'max_random'=>50)  
,
            7=>array(
                'id'=>7,
                'title'=>'Продать блок', 
                'post_fix'=>'шт.',
                'field'=>'block_sell', 
                'max_random'=>50)   
,
            8=>array(
                'id'=>8,
                'title'=>'Использовать предмет', 
                'post_fix'=>'раз(a)',
                'field'=>'object_use', 
                'max_random'=>100)                 
        );
    }   
    
public function SetOnline(){
        $this->update('gamer',array('date_online'=>date("Y-m-d H:i:s")), " WHERE id_user='{$_SESSION['user_id']}'");
        
    }
function KillUser($p=array()) // x,y,z = место смерти, смерть игрока, обнуление инвентаря и т.д.
{
    $p['json']=(int)$p['json'];
    

    $p['id_user']=$_SESSION['user_id'];
    $p['x']=$this->xyz['x']; 
    $p['y']=$this->xyz['y'];
    $p['z']=$this->xyz['z'];  
   // $p['id_user']
    
//-----  сбрасываем инвентарь    если есть такой
    $q="SELECT SUM(count) FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}'";
    $count=$this->getOne($q);
    if ($count>0){
        $id_chest=(int)$this->insertdata('map_objects_xy',
        array(
            (x)=>$p['x'],
            (y)=>$p['y'],
            (z)=>$p['z'], 
            (owner_object)=>$_SESSION['user_id'],
            (id_object)=>9               
        ));
    
 /*
        $q="UPDATE gamer_inventory SET count=0 WHERE id_user='{$_SESSION['user_id']}' AND object_act=1";
        $this->query($q);
*/            
        $q="UPDATE gamer_inventory SET id_user=0, id_chest='{$id_chest}', object_act=0 WHERE id_user='{$_SESSION['user_id']}'";
        $this->query($q);
    }
//-----     
//----- сбрасываем 
    $this->SetIndicatorValue(array('reset'=>'1')); 
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;   
}    
public function Walk()//шаг игрока 
{ 

    // получаем текущую позицию игрока
    $q="SELECT * FROM  map_objects_xy t1, gamer t2  WHERE (t1.id_user='{$_SESSION['user_id']}' AND t1.id_object=0) AND t1.vis>0 AND t2.id_user=t1.id_user";
    $row=$this->db->getRow($q); 
    
    $x_temp=$row['x'];
    $y_temp=$row['y'];
        
    if ($row['y']>0) 
    { 
        // уменьшаем кислород
        $indicator_now=$this->SetIndicatorValue(array('indicator'=>'oxygen','value'=>'-0.1')); 
        // если кислорода мало уменьшаем жизнь
        if ( $indicator_now['row']['oxygen']<=0) { $indicator_now=$this->SetIndicatorValue(array('indicator'=>'heart','value'=>-0.05)); //echo (-0.05*$row['y']);  
            
        }
        
        
    }
    else $indicator_now=$this->SetIndicatorValue(array('indicator'=>'oxygen','reset'=>'1')); // возобновляем кислород

 
 // загружаем массив с блоками Сейчас 5х 9y   
$row_pole=$this->MapLoad5x7(1); 
  
//-------------------------------------
    
    if (isset($_REQUEST['direction']))  $direction=$_REQUEST['direction']; // направление хотьбы  
    $fall=0;
    $kill=0;  
    if ($direction=='up') // вверх
    { 
        $field=$this->FieldCheck($row_pole, $row['x'], $row['y']-1,$direction); // проверяем блок по которому хотим пройти. функция так же разбивает блок
        // если res==0 препятсвий нет, блок разбит или пустой
        if (isset($field['stop_inventory'])) { $message='Инвентарь полон';} else
        if (isset($field['stop'])) { $message='Нельзя разбить';} else{
            
        if (isset($field['anti-block'])) { $message='Этот блок не разрушить';} else
        if (isset($field['dm'])){ $message='Блок поврежден';}
        else
        if ($field['res']==0) { $row['y']--;  } else {
             
            $q="SELECT count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object=1 AND count>0";   
            $row_inventory= (int)$this->getOne($q);
            if ($row_inventory==0)   $message='Нужно поставить лестницу'; 
            else{
                $this->SetObject(array('x'=>$row['x'],'y'=>$row['y'], 'direction'=>$_REQUEST['direction'],'id_object'=>1));
                $row['y']--;
            } 
        }
        }

    }
    if ($direction=='left')     { 

        // влево
            $field=$this->FieldCheck($row_pole, $row['x']-1, $row['y'],$direction); 
            if (isset($field['stop_inventory'])) { $message='Инвентарь полон';} else
            if (isset($field['stop'])) { $message='Нельзя разбить';} else{
                
            if (isset($field['anti-block'])) { $message='Этот блок не разрушить';} 
            else
                if (isset($field['dm'])){ $message='Блок поврежден';}
                else 
                    if ($field['res']==0) { $row['x']--; $row['y']+=$field['fall']-1;  } 
                    else { $fall=1;  $fall_x=$row['x']-1;  $fall_y=$row['y']; }
            }
            
            
        
    }    
    
    if ($direction=='right') { //вправо

            $field=$this->FieldCheck($row_pole, $row['x']+1, $row['y'],$direction); 
            if (isset($field['stop_inventory'])) { $message='Инвентарь полон';} else
            if (isset($field['stop'])) { $message='Нельзя разбить';} else{
                
                if (isset($field['anti-block'])) { $message='Этот блок не разрушить';} else
                if (isset($field['dm'])){ $message='Блок поврежден';}
                else if ($field['res']==0) { $row['x']++; $row['y']+=$field['fall']-1;  } else { $fall=1; $fall_x=$row['x']+1;  $fall_y=$row['y'];    }
            }

    }
    if ($direction=='down'){   //вниз     
        $field=$this->FieldCheck($row_pole, $row['x'], $row['y']+1,$direction); 
        if (isset($field['stop_inventory'])) { $message='Инвентарь полон';} else 
        if (isset($field['stop'])) { $message='Нельзя разбить';} else
        {
         if (isset($field['anti-block'])) { $message='Этот блок не разрушить';} else
        if (isset($field['dm'])){ $message='Блок поврежден';}
        else if ($field['res']==0) $row['y']+=$field['fall'];  else { $fall=1;   $fall_x=$row['x'];  $fall_y=$row['y']; }
        }
    } 
    
    if ($fall==1) { //$message='Произошло падение'; 
        
        $indicator_now=$this->SetIndicatorValue(array('indicator'=>'heart','value'=>'-0.10')); // отправляем минус к кислороду и тут же смотрим
        $res=$this->FindDeep(array('x'=>$fall_x,'y'=>$fall_y)); // если падение то проверяем глубину
        if ($res[(error)]==1) { $kill=1; $message="Вы разбились";} // если упал на 10 кубиков то смерть, инвентарь потерян :(
        else { $row['y']=$res['y']-1; $row['x']=$fall_x; $message="Вы упали, здоровье уменьшилось"; } // если нет то прсто уменьшаем
        
    }
    
//print_r( $indicator_now);

    if ((isset($indicator_now) && $indicator_now['row']['heart']<=0) || $kill==1){
        
        $this->KillUser(array('x'=>$row['x'],'y'=>$row['y'])); 
        
        $row['x']=$row['x_home'];
        $row['y']=0;         
        
    } 

  
    //if ($fall==0) $message="Мнстр"; нужно смотреть на сколько изменилось y b x если много то монстр не появляется
    if (($row['y']!=$y_temp || $row['x']!=$x_temp) && $row['y']>0){ 
        $rand=rand(0,30); //$message="Ищем монстра  $rand";
        if ($rand==0) { $message="Мoнстр! Cразись или беги";
            $this->CreateMonster(array('x'=>$row['x'],'y'=>$row['y']));   
            $row['y']=$y_temp; $row['x']=$x_temp;
        }
    }
$FindMonster=$this->FindMonster(array('x'=>$row['x'],'y'=>$row['y'])); 
//
if ($FindMonster['monster']==1) { 
    $message='Монстр ударил тебя';
  //  print_r($FindMonster);
    if ($FindMonster[0]['row']['heart']<=0) { 
        // старые координаты
        $this->KillUser(array('x'=>$row['x'],'y'=>$row['y']));
        
        $row['x']=$row['x_home'];
        $row['y']=0;               
       
        $message='Монстр убил тебя';
    }
}
//-----Удаляем предыдущее место

if ($row['y']!=$y_temp || $row['x']!=$x_temp) 
{ // движение произошло
    $this->SetGamerStat(array('stat'=>'distance','value'=>1));
    if ($row['y']>$y_temp){
        $y_max=$this->getOne("SELECT deep FROM gamer_stat WHERE id_user='{$_SESSION['user_id']}'");
        if ($row['y']>$y_max) { $this->SetGamerStat(array('stat'=>'deep','value'=>$row['y'],'set'=>1)); $message=$message.' Достигнута новая глубина!'; }
    }
}
    $arr=array('x'=>(int)$row['x'], 'y'=>(int)$row['y'], 'id_user_direction'=>$this->direction[$direction]);
    
    $this->update('map_objects_xy',$arr,"WHERE id_user=? AND id_object=0",array(intval($_SESSION['user_id'])));   
    
//---- рисуем карту    
    $q="INSERT INTO gamer_map (id_user, x,y,z) VALUES ('{$_SESSION['user_id']}', '{$row['x']}', '{$row['y']}', '0') ON DUPLICATE KEY UPDATE y ='{$row['y']}'";
    $this->query($q);
//----
    $row['message']=isset($message)?$message:0; // сообщения которые скопились ошибки и напоминания

    if (isset($_POST['json']) || isset($_GET['json']) ){  
        $result =  json_encode($row);
        echo "jsonpCallback(".$result.")";
    } 
    else return $row;
    
    
}
public function WalkFall($map,$x,$y) // проверяем не упадет ли игрок  
{    
  
   $row_down_1=$this->FieldSearch($map,$x,$y+1); // получаем одну строчку по координатам
    if  ($row_down_1['id_block']>0) // проверяем следующий квадрат
    { return array('res'=>0, 'fall'=>1); }//передаем что идти можно и на сколько спустится } 
    else { 
        $id_object=$this->FieldSearchObjects(array('x'=>$x,'y'=>$y+1,'id_object'=>1));
         if ($id_object['type']==2) return array('res'=>0,'fall'=>1); // проверяем на объект лестница
        else {
            $row_down_2=$this->FieldSearch($map,$x,$y+2);        
            if  ($row_down_2['id_block']>0) return array('res'=>0, 'fall'=>2); 
            else {
                $id_object=$this->FieldSearchObjects(array('x'=>$x,'y'=>$y+2,'id_object'=>1));
                if ($id_object['type']==2) return array('res'=>0,'fall'=>2); else {
                    $row_down_3=$this->FieldSearch($map,$x,$y+3);        
                    if  ($row_down_3['id_block']>0) return array('res'=>0, 'fall'=>3);
                    else {
                        $id_object=$this->FieldSearchObjects(array('x'=>$x,'y'=>$y+3,'id_object'=>1));
                        if ($id_object['type']==2) return array('res'=>0,'fall'=>3); else 
                        return array('res'=>1, 'fall'=>0);
                    }
                }
            }
        }
    }      
} 

public function BlockDestruction($p=array('id_block'=>1)) // старая версия - разбивается блок - процесс
{
 //  sleep($this->blocks[$p['id_block']]['dm']);  
    $q="INSERT INTO gamer_stat_item (id_user, id_block,count) VALUES ('{$_SESSION['user_id']}', '".intval($p['id_block'])."',1) ON DUPLICATE KEY UPDATE  count=count+1";
    $this->query($q);
           $this->SetGamerStat(array('stat'=>'block_break'));
}
public function FieldCheck($map=array(),$x=0,$y=0, $direction=0,$x_old=0,$y_old=0)
{
/* 
    - переменные принимаемые функцией. $array: массив с полем. $x=0,$y=0 - будущая точка, $current_xy - текущая точка
    - проверка влево и вправо - если там пустая клетка - то идет запрос на три поля вниз
    
*/   
    // получаем текущую позицию игрока
    $q="SELECT * FROM  map_objects_xy t1, gamer t2  WHERE (t1.id_user='{$_SESSION['user_id']}' AND t1.id_object=0) AND t1.vis>0 AND t2.id_user=t1.id_user";
    $xy=$this->db->getRow($q); 
    
    
    
$row=$this->FieldSearch($map,$x,$y,$z);

 /*       if ($direction=='up'){  if ($row['id_object']==0){ } else  if ($row['id_object']>2) {}   break;}
        if ($direction=='down'){ break;}
        if ($direction=='left'){ break;}
        if ($direction=='right'){ break;}   
             
*/    
    
if ($row['id_block']==0) // ячейка без блока
{ 
    if ($direction=='up')
    {

        
        if (count($this->FieldSearchObjects(array('x'=>$x,'y'=>$y+1,'id_object'=>1)))>0) return array('res'=>0, 'fall'=>0); 
        else return array('res'=>1, 'fall'=>0);
       
    }
    if ($direction=='down')  
    { 
        // идем вниз, если первый блок пустой проверяем второй блок если тот не пустой оставливаемся на первом больше трех проверок то  возвращаем игрока назад "/ еще не сделано и "восстанавливаем блок под ним
        if (count($this->FieldSearchObjects(array('x'=>$x,'y'=>$y,'id_object'=>1)))>0) return array('res'=>0,'fall'=>1); else 
        return $this->WalkFall($map,$x,$y);

    }                               
    if ($direction=='left')  
    { 
        // идем влево, если первый блок пустой проверяем второй блок если тот не пустой оставливаемся на первом больше трех проверок то  возвращаем игрока назад "/" и восстанавливаем блок под ним

        
        if (count($this->FieldSearchObjects(array('x'=>$x,'y'=>$y,'id_object'=>1)))>0){ return array('res'=>0,'fall'=>1); } else 
         return $this->WalkFall($map,$x,$y);
         //return array('res'=>0,'fall'=>1); 

    }   
    if ($direction=='right')  
    { 
        // идем вправо, если первый блок пустой проверяем второй блок если тот не пустой оставливаемся на первом больше трех проверок то  возвращаем игрока назад "/" и восстанавливаем блок под ним
        if (count($this->FieldSearchObjects(array('x'=>$x,'y'=>$y,'id_object'=>1)))>0) return array('res'=>0,'fall'=>1); else 
        return $this->WalkFall($map,$x,$y);

    }     
} 


if ($row['id_block']>0)    // любое препядствие
{ 
  //  print_r($xy);
    if ($xy['inventory']>=$xy['inventory_full']) return array('stop_inventory'=>1);
      if (($xy['x_min']>$x || $x>$xy['x_max']) || $y<1) return array('stop'=>1);
    //echo "{$xy['x_min']}>$x || $x>{$xy['x_max']}";
    // проверяем целостность блока
    if ($row['id_block']==11 || $row['id_block']==1){ return array('anti-block'=>1); } 
    if ($row['dm']>0){
        // блок жив, долбим
        //$this->update('map_xy',array("dm"=>"dm-0.5")," WHERE x={$x} AND y={$y} AND z=0");
        $dig_dm=0.5+$this->dig_dm;
        $q="UPDATE map_xy SET dm=dm-$dig_dm WHERE  x={$x} AND y={$y} AND z=0";//echo $q;
        $this->query($q);
        
        $this->SetLifeObject(array((id_record)=>$this->power_dig['id'],(value)=>'-0.5'));
    }   
       
        //echo $q;
        // echo "x={$x} AND y={$y} AND z=0";
        $q="SELECT dm FROM map_xy WHERE  x='{$x}' AND y='{$y}' AND z=0";
        $dm=$this->getOne($q);
        if ($dm>0) return array('dm'=>1); 
  

        $this->SetInventoryCount(array('id_block'=>$row['id_block'], 'count'=>1)); // увеличиваем в инвентаре
        
        if ($direction=='up')     
        {   
            $this->BlockDestruction(array('id_block'=>$row['id_block']));  
            $this->FieldDestroy(array('x'=>$x,'y'=>$y));  
            
            if (count($this->FieldSearchObjects(array('x'=>$x,'y'=>$y+1,'id_object'=>1)))>0) return array('res'=>0, 'fall'=>0); 
            else return array('res'=>1, 'fall'=>0);
            
        }    
        if ($direction=='left')   {  $this->BlockDestruction(array('id_block'=>$row['id_block']));     $this->FieldDestroy(array('x'=>$x,'y'=>$y));  return $this->WalkFall($map,$x,$y); }            
        if ($direction=='right') { $this->BlockDestruction(array('id_block'=>$row['id_block']));     $this->FieldDestroy(array('x'=>$x,'y'=>$y));  return $this->WalkFall($map,$x,$y);  }    
             
        if ($direction=='down')  
        { 
            // идем вниз, если первый блок пустой проверяем второй блок если тот не пустой оставливаемся на первом больше трех проверок то  возвращаем игрока назад и восстанавливаем блок под ним
            // чтобы проверить падение до 3-х при то что низа не видно, можно добавлять новых строк при создании внизу на 2-3
            $this->BlockDestruction(array('id_block'=>$row['id_block']));  
            $this->FieldDestroy(array('x'=>$x,'y'=>$y));          
            return $this->WalkFall($map,$x,$y);
    
        }   
                          
}   
    
}

public function FieldSearch($map,$x,$y, $z=0) // найти поле в выгруженной карте, передается строка с параметрами
{
    foreach ($map as $row) 
    {
        if ($row['x'] == $x && $row['y'] == $y) 
        { 
            return $row;
        }
    } 
   
}
public function FieldSearchObjects($p=array()) // найти объекты по координатам$x,$y,$obj=1
{
    $p['id_object']=(int)$p['id_object'];
    $p['type_object']=(int)$p['type_object'];    
    
    if ($p['id_object']>0)  $where="AND (t1.id_object='{$p['id_object']}' OR t1.id_object=10 OR t1.id_object=11)"; else $where=" AND t1.id_user=0 AND t1.id_object>0";
    
    $q="SELECT t1.*, t2.type FROM map_objects_xy t1, game_object t2 WHERE t1.x='{$p['x']}' AND t1.y='{$p['y']}' AND t1.z=0 $where AND t1.vis>0 AND t1.id_object=t2.id";
    //echo $q;
    $row= $this->getRow($q);
    return $row;
}
public function FieldSearchObjectsAndBlock($p=array('x','y')) // найти объекты по координатам
{
    
    $q="SELECT (id_block+(IF (id_object>0,id_object,0))+(IF (id_monster>0,id_monster,0))) as c FROM map_xy t1 LEFT JOIN map_objects_xy t2 ON t2.x=t1.x AND t1.y=t2.y WHERE t1.x='{$p['x']}' AND t1.y='{$p['y']}' ORDER by c DESC";
    $row=$this->getOne($q);
    return (int)$row; 
    
}
public function FieldDestroy($p=array()){ // убираем блок
    
    if ($p[0]['json']==1) $p=$p[0];     
    //$q="SELECT id_block FROM map_xy WHERE x=? AND y=?";
     //$id_block=$this->getOne()
    $arr=array('id_block'=>0);
    $this->update('map_xy',array('id_block'=>0),"WHERE x=? AND y=? ",array(intval($p['x']),intval($p['y'])));   
    $res[(error)]=0;
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;                        
}   
public function FindMonster($p=array())
{
    $p['json']=(int)$p['json'];
    $x=$p['x'];    $y=$p['y'];
    
    $q="SELECT SUM(t2.dm) FROM map_objects_xy t1 LEFT JOIN monster_bio t2 ON t2.id=t1.id_monster WHERE x=$x AND y=$y AND id_monster > 0";
    $dm=$this->getOne($q);
    if ($dm>0) {
        $res['monster']=1;
        $res[]=$this->SetIndicatorValue(array('indicator'=>'heart','value'=>0-$dm)); 
    }
    else { $res['monster']=0;}

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
function GetFightPlayerList($p=array())
{
    $p['json']=(int)$p['json'];
    $p['html']=(int)$p['html'];    
    $p['id_fight']=(int)$p['id_fight'];    
        // ищем открытые битвы с монстром
        $q="
            SELECT t2.*, t3.* 
            FROM fight t1, fight_list t2 LEFT JOIN monster_bio t3 ON (t3.id=t2.id_monster)
            WHERE 
                 t1.vis>0 AND t2.vis>0 AND 
                t2.id_fight='{$p['id_fight']}' AND t1.id=t2.id_fight 
            ORDER by side
        ";
        $fight=$this->getAll($q);
      //  print_r($fight);
        if (count($fight)>0) { 
            $in_fight=0; // в игре?
            foreach ($fight as $fight_row)
            {
                if ($fight_row['id_user']==$_SESSION['user_id']) $in_fight=1; // в игре
            }

        $res=array((error)=>0,  'open'=>1); 
            
        if ($p['html']==1){
            $this->smarty->assign('players', $fight);
            $html=$this->smarty->fetch('game_fight_info.html'); 
            if ($_SESSION['fight_info']!=$html) { $_SESSION['fight_info']=$html;
            $res['html']=$this->smarty->fetch('game_fight_info.html');}
            
        }
    } else {
        $res=array((error)=>1,'error_msg'=>'Битва не найдна');
    }

    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;   
}
public function KickToMonster($p=array())
{
    $p['json']=(int)$p['json'];
    $p['dm']=isset($p['dm'])?$p['dm']:1;
    
    // получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND vis>0";
    $row=$this->getRow($q); 

    // проверяем направление  
              
    if ($p['direction']=='left') $row['x']--;
    if ($p['direction']=='right') $row['x']++;          
    if ($p['direction']=='up') $row['y']--;
    if ($p['direction']=='down') $row['y']++;      
    $x=$row['x']; $y=$row['y'];
        
    // ищем монстра на координатах
    $q="SELECT t2.* FROM map_objects_xy t1 , monster_bio t2 WHERE x=$x AND y=$y AND id_monster>0 AND t2.id=t1.id_monster AND t2.life>0 ORDER by t2.type DESC";
    //echo $q;
    $monster_info=$this->getRow($q);
    
    if (count($monster_info)>0){    
        if ($monster_info['type']>6){             
            // ищем открытые битвы с монстром SELECT t2.* FROM fight t1, fight_list t2 WHERE t1.vis>0 AND t2.vis>0 AND (t2.id_monster='{$row['id']}' OR t2.id_user>0 OR t2.id_monster>0) AND t1.id=t2.id_fight        
            // SELECT t2.*, (SELECT COUNT(id) FROM fight_list t3 WHERE t3.id_fight=t1.id AND id_user='{$_SESSION['user_id']}') me  FROM fight t1, fight_list t2, WHERE t1.vis>0 AND t2.vis>0 AND (t2.id_monster='52' AND( t2.id_fight=t1.id AND( t2.id_user>0 OR t2.id_monster>0))) AND t1.id=t2.id_fight
            $q="SELECT t2.*, (SELECT COUNT(id) FROM fight_list t3 WHERE t3.id_fight=t1.id AND id_user='{$_SESSION['user_id']}') me  FROM fight t1, fight_list t2 WHERE t1.vis>0 AND t2.vis>0 AND (t2.id_monster='{$monster_info['id']}' AND( t2.id_fight=t1.id AND( t2.id_user>0 OR t2.id_monster>0))) AND t1.id=t2.id_fight";
            // echo $q;
            $fight=$this->getRow($q);
            // print_r($fight);
            if (count($fight)>0) {     
                if ($fight['me']==0){ // не в игре
                    $arr=array(
                        'id_user'=>$_SESSION['user_id'],
                        'side'=>2,
                        'id_fight'=>$fight['id_fight']
                    );
                    $this->insertdata('fight_list',$arr);
                }
                $_SESSION['fight_info']=''; // очищаем переменную с боем
                $res=array((error)=>0, 'msg'=>'Вошли в бой', 'open'=>1, 'id_fight'=>$fight['id_fight']); 
            }
            else{
                $id_fight=$this->insertdata('fight');
    
                $q="SELECT id_monster FROM map_objects_xy t1  WHERE x=$x AND y=$y AND id_monster>0";          
                $monster_list=$this->getAll($q);
                foreach($monster_list as $row_)
                    $this->insertdata('fight_list',array(
                        'id_monster'=>"{$row_['id_monster']}",
                        'side'=>1,
                        'id_fight'=>$id_fight
                    ));
     
                 $arr=array(
                    'id_user'=>$_SESSION['user_id'],
                    'side'=>2,
                    'id_fight'=>$id_fight
                );
                $this->insertdata('fight_list',$arr);
                
                $_SESSION['fight_info']=''; // очищаем переменную с боем
                $res=array((error)=>0, 'msg'=>'Битва создана', 'open'=>1, 'id_fight'=>$id_fight);  
                
                
            }
            
            //$res['html']=$this->smarty->fetch('game_fight_info.html');
        }
        else{
                // удар игрока
                $dm=1+$this->kick_dm;
                //decho $dm;
                if ($dm>0) {
                    $res['monster']=1;    // удар по монстру состоялся   
                    //$res[]=$this->SetIndicatorValue(array('indicator'=>'heart','value'=>0-$dm));         
                    $q="UPDATE map_objects_xy t1, monster_bio t2 SET t2.life = t2.life-{$dm}, id_monster=IF( t2.life-{$dm}<=0,0,id_monster) WHERE x={$x} AND y={$y} AND t1.id_monster=t2.id AND t2.id='{$monster_info['id']}'";       
                   // echo $q;       
                    $this->query($q);
                    
                    $this->SetLifeObject(array((id_record)=>$this->power_kick['id'],(value)=>'-0.5'));
                    
                }
                else { $res['monster']=0;}
                

                if (($monster_info['life']-$dm)>0)
                {                 
                    $life_percent=round((($monster_info['life']-$dm)/$monster_info['life_full'])*100,2);
                    $res=array((error)=>0,'msg'=>'Нанесли урон. Осталось жизни: '.$life_percent."%"." <br>Монстр нанес удар:-".$monster_info['dm']."xp"); 
                    
                    $this->SetGamerStat(array('stat'=>'kick_monster'));
                    
                    $this->SetIndicatorValue(array((indicator)=>'heart', (value)=>0-$monster_info['dm']));
                } 
                else {
                    $drop=rand(0,10);
                    if ($drop>0) {
                        $res=$this->GetRandomObject();
                    } 
                    else  { $res=array((error)=>0,'msg'=>'Убили монстра');  }
                   $this->SetGamerStat(array('stat'=>'kill_monster'));
                }
            }
    } 
    else {  $res=array((error)=>0,'msg'=>'Нечего руками махать, найди цель');}
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
function GetRandomObject($p=array())
{
    $p['json']=(int)$p['json'];
    // смотрим сколько инвентаря у игрока
    $q="SELECT inventory_full-inventory FROM gamer WHERE id_user='{$_SESSION['user_id']}'";
    $inventory=$this->getOne($q);
    
    if ($inventory>0) $drop=rand(0,1); else $drop=0;
    
    if ($drop==0){
        // выпали монетки
        $money=rand(1,10)*10; 
        $this->SetIndicatorValue(array('indicator'=>'money','value'=>$money));  
        $res=array((error)=>0,'msg'=>'С монстра выпали монетки: '.$money);
    }
     if ($drop==1){
        // выпал объект
        $object=rand(1,6); if ($object==5) $object=6;
        $object=$this->objects[$object-1];
        $this->SetInventoryCount(array('id_object'=>$object['id'],'count'=>1));  
        $res=array((error)=>0,'msg'=>"С монстра выпал объект: <img src='/img/game/objects/{$object['id']}.png' height=45>".$object['title']);
    }   
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
function KickMonsterInFight($p=array()) // удар монстра в битве.
{
    $p['json']=(int)$p['json'];
    $p['id_monster']=(int)$p['id_monster']; // получаем ID монстра
    $p['id_fight']=(int)$p['id_fight']; 
    $dm=1;
    
    // бьем монстра
    $q="UPDATE map_objects_xy t1, monster_bio t2, fight_list t3 SET t2.life = t2.life-{$dm}, t1.id_monster=IF( t2.life-{$dm}<=0,0,t1.id_monster), t3.vis=IF( t2.life<=0,0,1) WHERE t2.id='{$p['id_monster']}' AND t1.id_monster=t2.id AND t3.id_monster=t2.id";              
    $this->query($q);  

    // проверяем если живые монстры в битве
    
    $q="SELECT COUNT(id_monster) FROM fight_list WHERE id_fight='{$p['id_fight']}' AND id_monster>0 AND vis>0";
    
    $c=$this->getOne($q);

    if ($c>0){ $res=array((error)=>0); 
    }
    else { 
        $this->update('fight',array('vis'=>0)," WHERE id='{$p['id_fight']}'");
        $drop=rand(0,10); 
        $res=array((error)=>0);
        if ($drop>0) {
            $res=$this->GetRandomObject();
        } 
    }
    
        
    if ($p['json']==1)  {
        $result = json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
public function KickToMonster__COPY($p=array()) // копия удара - рабочая версия
{
    $p['json']=(int)$p['json'];
    $p['dm']=isset($p['dm'])?$p['dm']:1;
    
    // получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND vis>0";
    $row=$this->getRow($q); 

    // проверяем направление  
              
    if ($p['direction']=='left') $row['x']--;
    if ($p['direction']=='right') $row['x']++;          
    if ($p['direction']=='up') $row['y']--;
    if ($p['direction']=='down') $row['y']++;      
    $x=$row['x']; $y=$row['y'];
        
    // ищем монстра на координатах
    $q="SELECT SUM(t2.dm) dm, COUNT(t2.id) count_enemy FROM map_objects_xy t1 LEFT JOIN monster_bio t2 ON t2.id=t1.id_monster WHERE x=$x AND y=$y AND id_monster>0";

    $row=$this->getRow($q);
    if (($row['count_enemy'])>0){
    $dm=$row['dm'];
    if ($dm>0) {
        $res['monster']=1;       
        //$res[]=$this->SetIndicatorValue(array('indicator'=>'heart','value'=>0-$dm));         
        $q="UPDATE map_objects_xy t1, monster_bio t2 SET t2.life = t2.life-{$dm}, id_monster=IF( t2.life-{$dm}<=0,0,id_monster) WHERE x={$x} AND y={$y} AND t1.id_monster=t2.id";              
        $this->query($q);
    }
    else { $res['monster']=0;}
 
    $q="SELECT COUNT(t1.id) count_enemy FROM map_objects_xy t1 WHERE x=$x AND y=$y AND id_monster>0";
    $row2=$this->getOne($q);
    if ($row['count_enemy']==$row2)
    { 
        $res=array((error)=>0,'msg'=>'Нанесли урон'); 
        $this->SetGamerStat(array('stat'=>'kick_monster'));
    } 
    else {
        $money=rand(0,10);
        if ($money>0) {
            $money=$money*10; 
            $this->SetIndicatorValue(array('indicator'=>'money','value'=>$money));  
            $res=array((error)=>0,'msg'=>'С монстра выпили монетки: '.$money);
        } 
        else  { $res=array((error)=>0,'msg'=>'Убили монстра');  }
       $this->SetGamerStat(array('stat'=>'kill_monster'));
    }
    
} else {  $res=array((error)=>0,'msg'=>'Некого бить, иди ищи монстров');}
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
public function CreateMonster($p=array()) // создается монстр
{ 
    $p['json']=(int)$p['json'];
    $x=$p['x'];    
    $y=$p['y'];
   
    $id_monster=rand(0,count($this->monsters)-1); //echo "$id_monster";
    $id_monster=$this->insertdata('monster_bio',array('type'=>$this->monsters[$id_monster]['id'], 'life'=>$this->monsters[$id_monster]['life'],'life_full'=>$this->monsters[$id_monster]['life'], 'dm'=>$this->monsters[$id_monster]['dm']));
    $this->insertdata('map_objects_xy',array('x'=>$x, 'y'=>$y, 'id_monster'=>$id_monster));
    
 /*   
    $q="SELECT y FROM map_xy WHERE (x='$x' AND (y>'$y' AND y<'".($y+10)."')) AND id_block>0 ORDER by y LIMIT 1"; 
    $y1=(int)$this->getOne($q);
  
    $q="SELECT y FROM  map_objects_xy  WHERE (x='$x' AND (y>'$y' AND y<'".($y+10)."')) AND id_object>0 ORDER by y LIMIT 1";
    $y2=(int)$this->getOne($q);
*/

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
public function FindDeep($p=array()) // поиск глубины
{ 
    // принимаем x,y от даем $y
    $p['json']=(int)$p['json'];
    $x=$p['x'];    $y=$p['y'];
    
    $q="SELECT y FROM map_xy WHERE (x='$x' AND (y>'$y' AND y<'".($y+10)."')) AND id_block>0 ORDER by y LIMIT 1"; 
        $y1=(int)$this->getOne($q);
  
    $q="SELECT y FROM  map_objects_xy  WHERE (x='$x' AND (y>'$y' AND y<'".($y+10)."')) AND id_object>0 ORDER by y LIMIT 1";
    $y2=(int)$this->getOne($q);
      // echo $q;
        //exit;
        
        //echo $y1." - ".$y2;
    $res[(error)]=0; 
    
    if ($y1 > $y2)  $res['y']=$y2;     else $res['y']=$y1;
    if ($y1==0) $res['y']=$y2; 
    if ($y2==0) $res['y']=$y1;
     


    
    if ($y1==0 AND $y2==0) $res[(error)]=1;

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}

/**/
public function SetInventoryCountChest($p=array()) //  id_block или id_object, count (-1, 1)Уменьшаем или увеличиваем значение в инвентаре
{
    $p['json']=(int)$p['json'];   
    
    $p['id_block']=(int)$p['id_block'];
    $p['id_object']=(int)$p['id_object'];
    $p['id_chest']=(int)$p['id_chest'];
    
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));
    $p['count']=(int)$p['count'];



       
        if ($p['id_block']>0) 
        {   
            
 
            $q="SELECT id, count FROM gamer_inventory WHERE id_block='{$p['id_block']}' AND id_chest='{$p['id_chest']}'";
            //echo $q;
            $id_block=$this->getRow($q);
            
            if ($id_block['id']>0 ) { 
                //echo "2";
      
                    $q="UPDATE gamer_inventory SET count=count+{$p['count']} WHERE id='{$id_block['id']}'";
                    //echo $q;
                    $this->query($q);
       
            }            
            else { 
                //echo "1";
                $this->insertdata('gamer_inventory', array((id_chest)=>$p['id_chest'], (id_block)=>$p['id_block'], (count)=>1));
                }
               
        }
        
        if ($p['id_object']>0) {
            
      
                // если объект имеет жизнь и портится, то он не группируется
            
                //echo $row['this_object']."--";
            $q="SELECT * FROM gamer_inventory WHERE id_object='{$p['id_object']}' AND id_chest='{$p['id_chest']}' AND count>0";
            //echo $q;
            $id_block=$this->getRow($q); 
            // print_r($id_block);               
            if (($this->objects[$index_object_in_array]['life_full']>0 && $p['count']>0) || ($p['count']>0 && $id_block['id']==0) ) {
               //echo "1";
               if ($this->objects[$index_object_in_array]['life_full']>0){
                   $current_object=$this->getRow("SELECT *, object_type as type FROM gamer_inventory WHERE id='{$p['id_record']}'");
                   
                }
                if (!empty($current_object)) $info=$current_object; else $info=$this->objects[$index_object_in_array];
                 if ($this->objects[$index_object_in_array]['life_full']>0) $inv_count=1; else $inv_count=$p['count'];
              //  print_r($info);
                        $arr=array(
                            'id_chest'=>$p['id_chest'],
                            'id_object'=> $p['id_object'],
                            'count'=>$inv_count,
                            'power'=>$info['power'],
                            'power_kick'=>$info['power_kick'],
                            'power_dig'=>$info['power_dig'],
                            'life'=>$info['life'],
                            'life_full'=>$info['life_full'],
                            'object_type'=>$info['type']
                        );
                       
                        $this->insertdata('gamer_inventory',$arr);
        
                        $res=array((error)=>0);
                }
                else { //echo "2";
                    if  (( $p['count']>0) || ($id_block['count']>0 && $p['count']<0)){
                        //echo "3";
                          if ($p['id_record']>0 && $this->objects[$index_object_in_array]['life_full']>0) $id_block['id']=$p['id_record']; 
                        $q="UPDATE gamer_inventory SET count=count+{$p['count']} WHERE id='{$id_block['id']}' AND id_chest='{$p['id_chest']}'";
                        //echo $q;
                        $this->query($q);
                        $res=array((error)=>0);
                    }
                    else $res=array((error)=>1);
                }
        }                
  
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}

public function SetInventoryCount($p=array()) //  id_block или id_object, count (-1, 1)Уменьшаем или увеличиваем значение в инвентаре
{
    $p['json']=(int)$p['json'];   
    
    $p['id_block']=(int)$p['id_block'];
    $p['id_object']=(int)$p['id_object'];

    
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));
    $p['count']=(int)$p['count'];


//print_r($p);
    if ($p['count']>0){
        
        $q="SELECT SUM(t1.count) total, t2.inventory_full, (SELECT COUNT(id) FROM gamer_inventory t3 WHERE t3.id_object='{$p['id_object']}' AND t3.id_user=t1.id_user AND t3.object_act=0) as this_object  FROM gamer t2 LEFT JOIN gamer_inventory t1 ON t1.id_user='{$_SESSION['user_id']}'  WHERE t2.id_user='{$_SESSION['user_id']}' ";
       
        
        $row=$this->getRow($q);    
        //print_r($row);    
        
        if ((int)$row['total']+$p['count']>$row['inventory_full']) $res=array((error)=>1, 'error_msg'=>'Инвентарь переполнен!');
        else  $res=array((error)=>0); 
    }  
    else{ $res=array((error)=>0);}  
   // print_r($res);
   
    if ($res[(error)]==0){
       
        if ($p['id_block']>0) 
        {  // echo "1";
            
 
            $q="SELECT id, count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_block='{$p['id_block']}'";
            
            $id_block=$this->getRow($q);
            
            if ($id_block['id']>0 ) {  
            if ($p['count']>0 || ($p['count']<0 && $id_block['count']>0)) {       
                $q="UPDATE gamer_inventory SET count=count+{$p['count']} WHERE id='{$id_block['id']}'";
               
                $this->query($q);
               } 
            else { $res=array('error'=>1, 'error_msg'=>'Нет такого кол-ва');}
            }            
            else { 
                
                $this->insertdata('gamer_inventory', array((id_user)=>$_SESSION['user_id'], (id_block)=>$p['id_block'], (count)=>1));
                }
                        
        }
        if ($p['id_object']>0) {
            
      
                // если объект имеет жизнь и портится, то он не группируется
            
                //echo $row['this_object']."--";
            $q="SELECT id, count FROM gamer_inventory WHERE id_object='{$p['id_object']}' AND id_user='{$_SESSION['user_id']}' AND count>0 AND object_act=0";
            //echo $q;
            $id_block=$this->getRow($q); 
            // print_r($id_block);               
            if (($this->objects[$index_object_in_array]['life_full']>0 && $p['count']>0) || ($p['count']>0 && $id_block['id']==0) ) {
               //echo "1";
               $info=$this->objects[$index_object_in_array];
                    if ($this->objects[$index_object_in_array]['life_full']>0 ){
                   $current_object=$this->getRow("SELECT *, object_type as type FROM gamer_inventory WHERE id='{$p['id_record']}'");
                   
                }
                if (!empty($current_object)) $info=$current_object; 
                if ($this->objects[$index_object_in_array]['life_full']>0) $inv_count=1; else $inv_count=$p['count'];
                //print_r($info);
                        $arr=array(
                            'id_user'=>$_SESSION['user_id'],
                            'id_object'=> $p['id_object'],
                            'count'=>$inv_count,
                            'power'=>$info['power'],
                            'power_kick'=>$info['power_kick'],
                            'power_dig'=>$info['power_dig'],
                            'life'=>$info['life'],
                            'life_full'=>$info['life_full'],
                            'object_type'=>$info['type']
                        );
                       
                      // print_r($arr);
                        $this->insertdata('gamer_inventory',$arr);
        
                        $res=array((error)=>0);
                }
                else { //echo "2";
                    if  (( $p['count']>0) || ($id_block['count']>0 && $p['count']<0)){
                        //echo "3";
                        if ($p['id_record']>0 && $this->objects[$index_object_in_array]['life_full']>0) $id_block['id']=$p['id_record']; // если передан ордер_ид
                        $q="UPDATE gamer_inventory SET count=count+{$p['count']} WHERE id='{$id_block['id']}' AND id_user='{$_SESSION['user_id']}'";
                        //echo $q;
                        $this->query($q);
                        $res=array((error)=>0);
                    }
                    else $res=array((error)=>1);
                }
        }     
        
    
        
    }
    
    if ($res[(error)]==0){ 
        // уменьшаем инвентарь или увеличиваем
        $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>$p['count']));
        $life=0.5*abs($p['count']);
        // если инвентарь пополнился уменьшаем жизнь всем активным рюкзакам, если они есть
        $q="
        UPDATE 
            gamer_inventory t1, 
            gamer t2 
        SET  
            t1.life=t1.life-$life, 
            t2.inventory_full=IF(t1.life-$life>0, t2.inventory_full, t2.inventory_full-t1.power),
            t2.inventory=IF(t1.life-$life>0, t2.inventory, t2.inventory-1),
            t1.count=IF(t1.life-$life>0,1,0) 

             
        WHERE 
            t1.id_user='{$_SESSION['user_id']}' AND 
            t1.object_type=4 AND 
            t1.object_act=1 AND 
            t2.id_user='{$_SESSION['user_id']}' AND count>0";
        $this->query($q);
    }
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}



public function SetInventoryCountCOPY($p=array()) //  id_block или id_object, count (-1, 1)Уменьшаем или увеличиваем значение в инвентаре
{
    $p['json']=(int)$p['json'];   
    
    $p['id_block']=(int)$p['id_block'];
    $p['id_object']=(int)$p['id_object'];

    
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));
    $p['count']=(int)$p['count'];


//print_r($p);
    if ($p['count']>0){
        
        $q="SELECT SUM(t1.count) total, t2.inventory_full, (SELECT COUNT(id) FROM gamer_inventory t3 WHERE t3.id_object='{$p['id_object']}' AND t3.id_user=t1.id_user AND t3.object_act=0) as this_object  FROM gamer t2 LEFT JOIN gamer_inventory t1 ON t1.id_user='{$_SESSION['user_id']}'  WHERE t2.id_user='{$_SESSION['user_id']}' ";
       
        
        $row=$this->getRow($q);    
        //print_r($row);    
        
        if ((int)$row['total']+$p['count']>$row['inventory_full']) $res=array((error)=>1, 'error_msg'=>'Инвентарь переполнен!');
        else  $res=array((error)=>0); 
    }  
    else{ $res=array((error)=>0);}  
   // print_r($res);
   
    if ($res[(error)]==0){
       
        if ($p['id_block']>0) 
        {   
            
 
            $q="SELECT id, count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_block='{$p['id_block']}'";
            
            $id_block=$this->getRow($q);
            
            if ($id_block['id']>0 ) { 
            if ($p['count']>0 || ($p['count']<0 && $id_block['count']>0)) {       
                $q="UPDATE gamer_inventory SET count=count+{$p['count']} WHERE id='{$id_block['id']}'";
                $this->query($q);
               } 
            }            
            else { 
                
                $this->insertdata('gamer_inventory', array((id_user)=>$_SESSION['user_id'], (id_block)=>$p['id_block'], (count)=>1));
                }
            
           if ($p['id_chest']==0)  $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>$p['count']));                 
        }
        
        if ($p['id_object']>0) {
            
      
                // если объект имеет жизнь и портится, то он не группируется
            if ( $p['count']>0) {
                //echo $row['this_object']."--";
                if ($this->objects[$index_object_in_array]['life_full']>0 || (isset($row['this_object']) && $row['this_object']==0) ) {
               //echo "1";
                        $arr=array(
                            'id_user'=>$_SESSION['user_id'],
                            'id_object'=> $p['id_object'],
                            'count'=>1,
                            'power'=>$this->objects[$index_object_in_array]['power'],
                            'power_kick'=>$this->objects[$index_object_in_array]['power_kick'],
                            'power_dig'=>$this->objects[$index_object_in_array]['power_dig'],
                            'life'=>$this->objects[$index_object_in_array]['life_full'],
                            'life_full'=>$this->objects[$index_object_in_array]['life_full'],
                            'object_type'=>$this->objects[$index_object_in_array]['type']
                        );
                        //print_r($arr);
                        $this->insertdata('gamer_inventory',$arr);
                    
      
              $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>$p['count']));
                $res=array((error)=>0);
                }
                else {
                    //echo "2";
                    $q="SELECT id FROM gamer_inventory  WHERE id_user='{$_SESSION['user_id']}' AND id_object='{$p['id_object']}' AND object_act=0";
                    $id_object=$this->getOne($q);
                    //echo $q;
                    if ($id_object>0){
                        $this->query("UPDATE gamer_inventory SET count=count+1 WHERE id='{$id_object}' AND id_user='{$_SESSION['user_id']}'");
                        $res=array((error)=>0);
                        
                      $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>$p['count']));
                    }
                    else {
                        $res=array((error)=>1, (error_msg)=>"Нет объекта для удаления или он активирован");
                    }
                  //  $q="UPDATE";
                }
            }
            else{
                //echo "2";

                $q="
                    UPDATE gamer_inventory  
                    SET count=IF (count+{$p['count']}>=0,count+{$p['count']},0), life=life_full, object_act=0 
                    WHERE id_user='{$_SESSION['user_id']}' AND id_object='{$p['id_object']}' AND object_act=0";  

                //echo $q;
                $this->query($q);
                $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>$p['count']));
                $res=array((error)=>0);
            }

        }                
    }
    
    if ($res[(error)]==0){ // если инвентарь пополнился уменьшаем жизнь всем активным рюкзакам, если они есть
        $q="
        UPDATE 
            gamer_inventory t1, 
            gamer t2 
        SET  
            t1.life=t1.life-0.5, 
            t2.inventory_full=IF(t1.life-0.5>0, t2.inventory_full, t2.inventory_full-t1.power),
            t2.inventory=IF(t1.life-0.5>0, t2.inventory, t2.inventory-1),
            t1.count=IF(t1.life-0.5>0,1,0) 

             
        WHERE 
            t1.id_user='{$_SESSION['user_id']}' AND 
            t1.object_type=4 AND 
            t1.object_act=1 AND 
            t2.id_user='{$_SESSION['user_id']}' AND count>0";
        $this->query($q);
    }
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}
public function SetBlock($p=array('json'=>0)) // ставим блок
{ 
    
    $p['json']=(int)$p['json'];    
    $p['id_block']=(int)$p['id_block']; 
    
            
    if ($this->xyz['x_min']<=$this->xyz['x'] && $this->xyz['x_max']>=$this->xyz['x']){   
  

     // получаем текущую позицию игрока    

    // если координаты не переданы, получаем
    
    $q="SELECT count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_block=?";
    $count_block=$this->getOne($q,array((int)$p['id_block']));
    
    if ($count_block>0){
    
        $q="SELECT x,y FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
        $row=$this->db->getRow($q); 
        
        $p['x']=$row['x']; 
        $p['y']=$row['y'];  
    
        
        if ($p['direction']=='up') $p['y']--;
        if ($p['direction']=='down') $p['y']++;
        if ($p['direction']=='left') $p['x']--;
        if ($p['direction']=='right') $p['x']++;
        
        
        // проверяем нет ли блока и объекта на координатах
        $id_block=$this->FieldSearchObjectsAndBlock(array('x'=>$p['x'], 'y'=>$p['y']));
      
       
        if ($id_block==0) {
            //   $this->update('map_xy t1, game_block t2',array('id_block'=>$p['id_block']),"WHERE t2.id='{$}' AND x=? AND y=? ",array(intval($p['x']),intval($p['y'])));   
            $q="UPDATE map_xy t1, game_block t2 SET id_block='{$p['id_block']}', t1.dm=t2.dm WHERE t1.x='{$p['x']}' AND t1.y='{$p['y']}' AND t1.z='0' AND t2.id='{$p['id_block']}'";
            $this->query($q);
            
            $res[(error)]=0;
            

            $this->SetInventoryCount(array('id_block'=>$p['id_block'],'count'=>-1));
            $this->SetGamerStat(array('stat'=>'block_set'));
        }
        else $res=array((error)=>1, 'error_msg'=>'Нельзя установить');
    } else  $res=array((error)=>1, 'error_msg'=>'Нет в наличии');
    }
    else $res=array('error'=>1,'error_msg'=>'Нельзя устанавливать на чужой территории');  
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;                        
}

public function GetBlockList($p=array()) // INVENTORY получаем список блоков у пользователя
{ 
    $p['json']=(int)$p['json'];
    $p['html']=(int)$p['html'];
    
    $q="
    SELECT 
        t1.*, 
        t2.title as title,
        t2.title as block_title,
        t3.title as object_title, t2.sell_stock as price
    FROM gamer_inventory t1 
        LEFT JOIN game_block t2 ON t2.id=t1.id_block
        LEFT JOIN game_object t3 ON t3.id=t1.id_object
    WHERE t1.id_user='{$_SESSION['user_id']}' AND t1.count>0 ORDER by t1.object_act DESC, t1.object_type, t1.id_object";   
    $row= $this->getAll($q);
    $res[(error)]=0;

    foreach ($row as $item=>$item)
    {
        if ($row[$item]['life_full']>0) $row[$item]['life_percent']=round(($row[$item]['life']/$row[$item]['life_full'])*100,2);
        else $row[$item]['life_percent']=0;
    }
     /*   */
    $res['inventory_new']=0;
    if ($_SESSION['inventory']!=$row) {
        $_SESSION['inventory']=$row;  $res['inventory']=$row; $res['inventory_new']=1;}
        
        $this->smarty->assign('list', $row);
        $temp_html=$this->smarty->fetch('gamer_inventory.html');
        
        if ($_SESSION['inventory_html']!=$temp_html) {        
            $_SESSION['inventory_html']=$temp_html;            
            $res['inventory_html']=$temp_html; 
            $res['inventory']=$row;             
            $res['inventory_new']=1;
        }

    
     //print_r($res);
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
public function GetIndicatorList($p=array()) // получаем список индиакторов
{ 
    $p['json']=(int)$p['json'];
    $q="SELECT *,  (SELECT COUNT(id) FROM gamer WHERE date_online> DATE_SUB(NOW(), INTERVAL 2 MINUTE) ) as online FROM gamer WHERE id_user='{$_SESSION['user_id']}'";  // ,
    $row= $this->getRow($q);
    unset($row['date_online']);
    $row['oxygen_percent']=ceil(($row['oxygen']/$row['oxygen_full'])*100);
    $row['heart_percent']=ceil(($row['heart']/$row['heart_full'])*100);
    $row['inventory_percent']=ceil(($row['inventory']/$row['inventory_full'])*100);
    
       // if ($row[$item][''])

    $res[(error)]=0;
    if ($_SESSION['indicator']!=$row) { $_SESSION['indicator']=$row;  $res['indicator']=$row; $res['indicator_new']=1;} else {$res['indicator_new']=0; }    
    
    //print_r($res);
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}
function SetIndicatorValue($p=array()) // Уменьшаем или увеличиваем значение в инвентаре id_block, value (-1, 1)
{

    $p['json']=(int)$p['json'];   
    if (!isset($p['id_user'])) $p['id_user']=$_SESSION['user_id']; else $p['id_user']=(int)$p['id_user'];
    $p['value']=(float)$p['value'];

  //print_r($p);

  
       // $q="INSERT INTO gamer (id_user, {$p['indicator']}) VALUES ('{$p['id_user']}', '".floatval($p['value'])."') ON DUPLICATE KEY UPDATE {$p['indicator']}={$p['indicator']}+".floatval($p['value']);
        if ($p['value']>=0) $q="UPDATE gamer SET {$p['indicator']} = IF( {$p['indicator']} + {$p['value']} <= {$p['indicator']}_full,{$p['indicator']} + {$p['value']},{$p['indicator']}_full) WHERE `id_user` = {$p['id_user']}";
        else  $q="UPDATE gamer SET {$p['indicator']} = IF( {$p['indicator']} + {$p['value']} >= 0,{$p['indicator']} + {$p['value']},0) WHERE `id_user` = {$p['id_user']}";
        
        if ($p['indicator']=='money') $q="UPDATE gamer SET {$p['indicator']} = {$p['indicator']} + {$p['value']} WHERE `id_user` = {$p['id_user']}";
        if (isset($p['reset'])) $q="UPDATE gamer SET {$p['indicator']} = {$p['indicator']}_full WHERE `id_user` = {$p['id_user']}";
        if (isset($p['reset']) && !isset($p['indicator']))  $q="UPDATE gamer SET heart = heart_full, oxygen=oxygen_full, inventory=0 , inventory_full = 30 WHERE `id_user` = {$p['id_user']}";
        //echo $q;
        $this->query($q);
        
        $res[(error)]=0;
      // if ($p['indicator']!=0){       } else $res=array((error)=>1, 'error_msg'=>'Что-то пошло не так');
      // возвращаем обратно что там у нас получилось
       $q="SELECT * FROM gamer WHERE id_user='{$p['id_user']}'";
       $res['row']=$this->getRow($q);
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}
public function FieldInsert($x=0,$y=0, $z=0, $tableName='')    // вставляем новый блок
{
if ($tableName=='') $tableName='map_xy';
    /* 
        1 - блок земли на первом уровне
        2 - блок земли
        3 - блок камня x-64
        4 - блок камня y-64
        5 - блок угля x-128 y-128
        
        6 - дерево x -512 y-190
        7 - деовео x-574 y-254
        
.block6{ background-position-x: -510px; background-position-y: -192px } /* дерево
.block7{ background-position-x: -574px; background-position-y: -256px } /* елка 


.block25{ background-position-x: -64px; background-position-y: -128px } /* металл 
.block50{ background-position-x: -256px; background-position-y: -128px } /*  золото 
.block100{ background-position-x: 0px; background-position-y: -192px } /* бриллиант 
    */
   
//print_r($this->block);
$block=0; $dm=0;
$i=0;
 if ($y>0)  while ($block==0){
     $i++;
            foreach($this->blocks as $row)
            {
               
                if ($y>=$row['deep'] && $y<=$row['deep_end'])
                {
                    $rand=rand(0,(1000-$row['chance']));
                    If ($rand==0) { $block= $row['id'];  $dm=$row['dm'];}  //else  $block=0;
                    if ($rand==0) break;
                }
                
                
            }   
            //$block=0;
    if ($i==30) $block=1;
}    

    $q="INSERT INTO `$tableName` (x, y, z, id_block, dm) VALUES    ('".($x)."','".($y)."','".($z)."','{$block}', '{$dm}' )  ON DUPLICATE KEY UPDATE x='{$x}'"; 
    //echo $q; exit;
    $this->db->query($q);  
    
 }
 
public function SetObject($p=array('json'=>0)) // устанавливаем объект id_object 
{
    $row=$this->xyz;

        
    $q="SELECT count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object=? AND count>0";
    
    $count_object=(int)$this->getOne($q,array((int)$p['id_object']));

    if ($count_object>0){
    

        // если есть направление то проверяем изменив параметры если смотрим лево и право
        if ($p['direction']=='left') $row['x']--;
        if ($p['direction']=='right') $row['x']++;  
           
        $row_object=$this->FieldSearchObjectsAndBlock(array('x'=>$row['x'], 'y'=>$row['y']));
        if ($p['id_object']==6) $row_object=0;
        //print_r($row_object);
        if ($row_object>0) 
        {
            $res=array((error)=>1,'error_msg'=>'Что-то мешает');              
                  
        }
        else { 
            //$row['message']='Лестница установлена'; 
    
                  
            $array=array(
                'x'=>$row['x'],
                'y'=>$row['y'],
                'id_object'=>$p['id_object'],
                'owner_object'=>$_SESSION['user_id']
                
            );
          //  Идея - помещать объекту в эту таблицу, как владение, если vis=2 занчит объект у игрока, когда его поместят в ящик или еще какое место то у него vis будет как ид места, а x^y там где его искать, ну как то так ()
            $id=$this->insertdata('map_objects_xy',$array);
            $this->SetInventoryCount(array('id_object'=>(int)$p['id_object'],'count'=>-1));   
           $res=array(
            (error)=>0,
            (id)=>$id            
            );
        }

    } else $res=array((error)=>1, 'error_msg'=>'Нет в наличии');

    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      
}
/*
public function ObjectSet__OLD($obj=1) // устанавливаем объект
{
    if (isset( $_POST['id_object'])) $obj= $_POST['id_object'];
    
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    $row=$this->db->getRow($q); 

    $row_object=$this->FieldSearchObjects(array('x'=>$x,'y'=>$y,'id_object'=>1)$row['x'],$row['y'],$obj);
    
    if (count($row_object)>0) 
    {
        $row['message']='Лестница уже стоит';              
        $row['res']=0;       
    }
    else { 
        //$row['message']='Лестница установлена'; 
        
        $array=array(
            'x'=>$row['x'],
            'y'=>$row['y'],
            'id_object'=>$obj,
            'id_user'=>$_SESSION['id_user']
            
        );
      //  Идея - помещать объекту в эту таблицу, как владение, если vis=2 занчит объект у игрока, когда его поместят в ящик или еще какое место то у него vis будет как ид места, а x^y там где его искать, ну как то так ()
        $this->insertdata('map_objects_xy',$array);
        
        $row['res']=1;
    }
    
    
    
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return $row; 
        else {

            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row2;      
}*/

public function MapLoad5x5_TRUE($inside=0){
      
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    
    $sql='';
    for ($y=-2;$y<=2;$y++)
    {

            $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."')";  
            if ($y<2) $sql.=" OR ";  
    
    }    
    $q2="SELECT t1.* FROM  map_xy t1  WHERE  $sql ORDER BY t1.y, t1.x";
    //echo $q;
    $row2=$this->db->getAll($q2); 
 
    $q3="
        SELECT t1.*, t2.id_object obj, t2.id_user gamer
        FROM  
        map_xy t1, 
        map_objects_xy t2 
        WHERE     
        ($sql) AND  (t2.x=t1.x AND t2.y=t1.y) AND t2.vis>0 AND (t2.id_object>0 OR t2.id_user>0) ORDER BY t1.y, t1.x";
    //echo $q;
    $row3=$this->db->getAll($q3);  
    
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return array($row2,$row3); 
        else {
            $row['pole']=$row2;
            $row['objects']=$row3;
            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row2;    
}
public function MapLoad5x5($inside=0){
      
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    
    $sql='';
    for ($y=-2;$y<=4;$y++)
    {

            $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."')";  
            if ($y<4) $sql.=" OR ";  
    
    }    
    $q2="SELECT t1.* FROM  map_xy t1  WHERE  $sql ORDER BY t1.y, t1.x";
 //  echo $q;
    $row2=$this->db->getAll($q2); 
 
    $q3="
        SELECT t1.*, t2.id_object obj, t2.id_user gamer
        FROM  
        map_xy t1, 
        map_objects_xy t2 
        WHERE     
        ($sql) AND  (t2.x=t1.x AND t2.y=t1.y) AND t2.vis>0 AND (t2.id_object>0 OR t2.id_user>0) ORDER BY t1.y, t1.x";
    //echo $q;
    $row3=$this->db->getAll($q3);  
    
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return array($row2,$row3); 
        else {
            $row['pole']=$row2;
            $row['objects']=$row3;
            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row2;    
}
public function MapLoad5x7_v2($p=array())
{
    // выгружаем текущую карту, так же состояние игрока и другие данные

   // if ($p[0]['json']==1) { $p=$p[0]; $res=$p['json']; }
    $p['json']=(int)$p['json'];
    // вторая версия
      
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    

    $this->MapLoad5x7(1);
    
    $sql='';
    for ($y=-2;$y<=4;$y++)
    {
        $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."' AND t1.z='{$row['z']}')";  
        if ($y<4) $sql.=" OR ";      
    }    
    
    $q2="SELECT t1.*, t2.dm as dm_full , t3.x_min, t3.x_max FROM   map_xy t1 LEFT JOIN gamer t3 ON t3.id_user =  '{$_SESSION['user_id']}'  LEFT JOIN game_block t2 ON t1.id_block=t2.id  WHERE  $sql  ORDER BY t1.y, t1.x";
   //echo $q; exit;
    $row2=$this->db->getAll($q2); 

        
    $q3="
        SELECT t1.*, t2.id_object obj, t2.id_user gamer, t2.id_monster, t3.type as monster_type, t2.id_build
        FROM  
            map_xy t1, 
            map_objects_xy t2 LEFT JOIN monster_bio t3 ON t3.id=t2.id_monster
        WHERE     
            ($sql) AND  (t2.x=t1.x AND t2.y=t1.y) AND 
            t2.vis>0 AND (t2.id_object>0 OR (t2.id_user>0 AND t2.id_user<>'{$_SESSION['user_id']}') OR id_monster>0 OR id_build>0) 
        ORDER BY t1.y, t1.x,  t1.z, t2.id";
    //echo $q;
    $row3=$this->db->getAll($q3);     

    $is_monster=false;
    foreach ($row3 as $row)
    {
        if ($row['id_monster']>0) $is_monster=true;
    }
    if ($is_monster==true) {  }
   
    
    if ($_SESSION['pole']!=$row2) { $_SESSION['pole']=$row2;  $res['pole']=$row2; $res['pole_new']=1;} else {$res['pole_new']=0; }
    if (isset($_REQUEST['param']['id_user'])) { $_SESSION['pole']=$row2;  $res['pole']=$row2; $res['pole_new']=1;}
    
    
    
    if ($_SESSION['objects']!=$row3 || $res['pole_new']==1) { $_SESSION['objects']=$row3; $res['objects']=$row3;$res['objects_new']=1;} else { $res['objects_new']=0;}
    
    $inventory=$this->GetBlockList();
    $indicator=$this->GetIndicatorList();

    //print_r($indicator);
    $res=$res+$inventory+$indicator;

   $res['message_new']=(int)$this->getOne("SELECT COUNT(id) FROM message WHERE id>{$_SESSION['message_last']}");
    //print_r($res);
    $this->SetOnline();
    $res[(error)]=0;


    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;   
}

public function MapLoadBig($p=array())
{
    // выгружаем текущую карту, так же состояние игрока и другие данные

   // if ($p[0]['json']==1) { $p=$p[0]; $res=$p['json']; }
    $p['json']=(int)$p['json'];
    // вторая версия 

/*for ($y=0;$y<=1000;$y++){
    for ($x=-20;$x<=20;$x++)
    {
           $this->FieldInsert($x,$y,0,'map_xy_BIG');
    }
}   
*/


    $q2="SELECT id_block,x,y FROM   map_xy_BIG WHERE  x>=-20 AND x<=20 AND y>0 AND y<=20";
   //echo $q; exit;
    $row2=$this->db->getAll($q2); 
    
    if (isset($_REQUEST['param']['id_user'])) { $_SESSION['pole']=$row2;  $res['pole']=$row2; $res['pole_new']=1;}


    $res[(error)]=0;


    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;   
}

public function MapLoad5x7($inside=0){    // СОЗДАЕТСЯ карта 5 на 5 + две подопнительные строки вверх и вниз

      
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    
  //  print_r($row);
    $sql='';
    for ($y=-4;$y<=4;$y++)
    {
        $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."' AND t1.z='{$row['z']}')";  
        if ($y<4) $sql.=" OR ";  
    }    
    
    $q2="SELECT * FROM  map_xy t1  WHERE $sql ORDER BY y,x";

    $row2=$this->db->getAll($q2); 
    //echo $q2;
//echo count($row2);
if (count($row2)<45){


for ($y=-4;$y<=4;$y++){
    for ($x=-2;$x<=2;$x++)
    {
           $this->FieldInsert($row['x']+$x,$row['y']+$y);
    }
}    

}    

    $sql='';
    for ($y=-4;$y<=4;$y++)
    {
        $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."'  AND t1.z='{$row['z']}')";  
        if ($y<4) $sql.=" OR ";  
    }    
    
    $q2="SELECT * FROM  map_xy t1 WHERE $sql ORDER BY y,x";
    //echo $q;
    $row2=$this->db->getAll($q2); 
    
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return $row2; 
        else {
            $row['pole']=$row2;
            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row2;    
}

public function GetGamerStat($p=array()) // изменение статистики игрока. принимается:stat, value, set (если передать то значение не плюсуется а ставится)
{
    $p['json']=(int)$p['json'];   
    $q="SELECT * FROM gamer_stat WHERE id_user='{$_SESSION['user_id']}'";
    $row=$this->getRow($q);
    
    $res=array((error)=>0, 'row'=>$row);
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}
public function SetGamerStat($p=array()) // изменение статистики игрока. принимается:stat, value, set (если передать то значение не плюсуется а ставится)
{
    $p['json']=(int)$p['json'];   
  
    if (!isset($p['id_user'])) $p['id_user']=$_SESSION['user_id']; 
    $p['value']=(!isset( $p['value']))?1:$p['value'];
    $p['set']=(isset($p['set']))?1:0;
    
    if ($p['set']==1) $add="{$p['stat']} = {$p['value']}"; 
    else $add="{$p['stat']} =  {$p['stat']}  + {$p['value']}";
    
    $q="UPDATE gamer_stat SET  $add WHERE `id_user` = {$p['id_user']}";


    $this->query($q);

    $q="INSERT INTO gamer_stat_day (id_user,  date_create) VALUES ('{$_SESSION['user_id']}', '".date("Y-m-d 00:00:00")."') ON DUPLICATE KEY UPDATE  $add";
    $this->query($q);
        
    $res[(error)]=0;
      // if ($p['indicator']!=0){       } else $res=array((error)=>1, 'error_msg'=>'Что-то пошло не так');
      // возвращаем обратно что там у нас получилось
    $q="SELECT * FROM gamer_stat WHERE id_user='{$_SESSION['user_id']}'";
    $res['row']=$this->getRow($q);
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;         
}
public function GamerGetInfo($id_user=0)
{
    if (!isset($_REQUEST['id_user']) && $id_user==0) $id_user=$_SESSION['user_id']; // если ничего не передано в ид юзер то смотрим инфу посетилея из сессии
    
    $q="SELECT * FROM gamer WHERE id_user=?";
    $row=$this->getRow($q,array($id_user));
    
    if ($row['id']<1){ // если игрок не создан
        $q="SELECT * FROM gamer ORDER by id DESC LIMIT 1";
        $row=$this->getRow($q);
        if ($row['id']>0){ $x_min=$row['x_max']+1; $x_home=$x_min+20; $x_max= $x_home+20; }
        else { $x_min=0; $x_home=20; $x_max=40;}
        $array=array( 
            'id_user'=>$id_user,
            'x_min'=>$x_min,
            'x_max'=>$x_max,
            'x_home'=>$x_home
        );    
        $id=$this->insertdata('gamer',$array);
        
        $this->query("UPDATE map_xy SET id_block=0 WHERE x=$x_home AND (y=1 OR y=2 OR y=3)");
        
        $array=array( 'id_object'=>1,    'x'=>$x_home,'y'=>1,'owner_object'=>$id_user);    
        $id=$this->insertdata('map_objects_xy',$array);  
          
        $array=array( 'id_object'=>1,    'x'=>$x_home,'y'=>2,'owner_object'=>$id_user);    
        $id=$this->insertdata('map_objects_xy',$array);        
        
        $array=array( 'id_object'=>1,    'x'=>$x_home,'y'=>3,'owner_object'=>$id_user);    
        $id=$this->insertdata('map_objects_xy',$array);          
        
        
        $array=array( 'id_user'=>$id_user,    'x'=>$x_home,'y'=>0);    
        $id=$this->insertdata('map_objects_xy',$array);  
             
        $array=array( 'id_object'=>8,    'x'=>$x_home+1,'y'=>0,'owner_object'=>$id_user);    
        $id=$this->insertdata('map_objects_xy',$array);  
        
                
        $id=$this->insertdata('gamer_indicator',array('id_user'=>$id_user));   
        $id=$this->insertdata('gamer_inventory',array('id_user'=>$id_user, 'id_object'=>1, 'object_type'=>2, 'count'=>10)) ;   
        $id=$this->insertdata('gamer_stat',array('id_user'=>$id_user)) ;       
        $id=$this->insertdata('gamer_level', array('id_user'=>$id_user));
        
        // тут можно проверять все ли создалось в перемой ид у всех должно быть число
    }
    
    $q="SELECT * FROM gamer WHERE id_user='{$id_user}'";
    $row=$this->getRow($q);
       
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return $row; 
        else {

            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row;   
    
} 

//----- магазин
public function SellBlock($p=array()) // Уменьшаем или увеличиваем значение в инвентаре id_block, value (-1, 1)  // all - если 1, продать все
{    
    $p['json']=(int)$p['json'];
    $p['id_block']=(int)$p['id_block'];
    $p['all']=(int)$p['all']; // продать все если передано 0 - продается по -1  
    
    if ($this->xyz['y']>0 ){   
        // проверяем если у нас активный объект для связи
        $q="SELECT * FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object=15 AND object_act=1 AND count>0";
        $radio=$this->getRow($q);
    }
        
    if (($this->xyz['y']<=0) ||  ($this->xyz['y']>0 && count($radio)>0)){      
        if ($this->xyz['y']>0 && $this->xyz['y']<=50) $res=array('error'=>0);
        if ($this->xyz['y']>50 && $this->xyz['y']<=100) $res=array('error'=>rand(0,1));
        if ($this->xyz['y']>100){  
            if ($row['power']<=99) $rand_max=100-$row['power']; else $rand_max=1;
            $res=array('error'=>rand(0,$rand_max)); 
        }  
        
        if ($res['error']==1 && count($radio)>0){
            // если приемник не помог смотрим если ли вышка на уровне
            
            $q="SELECT * FROM map_objects_xy WHERE x='{$this->xyz['x']}' AND y='{$this->xyz['y']}' AND z='{$this->xyz['z']}' AND id_object=18";
            $radio_super=$this->getRow($q);
            //echo $q;
            if (count($radio_super)>0)$res['error']=0;
        }
                    
        if ($res['error']==0){
            $q="SELECT `count` FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_block='{$p['id_block']}'";   
            $count=$this->getOne($q);
            
            if ($count>0) { 
                
                
                if ($p['all']==1) $value=$this->blocks[$p['id_block']-1]['sell_stock']*$count; 
                else $value=$this->blocks[$p['id_block']-1]['sell_stock'];
                //echo $count;
                $this->SetIndicatorValue(array('indicator'=>'money','value'=>$value));
                
                if ($p['all']==1) $this->SetInventoryCount(array('id_block'=>$p['id_block'],'count'=>0-$count));
                else $this->SetInventoryCount(array('id_block'=>$p['id_block'],'count'=>-1));   
                
                $res=array((error)=>0,'msg'=>'Успешно');
                $this->SetGamerStat(array('stat'=>'block_sell'));

                // уменьшаем цену блоку до минимума
                $q="UPDATE game_block SET sell_stock=IF (sell_stock-0.5>sell,sell_stock-0.5,sell) WHERE id='{$p['id_block']}'";
                $this->query($q);
                // увеличиваем случайному блоку стоимость
                
                $random_block=rand(0, 4);
                $index_block_in_array=array_search( $random_block, array_column($this->blocks, 'id'));
                
                $q="UPDATE game_block SET sell_stock=sell_stock+0.5 WHERE id='{$index_block_in_array}'";
                $this->query($q);            
            } 
            else $res=array((error)=>1,'error_msg'=>'Нет в наличии');
        } else $res=array((error)=>1,'error_msg'=>'Плохая связь, увеличьте мощность приемника или поднимитесь выше или попробуйте еще раз');
        
        // уменьшаем жизнь приемнику если такой есть, вне зависимости удалось связаться или нет
        if ( count($radio)>0 ) $this->SetLifeObject(array((id_record)=>$radio['id'],(value)=>'-0.5'));  
        
    } else $res=array((error)=>1,'error_msg'=>'Магазин недоступен под землей. Поднимитесь на поверхность или активируйте приемник');


    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}
//----- магазин
public function BuyObject($p=array('json'=>0)) // Уменьшаем или увеличиваем значение в инвентаре id_block, value (-1, 1)
{
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));

    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    
    
    if ($row['y']>0 ){   
        // проверяем если у нас активный объект для связи
        $q="SELECT * FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object=15 AND object_act=1 AND count>0";
        $radio=$this->getRow($q);

    }
    
    if (($row['y']<=0) ||  ($row['y']>0 && count($radio)>0)){      
              
    $q="SELECT SUM(t1.count) total, t2.inventory_full FROM  gamer t2 LEFT JOIN gamer_inventory t1 ON t1.id_user='{$_SESSION['user_id']}'   WHERE  t2.id_user='{$_SESSION['user_id']}' ";
    $row=$this->getRow($q);
    if ($row['total']+1>$row['inventory_full']) $res=array((error)=>1, 'error_msg'=>'Инвентарь переполнен!');
    else {
        $q="SELECT money FROM gamer WHERE id_user='{$_SESSION['user_id']}'";   
        $row=$this->getOne($q);
        //print_r($p);print_r($this->objects);
        if ($row>=$this->objects[$index_object_in_array]['buy']) {
           // echo $this->objects[$index_object_in_array]['buy'];
           $res=$this->SetInventoryCount(array('id_object'=>$p['id_object'],'count'=>1));  
           //print_r($res);
            if ($res[(error)]==0) {  
                 
                $this->SetIndicatorValue(array('indicator'=>'money','value'=>(0-$this->objects[$index_object_in_array]['buy'])));
                $res=array((error)=>0,'msg'=>'Успешно');
                $this->SetGamerStat(array('stat'=>'object_buy'));
                 if ( count($radio)>0 ) $this->SetLifeObject(array((id_record)=>$radio['id'],(value)=>'-0.5'));
            }
        } 
        else $res=array((error)=>1,'error_msg'=>'Недостаточно денег');
    }
    } else $res=array((error)=>1,'error_msg'=>'Магазин недоступен подземлей. Поднимитесь на поверхность или активируйте приемник'); 
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}

public function TakeObject($p=array('json'=>0)) // поднимаем объект
{

    $row=$this->xyz; 
      //  print_r($row);

        if ($p['direction']=='left') $row['x']--;
        if ($p['direction']=='right') $row['x']++;      
        
        
            
        $id_object=$this->FieldSearchObjects(array('x'=>$row['x'],'y'=>$row['y'],'id_object'=>$p['id_object']));
    
        if (isset($id_object['id'])) { 
            if ($id_object['id_object']==8 || $id_object['id_object']==9 || $id_object['id_object']==10 || $id_object['id_object']==11) {
                $res=array((error)=>1,'msg'=>'Ящики и коробки нельзя забрать');
            }
            else{
                $this->update('map_objects_xy',array('id_object'=>0),"WHERE id=?",array($id_object['id'])); 
                
                $this->SetInventoryCount(array('id_object'=>(int)$id_object['id_object'],'count'=>1));   
                $res=array((error)=>0,'msg'=>'Ушло в инвентарь');
                $this->SetGamerStat(array('stat'=>'object_take'));
            }
        } else $res=array((error)=>1,'error_msg'=>'Нет нифига'.$id_object['id_object']);


    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;     
}

public function UseObject($p=array('json'=>0)) // используем объект из инвентаря
{
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];  
    $p['id_record']=(int)$p['id_record'];
    
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));
       
    $q="SELECT * FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object='{$p['id_object']}' AND count>0";
    $row=$this->getRow($q);
    $count_object=$row['count'];
    if (count($row)>0){     
              
        if ($p['id_object']==1 || $p['id_object']==8 || $p['id_object']==18){
            $p_temp=$p;
            $p_temp['json']=0; // вырубаем чтобы json не читался, чтобы передать в функцию
            $res=$this->SetObject($p_temp);
        }
        if($p['id_object']==2) {
            
            $this->SetLifeObject(array((id_record)=>$p['id_record'],(value)=>'-0.5'));
            
            $this->SetIndicatorValue(array('indicator'=>'oxygen','value'=>$this->objects[$index_object_in_array]['power']));
           // $this->SetInventoryCount(array('id_object'=>(int)$p['id_object'],'count'=>-1));   
            $res=array((error)=>0, 'msg'=>'Ваш кислород пополнен');
        }     
        if($p['id_object']==3) { // аптечка нужно добавить проверку на здоровье и на кислород выше усорвие
            $this->SetIndicatorValue(array('indicator'=>'heart','value'=>'1.5'));
            $this->SetInventoryCount(array('id_object'=>(int)$p['id_object'],'count'=>-1));   
            $res=array((error)=>0, 'msg'=>'Здоровье пополнено');
        }    

        if($p['id_object']==4) { // аптечка нужно добавить проверку на здоровье и на кислород выше усорвие
            $this->SetIndicatorValue(array('indicator'=>'heart','reset'=>1));
            $this->SetInventoryCount(array('id_object'=>(int)$p['id_object'],'count'=>-1));   
            $res=array((error)=>0, 'msg'=>'Здоровье пополнено');
        }          
                
        if($p['id_object']==6) { // динамит
            
            $p_temp=$p;
            $p_temp['json']=0; // вырубаем чтобы json не читался, чтобы передать в функцию
            $res=$this->SetObject($p_temp);
        }            
        if($p['id_object']==11) { // канат от лифта
            $res['error']=0;
            $p_temp=$p;
            $p_temp['json']=0; 
            
            
            // вырубаем чтобы json не читался, чтобы передать в функцию
            // $res=$this->SetObject($p_temp);
            // получаем координаты игрока
             

            $q="SELECT x,y,z FROM map_objects_xy WHERE id_user='{$_SESSION['user_id']}'";
            $xyz=$this->getRow($q);
            
            $row_object=$this->FieldSearchObjectsAndBlock(array('x'=>$xyz['x'], 'y'=>$xyz['y']));
            
            if ($row_object==0){
    // смотрим что вверху юзера
          
                $q="
                SELECT 
                    parent 
                FROM  elevator t1  
                WHERE (t1.id_object=10 OR t1.id_object=10)  AND t1.vis>0 AND 
                (t1.x={$xyz['x']} AND t1.y='".($xyz['y']-1)."' AND t1.z='{$xyz['z']}') ORDER by t1.id DESC LIMIT 1";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
                $parent1=(int)$this->getOne($q);             
    // смотри что внизу юазера            
                $q="
                SELECT 
                    parent 
                FROM  elevator t1  
                WHERE (t1.id_object=10 OR t1.id_object=10)  AND t1.vis>0 AND 
                (t1.x={$xyz['x']} AND t1.y='".($xyz['y']+1)."' AND t1.z='{$xyz['z']}') ORDER by t1.id DESC LIMIT 1";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
                $parent2=(int)$this->getOne($q);  
                
                $parent=$parent1+$parent2; // при сложении если когото не будет - будет равно тому кто не 0
                $merge=0;
                if ($parent2>0 && $parent1>0) { $parent=$parent1; $merge=1;}// выбираем первого родителя
                if ($parent>0){
                    $array=array(
                        'x'=>$xyz['x'],
                        'y'=>$xyz['y'],
                        'id_object'=>11,
                        'owner_object'=>$_SESSION['user_id']);                        
                    $id=$this->insertdata('map_objects_xy',$array);
                    
                    $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>11,
                            'parent'=>$parent,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y'],
                            'z'=>$xyz['z']
                        );
                    
                        $id_parent=$this->insertdata('elevator',$array);
                    
                    if ($merge==1){
                        $this->update('elevator',array('parent'=>$parent), " WHERE parent='$parent2'");
                    }
                    
                    
                } else {
                    $res=array('error'=>1,'error_msg'=>'Канат цепляется только к рабочему лифту');
                }
                  
                    
                    
                
                
                
            }
            else $res=array('error'=>1, 'error_msg'=>'Что-то мешает');             
            
           // $res=$this->SetObject(array('id_object'=>$p['id_object'], 'direction'=>'up'));
            
/*            
            $q="SELECT * FROM map_objects_xy WHERE x={$xyz['x']} AND y={$xyz['y']} AND z={$xyz['y']} AND (id_object=10 OR id_object=11)";
     

            // получаем параметры объекта
            $q="SELECT * FROM gamer_inventory WHERE id='{$p['id_record']}'";
            $object_info=$this->getRow($q);
            
            $q="SELECT id_block FROM map_xy WHERE x='{$xyz['x']}' AND (y>='{$xyz['y']}' AND y<={$xyz['y']}+19) AND id_block=0";
            $row=$this->getAll($q);
            //echo $q;
            if (count($row)>19){
                if ($row[1]['id_block']>0) $res=array((error)=>1, (error_msg)=>'Нет шахты');             
                $i=0;
                foreach ($row as $row)
                {
                    if ($i==0 || $i==19) $obj=10; else $obj=11; 
                    
                    // очищаем все на пути
                    $q="UPDATE map_objects_xy SET id_object=0 WHERE x={$xyz['x']} AND y=".($xyz['y']+$i)." AND z=0";
                  //  echo $q;
                    $this->query($q);
                  
                    $array=array(
                        'x'=>$xyz['x'],
                        'y'=>$xyz['y']+$i,
                        'id_object'=>$obj,
                        'owner_object'=>$_SESSION['user_id']);
                        
                    $id=$this->insertdata('map_objects_xy',$array);
                    if ($i==0)
                    { 
                        $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>10,
                            'parent'=>0,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y']+$i,
                            'z'=>0
                        );
                    
                        $id_parent=$this->insertdata('elevator',$array);
                        $this->update('elevator',array('parent'=>$id_parent)," WHERE id='{$id_parent}'");
                    }
                    
                    if ($i>0 && $i<19) {
                        $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>11,
                            'parent'=>$id_parent,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y']+$i,
                            'z'=>0
                        );
                        $this->insertdata('elevator',$array);
                    }
                    
                    if ($i==19) {
                        $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>10,
                            'parent'=>$id_parent,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y']+$i,
                            'z'=>0
                        );
                        $this->insertdata('elevator',$array);
                    }                    
                    
                    $i++;
                    
                }
                $this->SetInventoryCount(array('id_object'=>(int)$p['id_object'],'count'=>-1));   
            }
            else $res=array((error)=>1, (error_msg)=>'Шахта должна быть 20 блоков глубиной'); 
*/       
            
        }
        
        if($p['id_object']==16) { // лифт
            $res['error']=0;
            $p_temp=$p;
            $p_temp['json']=0; 
            
            
            // вырубаем чтобы json не читался, чтобы передать в функцию
            // $res=$this->SetObject($p_temp);
            // получаем координаты игрока
             
            $q="SELECT x,y,z FROM map_objects_xy WHERE id_user='{$_SESSION['user_id']}'";
            $xyz=$this->getRow($q);
/*            
            $q="SELECT * FROM map_objects_xy WHERE x={$xyz['x']} AND y={$xyz['y']} AND z={$xyz['y']} AND (id_object=10 OR id_object=11)";
            SELECT COUNT(t1.id_block, (SELECT SUM(id) FROM map_objects_xy t2 WHERE ( t1.x=t2.x AND t1.y=t2.y AND t1.z=t2.z AND (t2.id_object=10 OR t2.id_object=11) ) ) as elevator FROM map_xy t1  WHERE t1.x='20' AND (t1.y>='60' AND t1.y<=63+19) AND t1.id_block=0 ORDER by elevator DES
*/            

            // получаем параметры объекта
            $q="SELECT * FROM gamer_inventory WHERE id='{$p['id_record']}'";
            $object_info=$this->getRow($q);
            
            $q="SELECT count(t1.id_block) as id_block, COUNT(t2.id) as elevator FROM map_xy t1 LEFT JOIN map_objects_xy t2 ON ( t1.x=t2.x AND t1.y=t2.y AND t1.z=t2.z AND (t2.id_object=10 OR t2.id_object=11) ) WHERE t1.x='{$xyz['x']}' AND (t1.y>='{$xyz['y']}' AND t1.y<={$xyz['y']}+19) AND t1.id_block=0";
           // echo $q;
            $row=$this->getRow($q);
            
            //echo $q;
            if ($row['id_block']>19){ 
                if ($row['elevator']==0){
                             
                $i=0;                   
                
                // очищаем все на пути
                    $q="UPDATE map_objects_xy SET id_object=0, id_monster=0 WHERE x={$xyz['x']} AND ( y>=".($xyz['y'])." AND   y<=".($xyz['y']+19).") AND z=0";
                  //  echo $q;
                    $this->query($q);
                for ($i=0; $i<=19; $i++)
                {
                    if ($i==0 || $i==19) $obj=10; else $obj=11; 
                    
 
                  
                    $array=array(
                        'x'=>$xyz['x'],
                        'y'=>$xyz['y']+$i,
                        'id_object'=>$obj,
                        'owner_object'=>$_SESSION['user_id']);
                        
                    $id=$this->insertdata('map_objects_xy',$array);
                    if ($i==0)
                    { 
                        $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>10,
                            'parent'=>0,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y']+$i,
                            'z'=>0
                        );
                    
                        $id_parent=$this->insertdata('elevator',$array);
                        $this->update('elevator',array('parent'=>$id_parent)," WHERE id='{$id_parent}'");
                    }
                    
                    if ($i>0 && $i<19) {
                        $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>11,
                            'parent'=>$id_parent,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y']+$i,
                            'z'=>0
                        );
                        $this->insertdata('elevator',$array);
                    }
                    
                    if ($i==19) {
                        $array=array(
                            'id_map_objects_xy'=>$id,
                            'id_object'=>10,
                            'parent'=>$id_parent,
                            'x'=>$xyz['x'],
                            'y'=>$xyz['y']+$i,
                            'z'=>0
                        );
                        $this->insertdata('elevator',$array);
                    }                                        
                }
                $this->SetInventoryCount(array('id_object'=>(int)$p['id_object'],'count'=>-1));   
            
                }else $res=array((error)=>1, (error_msg)=>'Имеется препятствие'); 
            }
            else $res=array((error)=>1, (error_msg)=>'Шахта должна быть 20 блоков глубиной'); 

            
        }
        if($p['id_object']==15) { // приемник
            $res=$this->ActivationObject( array((id_object)=>$p['id_object'], (id_record)=>$p['id_record'], (index_object_in_array)=>$index_object_in_array));         
        }     
        
// типы объектов!!!        

        if($row['object_type']==4) { // рюкзак
            
            $p_temp=$p;
            $p_temp['json']=0; // вырубаем чтобы json не читался, чтобы передать в функцию
            // $res=$this->SetObject($p_temp);
            
            $res=$this->ActivationObject( array((id_object)=>$p['id_object'], (id_record)=>$p['id_record'], (index_object_in_array)=>$index_object_in_array));  
            if ($res['error']==0){
                if ( $res['activation']==1){
                    
                    $q="UPDATE  gamer_inventory t1,  gamer t2  SET t2.inventory_full=t2.inventory_full+t1.power, t1.object_act=1 WHERE t1.id='{$p['id_record']}' AND t1.id_user='{$_SESSION['user_id']}' AND t1.id_user=t2.id_user";                    
                    $this->query($q);       
                    $res=array((error)=>0,'msg'=>'Объект активирован');  
                }
                if ( $res['activation']==2){
                    $q="UPDATE gamer SET inventory_full=inventory_full-{$res['power']} WHERE id_user='{$_SESSION['user_id']}'";
                    $this->query($q);
                                        
                    $res=array((error)=>0,'msg'=>'Объект деактивирован');                     
                }                
                if ( $res['activation']==3){

                    
                    $q="UPDATE  gamer_inventory t1,  gamer t2  SET t2.inventory_full=t2.inventory_full-t1.power, t1.object_act=0 WHERE t1.id='{$res['id']}' AND t1.id_user='{$_SESSION['user_id']}'  AND t1.id_user=t2.id_user";
                    $this->query($q);
                    
                    
                    $q="UPDATE  gamer_inventory t1,  gamer t2  SET t2.inventory_full=t2.inventory_full+t1.power, t1.object_act=1 WHERE t1.id='{$p['id_record']}' AND t1.id_user='{$_SESSION['user_id']}'  AND t1.id_user=t2.id_user";
                    //echo $q;
                    $this->query($q);                  
                    
                    $res=array((error)=>0,'msg'=>'Объект активирован');  
                }                
                
            }
        
        
        }         
        if($row['object_type']==6) { //инструменты
            $res=$this->ActivationObject( array((id_object)=>$p['id_object'], (id_record)=>$p['id_record'], (index_object_in_array)=>$index_object_in_array));         
        }
        if($row['object_type']==7) { //оружие
            $res=$this->ActivationObject( array((id_object)=>$p['id_object'], (id_record)=>$p['id_record'], (index_object_in_array)=>$index_object_in_array));         
        }

      if($row['object_type']==9) { // ключи
            $res=$this->ActivationObject( array((id_object)=>$p['id_object'], (id_record)=>$p['id_record'], (index_object_in_array)=>$index_object_in_array));         
        }
        $this->SetGamerStat(array('stat'=>'object_use'));
          
    } else $res=array((error)=>1,'error_msg'=>'Нет в наличии');
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      


}

function ActivationObject($p=array('json'=>0)) // активация объекта
{
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];
    $p['id_record']=(int)$p['id_record'];    
    $p['index_object_in_array']=(int)$p['index_object_in_array']; 
    
        $q="SELECT id, power  FROM gamer_inventory WHERE object_type={$this->objects[$p['index_object_in_array']]['type']} AND object_act=1 AND count>0 AND  id_user='{$_SESSION['user_id']}'";
        $row2=$this->getRow($q);
            
            if ($row2['id']>0){ 
                if ($row2['id']==$p['id_record']){
                    
                    $q="UPDATE  gamer_inventory SET object_act=0 WHERE id='{$p['id_record']}' AND id_user='{$_SESSION['user_id']}'";
                    $this->query($q);
                                        
                    $res=array((error)=>0, (activation)=>'2', (id)=>$row2['id'], (power)=>$row2['power'],(msg)=>'Объект деактивирован'); 
                
                }
                else {
                    
                    $q="UPDATE  gamer_inventory t1  SET t1.object_act=0 WHERE t1.id='{$row2['id']}' AND t1.id_user='{$_SESSION['user_id']}' ";
                    $this->query($q);
                                       
                    $q="UPDATE  gamer_inventory t1 SET  t1.object_act=1 WHERE t1.id='{$p['id_record']}' AND t1.id_user='{$_SESSION['user_id']}' ";
                    $this->query($q);                  
                    
                     $res=array((error)=>0, (activation)=>'3', (id)=>$row2['id'], (power)=>$row2['power'],(msg)=>'Объект активирован'); 
                }
            }
            else {
                    $q="UPDATE  gamer_inventory t1 SET  t1.object_act=1 WHERE t1.id='{$p['id_record']}' AND t1.id_user='{$_SESSION['user_id']}'";
                    
                    $this->query($q);       
                     $res=array((error)=>0, (activation)=>'1', (id)=>$row2['id'], (power)=>$row2['power'],(msg)=>'Объект активирован');             
            }
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;                
}
public function SetLifeObject($p=array()) // id_record (ид объекта), value, reset-- изменяем жизнь объекту ПРОСТОМУ  который не влияет на индикаторы
{

    $p['json']=(int)$p['json'];   
    $p['id_record']=(int)$p['id_record'];
    $p['value']=(float)$p['value'];
        
    $q="UPDATE gamer_inventory SET life= life+{$p['value']}, count=IF( life+{$p['value']}>0,count,count-1), object_act=IF( life+{$p['value']}>0,object_act,0)   WHERE `id` = {$p['id_record']}";
    if (isset($p['reset']))  $q="UPDATE gamer_inventory SET life = life_full WHERE `id` = {$p['id_record']}";

    $this->query($q);
        
    $res['error']=0;

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;     
}
public function RemoveItemInventory($p=array('json'=>0)) // используем объект из инвентаря
{
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];
    $p['id_block']=(int)$p['id_block'];
    $p['all']=(int)$p['all'];

    if ($p['id_object']>0) {  if ($p['all']==0)    $res=$this->SetInventoryCount(array('id_object'=>$p['id_object'],'count'=>'-1')); else{
         
        $q="SELECT id, count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object='{$p['id_object']}' AND count>0";
        $id_object=$this->getRow($q);
       //echo  $q;
        $q="UPDATE gamer_inventory SET count=0 WHERE id='{$id_object['id']}' AND id_user='{$_SESSION['user_id']}'";
        $this->query($q);
        
        $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>0-$id_object['count']));
        $res=array('error'=>0);
    }}
    if ($p['id_block']>0)  { if ($p['all']==0)    $res=$this->SetInventoryCount(array('id_block'=>$p['id_block'],'count'=>'-1'));  else {
        
        $q="SELECT id, count FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_block='{$p['id_block']}' AND count>0";
        $id_block=$this->getRow($q);
       //echo  $q;
        $q="UPDATE gamer_inventory SET count=0 WHERE id='{$id_block['id']}' AND id_user='{$_SESSION['user_id']}'";
        $this->query($q);
        
        $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>0-$id_block['count']));
        $res=array('error'=>0);
    }    }

    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      


}

public function GetObjectOutChest($p=array('json'=>0)) // забираем объект из сундука
{
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];    
     $p['all']=(int)$p['all'];    
        
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));
//если инвентраь переполнен то создается сундук при копке или монстре
    $q="SELECT 
                (SELECT id FROM map_objects_xy t2 WHERE t1.x=t2.x AND t1.y=t2.y AND (t2.id_object=8 OR t2.id_object=9) AND t2.owner_object='{$_SESSION['user_id']}' LIMIT 1) as id_chest 
            FROM  map_objects_xy t1  
            WHERE 
                (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND 
                vis>0";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    $id_chest=(int)$this->getOne($q);    
    //echo $q;
    if ($id_chest==0){
        $res=array((error)=>1, (error_msg)=>'Нет сундука или ящика');
    }
    else{
        $q="SELECT count FROM gamer_inventory WHERE id_chest='{$id_chest}' AND id_object='{$p['id_object']}' AND count>0";
        $id_object_count=$this->getOne($q);
        if ($id_object_count>0){
            
            if ($p['all']>0) $inv_count=$id_object_count; else $inv_count='1';
            // echo $inv_count;
            $res=$this->SetInventoryCount(array('id_object'=>$p['id_object'],'count'=>$inv_count,'id_record'=>$p['id_record']));
            // создаем запись или обновляем
            
            //print_r($res); exit;
            if ($res['error']==0) { 
              /*
                $q="INSERT INTO gamer_inventory (id_user, id_chest, id_object) VALUES ('0', '{$id_chest}', '{$p['id_object']}') ON DUPLICATE KEY UPDATE  count=count-1";
                $this->query($q);               
                */
               
                $this->SetInventoryCountChest(array('id_chest'=>$id_chest,'id_object'=>$p['id_object'],'count'=>0-$inv_count,'id_record'=>$p['id_record']));
                 
                $res=array((error)=>0, (msg)=>'Забрал из ящика');
            }
            
        }
        else $res=array((error)=>1, (msg)=>'Нет объекта');
    }
    if ($res['error']==0){
        // проверяем наличие объектов и если пусто удаляем 
        $q="SELECT SUM(count) FROM gamer_inventory WHERE id_chest='{$id_chest}'";
        $count=$this->getOne($q);
        if ($count<=0){ $this->update('map_objects_xy', array('id_object'=>0, 'owner_object'=>0)," WHERE id='{$id_chest}' AND id_object=9");
        $res['msg']=$res['msg'].' Сундук пуст';}


    }
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      


}
public function GetBlockOutChest($p=array('json'=>0)) // используем объект из инвентаря
{
    $p['json']=(int)$p['json'];
    $p['id_block']=(int)$p['id_block'];    
    $p['id_record']=(int)$p['id_record'];       
    $p['all']=(int)$p['all'];
    $q="SELECT 
                (SELECT id FROM map_objects_xy t2 WHERE t1.x=t2.x AND t1.y=t2.y AND (t2.id_object=8 OR t2.id_object=9) AND t2.owner_object='{$_SESSION['user_id']}' LIMIT 1) as id_chest 
            FROM  map_objects_xy t1  
            WHERE 
                (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND 
                vis>0";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    $id_chest=(int)$this->getOne($q);    
    if ($id_chest==0){
        $res=array((error)=>1, (error_msg)=>'Нет сундука или ящика');
    }
    else{    
        
        
        if ($p['all']>0){ 
            $q="SELECT count FROM gamer_inventory WHERE id_block='{$p['id_block']}' AND id_chest='{$id_chest}' AND count>0";
            //echo $q;
            $inv_count=$this->getOne($q);

        }else $inv_count='1';
        
       
        
        $res=$this->SetInventoryCount(array('id_block'=>$p['id_block'],'count'=>$inv_count));
        // создаем запись или обновляем
        
        //print_r($res); exit;
        if ($res['error']==0) { 
         /*   $q="INSERT INTO gamer_inventory (id_user, id_chest, id_block) VALUES ('0', '{$id_chest}', '{$p['id_block']}') ON DUPLICATE KEY UPDATE  count=count-1";
            $this->query($q);       */
            
            $this->SetInventoryCountChest(array('id_chest'=>$id_chest,'id_block'=>$p['id_block'],'count'=>0-$inv_count, 'id_record'=>$p['id_record']));
        
        
            $res=array((error)=>0, (msg)=>'Забрал из ящика');
        }
    }
    if ($res['error']==0){
        // проверяем наличие объектов и если пусто удаляем 
        $q="SELECT SUM(count) FROM gamer_inventory WHERE id_chest='{$id_chest}'";
        $count=$this->getOne($q);
        if ($count<=0){ $this->update('map_objects_xy', array('id_object'=>0, 'owner_object'=>0)," WHERE id='{$id_chest}' AND id_object=9");
            $res['msg']=$res['msg'].' Сундук пуст';}
  

    }
        
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      
}

public function PutObjectToChest($p=array('json'=>0)) // используем объект из инвентаря
{
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];    
    $p['id_record']=(int)$p['id_record'];    
    $p['all']=(int)$p['all'];
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));

    
    $q="SELECT 
                (SELECT id FROM map_objects_xy t2 WHERE t1.x=t2.x AND t1.y=t2.y AND (t2.id_object=8 OR t2.id_object=9) AND t2.owner_object='{$_SESSION['user_id']}' LIMIT 1) as id_chest 
            FROM  map_objects_xy t1  
            WHERE 
                (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND 
                vis>0";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    $id_chest=(int)$this->getOne($q);    
    
    if ($id_chest==0){
        $res=array((error)=>1, (error_msg)=>'Нет сундука или ящика');
    }
    else{
        // создаем запись или обновляем
        /*
        $q="INSERT INTO gamer_inventory (id_user, id_chest, id_object) VALUES ('0', '{$id_chest}', '{$p['id_object']}') ON DUPLICATE KEY UPDATE  count=count+1";
        $this->query($q);
        */
        $q="SELECT count FROM gamer_inventory WHERE id_object='{$p['id_object']}' AND id_user='{$_SESSION['user_id']}' AND count>0";
        $id_object_count=$this->getOne($q);
        
        if ($p['all']>0) $inv_count=$id_object_count; else $inv_count='1';
         
        $res1=$this->SetInventoryCountChest(array('id_chest'=>$id_chest,'id_object'=>$p['id_object'], 'id_record'=>$p['id_record'],'count'=>$inv_count));
        if ($res1['error']==0) $res2=$this->SetInventoryCount(array('id_object'=>$p['id_object'],'count'=>0-$inv_count, 'id_record'=>$p['id_record']));
         $res=array((error)=>0, (msg)=>'Поместили в ящик');
        if ($res1['error']==1) { $res=$res1; $res['error_msg']=$res['error_msg'].' Ошибка сунудка';}
        if ($res2['error']==1) { $res=$res2;  $res['error_msg']=$res['error_msg'].' Ошибка инвентаря';   } 
    }

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      
}
public function PutBlockToChest($p=array('json'=>0)) // положить в сундук
{
    $p['json']=(int)$p['json'];
    $p['id_block']=(int)$p['id_block'];    
     $p['all']=(int)$p['all']; 
    $q="
        SELECT 
            (SELECT id 
            FROM map_objects_xy t2 
            WHERE t1.x=t2.x AND t1.y=t2.y AND (t2.id_object=8 OR t2.id_object=9) AND 
                t2.owner_object='{$_SESSION['user_id']}' LIMIT 1) as id_chest 
        FROM  map_objects_xy t1  
        WHERE 
            (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND  vis>0";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    //echo $q;
    $id_chest=(int)$this->getOne($q);    
    if ($id_chest==0){
        $res=array((error)=>1, (error_msg)=>'Нет сундука или ящика');
    }
    else{
         
           $q="SELECT count FROM gamer_inventory WHERE id_block='{$p['id_block']}' AND id_user='{$_SESSION['user_id']}' AND count>0";
           //echo $q;
        $id_block_count=$this->getOne($q);
        // создаем запись или обновляем
        //$q="INSERT INTO gamer_inventory (id_user, id_chest, id_block) VALUES ('0', '{$id_chest}', '{$p['id_block']}') ON DUPLICATE KEY UPDATE  count=count+1";
        //$this->query($q);
        /*
            $q="SELECT id, count FROM gamer_inventory WHERE id_block='{$p['id_block']}' AND id_chest='{$id_chest}'";
            $id_block=$this->getRow($q);
            
            if ($id_block['id']>0 ) { 
            if ($p['count']>0 || ($p['count']<0 && $id_block['count']>0)) {       
                $q="UPDATE gamer_inventory SET count=count+{$p['count']} WHERE id='{$id_block['id']}'";
                $this->query($q);
               } 
            }            
            else $this->insertdata('gamer_inventory', array((id_chest)=>'{$id_chest}', (id_block)=>$p['id_block'], (count)=>1));
            
            */
        
        if ($p['all']>0) $inv_count=$id_block_count; else $inv_count='1';
        
        $this->SetInventoryCountChest(array('id_chest'=>$id_chest,'id_block'=>$p['id_block'],'count'=>$inv_count));
        
        $this->SetInventoryCount(array('id_block'=>$p['id_block'],'count'=>0-$inv_count));
        $res=array((error)=>0, (msg)=>'Поместили в ящик');
    }



    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      


}
public function GetChestObjectList($p=array('json'=>0)) // список 
{
    $p['json']=(int)$p['json'];
    $p['id_object']=8;    
    $access=0;
    $index_object_in_array=array_search($p['id_object'], array_column($this->objects, 'id'));

    $q="SELECT t1.* FROM map_objects_xy t1, game_object t2
         WHERE 
            t1.x={$this->xyz['x']} AND t1.y={$this->xyz['y']} AND t1.z={$this->xyz['z']}  AND 
            id_object=t2.id AND
            (type=5)  LIMIT 1";
    // получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    //echo $q; exit;
    $id_chest=$this->getRow($q);   

    if (count($id_chest)) { 
        // проверяем чей сундук
        if ($id_chest['owner_object']==$_SESSION['user_id'])  $access=1;
        else {
            if ($id_chest<>8){ // если не сейф
                $q="SELECT id, power  FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND object_act=1 AND object_type=9 AND count=1";
                $key=$this->getRow($q);
                if ($key['id']>0){
                     $this->SetIndicatorValue(array('indicator'=>'inventory','value'=>'-1'));
                    // получаем силу ключа, чем выше power тем меньше вероятности получить 1 и открыть сундук
                    $access=rand(0,100-$key['power']);// echo $access;
                    $q="UPDATE gamer_inventory SET count=0, object_act=0 WHERE id={$key['id']} AND id_user='{$_SESSION['user_id']}'";
                    $this->query($q);  
                }
            }
        }
    }
    else $access=0;
        

        
 if ($access==1){       
    $q="UPDATE map_objects_xy SET owner_object='{$_SESSION['user_id']}' WHERE id='{$id_chest['id']}'";
    $this->query($q); 
          
        $q="
        SELECT       
            t1.*, 
            t2.title as title,
            t2.title as block_title,
            t3.title as object_title, t2.sell as price
        FROM 
            gamer_inventory t1 
            LEFT JOIN game_block t2 ON t2.id=t1.id_block
            LEFT JOIN game_object t3 ON t3.id=t1.id_object 
        WHERE t1.id_chest='{$id_chest['id']}' AND t1.count>0";
        $row=$this->getAll($q);

    foreach ($row as $item=>$item)
    {
        if ($row[$item]['life_full']>0) $row[$item]['life_percent']=round(($row[$item]['life']/$row[$item]['life_full'])*100,2);
        else $row[$item]['life_percent']=0;
    }
        
        $this->smarty->assign('list', $row);
         $this->smarty->assign('chest', 1);
        $html=$this->smarty->fetch('gamer_inventory.html'); 
        $res=array((error)=>0, (inventory_html)=>$html);
        
    }
    else {
        $res['error']=1; 
        $res['error_msg']='Нет ящика или нет доступа ';
    }
/*
    $q="SELECT t1.*, (SELECT id ) FROM  map_objects_xy t1  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0 ) AND vis>0 AND ";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    $row=$this->getRow($q);    
       
    $q="SELECT count, object_act FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND id_object='{$p['id_object']}'";
    $row=$this->getRow($q);
    
    $count_object=$row['count'];
    if ($count_object>0){     
        if ($row['object_act']==1) $res=array((error)=>1,'error_msg'=>'Объект в действии');
        else{      
        if ($p['id_object']==1){
            $p_temp=$p;
            $p_temp['json']=0; // вырубаем чтобы json не читался, чтобы передать в функцию
            $res=$this->SetObject($p_temp);
        }
 


        $this->SetGamerStat(array('stat'=>'object_use'));
       }    
    } else $res=array((error)=>1,'error_msg'=>'Нет в наличии');
 */   
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      


}

function boom($p=array())
{
    
    $q="SELECT * FROM map_objects_xy WHERE owner_object='{$_SESSION['user_id']}' AND (id_object=6)";
    $row=$this->getAll($q);
    if (count($row)>0) 
    foreach ($row as $row)
    {
        // готовим убить все в квадрате 3х3 от бомбы - монстры и блоки
        $x_min=$row['x']-1; $x=$row['x']; $x_max=$row['x']+1;
        $y_min=$row['y']-1; $y=$row['y']; $y_max=$row['y']+1;

if ($row['id_object']==6)   {
         // монстры и объекты
        $q="UPDATE map_objects_xy SET id_monster=0 WHERE x>={$x_min} AND x<={$x_max} AND y>={$y_min} AND y<={$y_max}";
        $this->query($q);   
} 
/*        // монстры и объекты
        $q="UPDATE map_objects_xy SET id_monster=0, id_object=0 WHERE x>={$x_min} AND x<={$x_max} AND y>={$y_min} AND y<={$y_max}";
        $this->query($q);
        
        // блоки
        $q="UPDATE map_xy SET id_block=0 WHERE x>={$x_min} AND x<={$x_max} AND y>={$y_min} AND y<={$y_max}";
        $this->query($q);

        $q="UPDATE map_objects_xy t1, gamer t2 SET x=0, y=0, t2.heart=t2.heart_full, t2.oxygen=t2.oxygen_full  WHERE x>={$x_min} AND x<={$x_max} AND y>={$y_min} AND y<={$y_max} AND t1.id_user>0 AND t1.id_user>0 AND t2.id_user=t1.id_user";
        $this->query($q);
 */        
        
        $this->update('map_objects_xy',array('id_object'=>0),"WHERE id='{$row['id']}'"); 
    }
    $res=array((error)=>0,'msg'=>'Бууум');
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      
}

function DropBlock($p=array())
{
    //print_r($p);
    $p['xyz_id']=(int)$p['xyz_id'];
    
    $q="SELECT * FROM map_xy WHERE id='{$p['xyz_id']}' AND id_block=11";
    $row=$this->getRow($q);
    
    if (($row['id'])>0) 
    {
        $q="UPDATE map_xy SET id_block=0 WHERE id='{$p['xyz_id']}' ";
        $this->query($q);
        
        
        $res=$this->FindDeep(array('x'=>$row['x'], 'y'=>$row['y']));        
        $q="UPDATE map_xy SET id_block=11, dm='0.50' WHERE x='{$row['x']}' AND y='".($res['y']-1)."' AND z='{$row['z']}' ";
        $this->query($q);
        $q="UPDATE  map_objects_xy  SET id_monster=0 WHERE x='{$row['x']}' AND( y>='{$row['y']}' AND  y<='".($res['y']-1)."')AND z='{$row['z']}' ";
        $this->query($q);
        
        $q="UPDATE map_objects_xy t1, gamer t2 SET x=t2.x_home, y=0, t2.heart=t2.heart_full, t2.oxygen=t2.oxygen_full  WHERE x='{$row['x']}'  AND ( y>='{$row['y']}' AND  y<='".($res['y']-1)."') AND  z='{$row['z']}'  AND t1.id_user>0 AND t1.id_user>0 AND t2.id_user=t1.id_user";
        $this->query($q);
                
/*
         // монстры и объекты
        $q="UPDATE map_objects_xy SET id_monster=0 WHERE x>={$x_min} AND x<={$x_max} AND y>={$y_min} AND y<={$y_max}";
        $this->query($q);   
*/


    }
    
    $res=array((error)=>0);
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;      
}

function EmergencyCall($p=array())
{
   $p['json']=(int)$p['json'];
   


        $q="SELECT money, emergency_call, x_home FROM gamer t1, gamer_stat t2 WHERE t1.id_user='{$_SESSION['user_id']}' AND t1.id_user=t2.id_user";   
        $row=$this->getRow($q);
         $price=round((($row['emergency_call'])*10)*2.5);
        if ($row['money']>= $price) {
            
            $this->SetIndicatorValue(array('indicator'=>'money','value'=>(0-$price)));
            $price=round((($row['emergency_call']+1)*10)*2.5);
            $res=array((error)=>0,'msg'=>'Успешно', 'emergency_price'=>$price);
            $this->SetGamerStat(array('stat'=>'emergency_call'));
            
            $this->update('map_objects_xy',array('x'=>$row['x_home'], 'y'=>0),"WHERE id_user=? AND id_object=0",array(intval($_SESSION['user_id'])));   
            
            
        } 
        else $res=array((error)=>1,'error_msg'=>'Недостаточно денег');
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}

function GetStatAll($p=array())
{
    $p['json']=(int)$p['json'];

    $q="SELECT * FROM  gamer_stat WHERE id_user='{$_SESSION['user_id']}'";
    $row=$this->getRow($q);    
    $this->smarty->assign('my_stat', $row);
    
    $q="SELECT t1.deep, t1.id_user, t2.avatar, t2.first_name FROM  gamer_stat t1, user t2 WHERE t2.id=t1.id_user ORDER by deep DESC LIMIT 3";
    $row=$this->getAll($q);    
    $this->smarty->assign('top_digger', $row);    

      
    $q="SELECT t1.kill_monster, t1.id_user, t2.avatar, t2.first_name FROM  gamer_stat t1, user t2 WHERE t2.id=t1.id_user ORDER by kill_monster DESC LIMIT 3";
    $row=$this->getAll($q);    
    $this->smarty->assign('top_monster', $row);    

    
    $q="SELECT t1.deep, t1.id_user, t2.avatar, t2.first_name FROM  gamer_stat t1, user t2 WHERE t2.id=t1.id_user ORDER by deep DESC LIMIT 3";
    $row=$this->getAll($q);    
    $this->smarty->assign('top_digger', $row);    
    
        
    $q="SELECT t1.money, t1.id_user, t2.avatar, t2.first_name FROM  gamer t1, user t2 WHERE t2.id=t1.id_user AND money>100 ORDER by money DESC LIMIT 3";
    $row=$this->getAll($q);    
    $this->smarty->assign('top_money', $row);      
    
        
    $q="SELECT t1.count, t1.id_user, t2.avatar, t2.first_name FROM  gamer_stat_item t1, user t2 WHERE t1.id_block=9 AND t2.id=t1.id_user ORDER by count DESC LIMIT 3";
    $row=$this->getAll($q);    
    $this->smarty->assign('top_gold', $row);      
    
    
    $q="SELECT t1.id, t2.id_block FROM  game_block t1 LEFT JOIN gamer_stat_item t2 ON (t2.id_block=t1.id AND t2.id_user='{$_SESSION['user_id']}') WHERE t1.id<>6 AND t1.id<>7 AND t1.id<>11 AND t1.id<>1 ORDER by t1.type, t1.id";
//echo $q;
    $row=$this->getAll($q);    
    $this->smarty->assign('found_block', $row);        
    
    $html=$this->smarty->fetch('gamer_stat.html'); 
    $res=array((error)=>0, (stat_html)=>$html);
        

    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}


function WalkElevator($p=array()) // direction up=1 down=2 left=3 right=4
{
    $p['json']=(int)$p['json'];   
    $p['direction']=(int)$p['direction'];
    
    $p['floor']=(int)$p['floor'];    
    // необъодима проверка лифта, есть ли он, в рабочем ли состоянии, правильно ли указан этаж
    
/*    
    // получаем текущую позицию игрока
    $q="SELECT * FROM  map_objects_xy t1  WHERE (t1.id_user='{$_SESSION['user_id']}' ) AND t1.vis>0";
    $row=$this->db->getRow($q); 
    
    $x_temp=$row['x'];
    $y_temp=$row['y'];
*/
    
    $q="UPDATE map_objects_xy t1  SET y={$p['floor']} WHERE t1.id_user='{$_SESSION['user_id']}' ";
    $this->query($q);    
    $res=array('error'=>0, 'msg'=>'Готово');
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;    
}


//--Chat
function GetMessageList($p=array())
{
    $p['json']=(int)$p['json'];
    $_SESSION['message_last']=(int)$_SESSION['message_last'];
    
    if ($_SESSION['message_last']==0) $q="SELECT t1.*, t2.avatar FROM  message t1, user t2 WHERE t1.id_user=t2.id ORDER by t1.id DESC LIMIT 50";
    else  $q="SELECT t1.*, t2.avatar, t2.first_name, t2.email, t3.is_ready, t3.is_ambush FROM  message t1, user t2 LEFT JOIN gamer_pvp t3 ON t3.id_user=t2.id WHERE t1.id_user=t2.id AND t1.id>'{$_SESSION['message_last']}' ORDER by t1.id DESC";
    //echo $q;, t3.*
    $row=$this->getAll($q);    
    
    if (count($row)>0 && $row[0]['id']>$_SESSION['message_last']) 
    {
        $_SESSION['message_last']=$row[0]['id'];
        $this->smarty->assign('message_list', $row);        
    
        $html=$this->smarty->fetch('gamer_chat_message.html'); 
        $res=array((error)=>0, (html)=>$html);}
    else {
        $res=array((error)=>1);
    }
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}

function CreateMessage($p=array())
{
    $p['json']=(int)$p['json'];
    $p['html']=(int)$p['html'];
    if ($p['html']==0) $p['text']=htmlspecialchars(mysql_escape_string($p['text']));
    
    
    $arr=array(
        'id_user'=>$_SESSION['user_id'],
        'room'=>0,
        'text'=>$p['text']
    );
    $this->insertdata('message', $arr);    
    
  
    $html=$this->smarty->fetch('gamer_chat_message.html'); 
    $res=array((error)=>0, (html)=>$html);
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}
//--------

function GetElevatorInfo($p=array())
{
    $p['json']=(int)$p['json'];
    
    $q="
    SELECT 
        parent 
    FROM  map_objects_xy t1, elevator t2  
    WHERE (t1.id_user='{$_SESSION['user_id']}' AND t1.id_object=0 ) AND t1.vis>0 AND 
    ((t1.x=t2.x) AND (t1.y=t2.y) AND (t1.z=t2.z)) ORDER by t2.id DESC LIMIT 1";// получаем текущее положение (можно перенести в session чтобы не запрашивать каждый раз)
    $parent=$this->getOne($q); 
       
    $q="SELECT y, id FROM elevator WHERE parent='$parent' AND id_object=10 ORDER by y";
    $list=$this->getAll($q);
    
    if (count($list)>0) $res=array('error'=>0, 'list'=>$list);
    else $res=array((error)=>1,'error_msg'=>'Лифт недоступен');
     
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;        
}


function UseMagic($p=array()) // Используем магию - magic - 1:ремонт life, 2:life_full, 3:power_dig, 4: power_kick
{
    /* 
        Таблица scheme_magic
        id_object - на которым проходит воздействие
        id_block - блок которые используется в качестве силы магии
        block_count - число блоков которые потребуются для магии
        life - если какое то из нижних полей не затронуто то можно восстановить здоровье предмета
        life_full - увеличиваем жизнь +!
        power - сила (вместимость +1 ) RAND 100- power 
        power_dig
        power_kick
        
        level который должен быть у игрока - пока 0
        type_magic- тип магии
        
    */
    
    $p['json']=(int)$p['json'];
    $p['id_object']=(int)$p['id_object'];
    $p['id_record']=(int)$p['id_record'];
    $p['magic']=(int)$p['magic'];   
    
    // находим схему и тут же проверяем наличие нужных блоков
    $q="SELECT t1.* FROM scheme_magic t1, gamer_inventory t2 WHERE t1.type_magic='{$p['magic']}' AND t1.id_block=t2.id_block AND t1.block_count<=t2.count AND t2.id_user='{$_SESSION['user_id']}' ORDER by t2.count DESC LIMIT 1";
    $scheme=$this->getRow($q);
    
    //если схема найдена все ок
    if (count($scheme)>0) {
        // первый типа магии
        if ($p['magic']==1){
            // восстанваливаем здоровие инструменты
            $q="UPDATE gamer_inventory SET life=life_full WHERE id='{$p['id_record']}' AND id_user='{$_SESSION['user_id']}'";  
            $this->query($q);         
        }

        if ($p['magic']==2){
            // восстанваливаем здоровие инструменты
            $q="UPDATE gamer_inventory SET life_full=life_full+1 WHERE id='{$p['id_record']}' AND id_user='{$_SESSION['user_id']}'";  
            $this->query($q);         
        }

        if ($p['magic']==3){
            // восстанваливаем здоровие инструменты
            $q="UPDATE gamer_inventory SET power_dig=power_dig+1 WHERE id='{$p['id_record']}' AND id_user='{$_SESSION['user_id']}'";  
            $this->query($q);         
        }
 
 
        if ($p['magic']==4){
            // восстанваливаем здоровие инструменты
            $q="UPDATE gamer_inventory SET power_kick=power_kick+1 WHERE id='{$p['id_record']}' AND id_user='{$_SESSION['user_id']}'";  
            $this->query($q);         
        }     
 
        if ($p['magic']==5){
            // восстанваливаем здоровие инструменты
            $q="UPDATE gamer_inventory SET power=power+1 WHERE id='{$p['id_record']}' AND id_user='{$_SESSION['user_id']}'";  
            $this->query($q);         
        }         
        // убираем из инвентаря блоки
       // $this->SetInventoryCount(array('id_block'=>$scheme['id_block'],'count'=>0-$scheme['block_count'])); 
         $res=array('error'=>0, 'msg'=>'Готово');
    }
    else  $res=array('error'=>1, 'error_msg'=>'Нет необходимых элементов');
   
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;        
}

// простые задачи
function CreateTaskEasy($p=array())
{
    $p['json']=(int)$p['json'];
    $p['type']=(int)$p['type'];
    
    if ($p['type']==0) $p['type']=rand(1,3);
    
    if ($p['type']==1) {
        // создаем случаеное задание стастистики - проти сто метров, разбить 20 блоков и т.д.

        $rand=rand(1,count($this->task_events));
        $rand_count=rand(1,$this->task_events[$rand]['max_random']);
        $array=array(
            'id_user'=>$_SESSION['user_id'],
            'id_stat'=>$this->task_events[$rand]['id'],
            'count'=>$rand,
            'start_count'=>$this->getOne("SELECT {$this->task_events[$rand]['field']} FROM gamer_stat WHERE id_user='{$_SESSION['user_id']}'")          
        );
    } 
    if ($p['type']==2) {
        // задание на объекты
    } 
    if ($p['type']==3) {
        // задание на блоки
        // выгружаем то что есть у игрока
        $q="SELECT t1.id FROM  game_block t1 INNER JOIN gamer_stat_item t2 ON (t2.id_block=t1.id AND t2.id_user='{$_SESSION['user_id']}') WHERE t1.id<>6 AND t1.id<>7 AND t1.id<>11 ORDER by t1.type, t1.id";
        $gamer_block=$this->getAll($q);
        if (count($gamer_block)>0) { $id_block=rand(0,count($gamer_block)-1);
                            $id_block=array_search($gamer_block[$id_block]['id'], array_column($this->blocks, 'id')); }
        else $id_block=rand(0,4);
        $array=array(
            'id_user'=>$_SESSION['user_id'],
            'id_block'=>$this->blocks[$id_block]['id'],
            'count'=>$this->blocks[$id_block]['type']==2?rand(1,round(($this->blocks[$id_block]['chance']/100)/2)):rand(1,round($this->blocks[$id_block]['chance']/100)),            
        );
        
    }     
    //print_r($array);
    $this->insertdata('task',$array);
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;          
}
 function GetTaskEasyList($p=array())
 {
    $p['json']=(int)$p['json'];
    $q="SELECT * FROM task WHERE id_user='{$_SESSION['user_id']}' AND vis>0 AND vis<3";
    $row=$this->getAll($q);    
    
    if (count($row)>0){
        foreach($row as $item=>$key)
        {
            $row[$item]['title']= $this->task_events[$row[$item]['id_stat']]['title']." ".$row[$item]['count']." ".$this->task_events[$row[$item]['id_stat']]['post_fix'];

        }
        $this->smarty->assign('task',$row);
        $html=$this->smarty->fetch('gamer_task.html');
        $res=array(
            'error'=>0,
            'html'=>$html,
            'row'=>$row        
        );
        
    } else {          
        $this->CreateTaskEasy(array('type'=>3));
        $this->CreateTaskEasy(array('type'=>3));
        $this->CreateTaskEasy(array('type'=>3));
        
        $res=$this->GetTaskEasyList();
    }
     
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;          
 }

function AcceptTaskEasy($p=array())
{
    $p['json']=(int)$p['json'];
    $p['id_task']=(int)$p['id_task'];
    
    if ($p['id_task']>0){
        $this->update('task',array('vis'=>1),"WHERE id='{$p['id_task']}' AND id_user='{$_SESSION['user_id']}'");
        $res=array('error'=>0);
    } else $res=array('error'=>1);
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}

function ResetTaskEasy($p=array())
{
    $p['json']=(int)$p['json'];
    $p['id_task']=(int)$p['id_task'];
    
    if ($p['id_task']>0){
        if ($this->xyz['money']-10>0){
        $this->update('task',array('vis'=>3),"WHERE id='{$p['id_task']}' AND id_user='{$_SESSION['user_id']}'");
        $rand=rand(1,3);
        if ($rand==2) $this->CreateTaskEasy(array('type'=>3)); else $this->CreateTaskEasy(array('type'=>$rand));
        $this->SetIndicatorValue(array('indicator'=>'money', 'value'=>0-10));
        $res=array('error'=>0);
        } else  $res=array('error'=>1,'error_msg'=>'Нет денег для сброса задания');
    } else $res=array('error'=>1);
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}

function DoneTaskEasy($p=array())
{
    $p['json']=(int)$p['json'];
    $p['id_task']=(int)$p['id_task'];
    $p['id_type']=(int)$p['id_type']; 
    
    $res=array('error'=>1);
     if ($p['id_type']==1){
        $q="SELECT t2.* FROM task t2 WHERE t2.id='{$p['id_task']}' AND  t2.id_user='{$_SESSION['user_id']}'";
        //echo $q;
        $row=$this->getRow($q); 
        
        if (count($row)>0){
            // убираем из инвентаря кол-во блоков
            $q="SELECT {$this->task_events[$row['id_stat']]['field']} FROM gamer_stat WHERE id_user='{$_SESSION['user_id']}'";
        //    echo $this->events[$row['id_stat']]['field']; exit;
            $new_count=$this->getOne($q);
            //echo $new_count;
            if ($new_count>=($row['start_count']+$row['count'])) { 
                $money=rand(10,100); // исправить чтобы случайность была от рандома_мах
                $res=array('error'=>0,'msg'=>"Задание выполнено! Вы получили {$money} монет");
            }else {
                $res=array((error)=>1, (error_msg)=>'Выполните условие задания');
            }

        }  
        else $res=array((error)=>1, (error_msg)=>'Выполните условие задания');
    }      
    if ($p['id_type']==3){
    $q="SELECT t2.* FROM gamer_inventory t1, task t2 WHERE t2.id='{$p['id_task']}' AND t2.count<=t1.count AND t2.id_block=t1.id_block AND t2.id_user=t1.id_user";
    $row=$this->getRow($q); 
  // echo $q;
    //exit;
        if (count($row)>0){
            // убираем из инвентаря кол-во блоков
            $res=$this->SetInventoryCount(array('id_block'=>$row['id_block'],'count'=>0-$row['count']));
            // добавляем денег пользователию
            if ($res['error']==0){
                $index_object_in_array=array_search($row['id_block'], array_column($this->blocks, 'id'));
                $money=$this->blocks[$index_object_in_array]['sell']*$row['count']+rand(10,100);                
            
                $res=array('error'=>0,'msg'=>"Задание выполнено! Вы получили {$money} монет");}
        }  
        else $res=array((error)=>1, (error_msg)=>'Выполните условие задания');
    }
    
    if ($res['error']==0)
    {
        $this->SetIndicatorValue(array('indicator'=>'money', 'value'=>$money));
        $this->update('task',array('vis'=>0),"WHERE id='{$p['id_task']}' AND id_user='{$_SESSION['user_id']}'");
        $rand=rand(1,3);
        if ($rand==2) $this->CreateTaskEasy(array('type'=>3)); else $this->CreateTaskEasy(array('type'=>$rand));
    }
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;       
}

function SetLevelUp($p=array()) // увеличиваем уровень игрока
{
    /*
        Входные данные
        level - уровень который надо увеличить, по умолчанию: main
        value - значение на которое нужно увеличить, по умолчанию: 0.01
        
        Примеры вызова
        SetLevelUp(); Увеличит Main уровень +0.01
        SetLevelUp(array('level'=>'social')) Увеличит Социальный уровень +0.01
        SetLevelUp(array('value'=>0.02))  Увеличит Главный +0.02
    */
    // передается уровень
    $p['level']=isset($p['level'])?$p['level']:'main';
    // передается значение
    $p['value']=isset($p['value'])?(float)$p['value']:0.01;
    // существует ли запись левела
    $q="SELECT id FROM gamer_level WHERE id_user='{$_SESSION['user_id']}'";
    $is_level=(int)$this->getOne($q);
    // если нет создаем (потом можно не проверять. создается при старте игры)
    if ($is_level==0) $this->insertdata('gamer_level', array('id_user'=>$_SESSION['user_id']));
    
    // короткие переменные для запроса
    $lvl=$p['level'];
    $val=$p['value'];
    $q="
    UPDATE gamer_level
    SET
        {$lvl}_now={$lvl}_now+{$val},
        {$lvl}_max=IF({$lvl}_now+{$val}>={$lvl}_max,{$lvl}_max*3,{$lvl}_max),
        {$lvl}=IF({$lvl}_now+{$val}>={$lvl}_max,{$lvl}+1,{$lvl})

    WHERE id_user='{$_SESSION['user_id']}'";//echo $q;
    $this->query($q);
    
    
    $res=array('error'=>0);
    return  $res;           
}
function GetLevel($p=array())
{
    $p['json']=(int)$p['json'];
    // игрок чей левел получаем - по умолчанию из Сессии
    $p['id_user']=isset($p['id_user'])?$p['id_user']:$_SESSION['user_id'];
    
    $q="SELECT * FROM gamer_level WHERE id_user='{$p['id_user']}'";    
    $row=$this->getRow($q);
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;      
}

// shop
function RefreshShop($p=array())
{
    $p['json']=(int)$p['json']; 
    
    
    $q="SELECT * FROM gamer_inventory t1, game_block t2 WHERE t1.id_user='{$_SESSION['user_id']}' AND t1.count>0 AND t2.id=t1.id_block";
    $row=$this->getAll($q);    
    //print_r($row);
    if (count($row)>0){
     }  
        
        $this->smarty->assign('my_items',$row);
        $this->smarty->assign('shop_items',$this->objects);
        
        $q="SELECT t1.id, t2.id_block, t1.sell, t1.sell_stock, t1.title FROM  game_block t1 LEFT JOIN gamer_stat_item t2 ON (t2.id_block=t1.id AND t2.id_user='{$_SESSION['user_id']}') WHERE t1.id<>6 AND t1.id<>7 AND t1.id<>11 AND t1.id<>1 ORDER by t1.type, t1.id";
        $row=$this->getAll($q);       
        $this->smarty->assign('stock_items',$row);
        //print_r($this->blocks);
        $html=$this->smarty->fetch('gamer_shop.html');
        $res=array(
            'error'=>0,
            'html'=>$html,
            'row'=>$row        
        );
        
    
    
    
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;        
}
//-------
// pvp
function SetPVPReady($p=array())
{
    $p['json']=(int)$p['json']; 
    $p['value']=(int)$p['value'];
    
    $q="INSERT INTO gamer_pvp (id_user, is_ready) VALUES ('{$_SESSION['user_id']}', '{$p['value']}') ON DUPLICATE KEY UPDATE  is_ready='{$p['value']}'";
    $this->query($q);  
    if ($p['value']==0) $this->SetPVPAmbush(array('value'=>1));
     
     
    $res=array('error'=>0,'is_ready'=>$p['value']);

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;      
}

function SetPVPAmbush($p=array())
{
    $p['json']=(int)$p['json']; 
    $p['value']=(int)$p['value'];
    if (!isset($p['id_user'])) $p['id_user']=$_SESSION['user_id']; else $p['id_user']=(int)$p['id_user'];
    
    $q="INSERT INTO gamer_pvp (id_user, is_ambush) VALUES ('{$p['id_user']}', '{$p['value']}') ON DUPLICATE KEY UPDATE  is_ambush='{$p['value']}'";
    $this->query($q);  
    if ($p['value']==0) $this->CreateMessage(array('text'=>'<strong>Вышел драться</strong>', 'html'=>1));
    if ($p['value']==1) $this->CreateMessage(array('text'=>'<strong>Спрятался</strong>', 'html'=>1));
          
    $res=array('error'=>0,'is_ready'=>$p['value']);

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;      
}

function AttackPVP($p=array())
{
    $p['json']=(int)$p['json']; 
    $p['target']=(int)$p['target'];
    
    $q="SELECT * FROM gamer_pvp WHERE id_user='{$_SESSION['user_id']}'";
    $pvp=$this->getRow($q);
    if ($pvp['is_ambush']==0 && $pvp['is_ready']==1 ){
    $q="SELECT count  FROM gamer_inventory WHERE id_user='{$_SESSION['user_id']}' AND object_type=3 AND count>0";
    $bomb=$this->getOne($q);
    
    if ($bomb>0){
        $q="SELECT CONCAT(t1.first_name,'@',t1.id) name, t2.* FROM user t1, gamer_pvp t2 WHERE t1.id=t2.id_user AND t1.id='{$p['target']}'";
        $target_info=$this->getRow($q);       
        if ($target_info['is_ready']==1 && $target_info['is_ambush']==0) { $heart=rand(1,100)/100; 
            $res=$this->SetIndicatorValue(array('indicator'=>'heart', 'value'=>0-$heart, 'id_user'=>$p['target']));
            if ($res['row']['heart']<=0) 
            { 
                $money=rand(10,50);
                $this->SetIndicatorValue(array('indicator'=>'money', 'value'=>0-$money, 'id_user'=>$p['target']));
                $this->SetIndicatorValue(array('indicator'=>'money', 'value'=>$money));
                                
                $message="<strong class='text-success'>Убил, {$target_info['name']}. Выпало {$money} монеты</strong>"; 
                $this->SetGamerStat(array('stat'=>'kill_user', 'value'=>1));
               // --- востанавливаем здоровье - (убираем деньги, отдаем игроку который убил, выключаем is_ambsu) 
               $this->SetIndicatorValue(array('indicator'=>'heart', 'reset'=>1, 'id_user'=>$p['target']));
               $this->SetPVPAmbush(array('id_user'=>$p['target'],'value'=>1));
            }
            else $message="<strong class='text-success'>Атаковал, {$target_info['name']} -{$heart}hp</strong>";
        }
        if ($target_info['is_ready']==1 && $target_info['is_ambush']==1) { $heart=0.001; $message="<strong class='text-success'>Атаковал, {$target_info['name']} -{$heart}hp</strong>";}
        $this->CreateMessage(array('text'=>$message, 'html'=>1));
    } else $this->CreateMessage(array('text'=>'<strong class="text-danger">Попытался атаковать, да нечем</strong>', 'html'=>1));
    } else $res=array('msg'=>'Нажмите <strong>Воевать</strong>');
          
    $res=array('error'=>0);

    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;      
}
//------











//----------------- 3D
public function MapLoad5x7_3D($inside=0){    // СОЗДАЕТСЯ карта 5 на 5 + две подопнительные строки вверх и вниз

      
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    
    
    $sql='';
    for ($z=-2;$z<=2; $z++)
    for ($y=-4;$y<=4;$y++)
    {
        $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."' AND t1.z='".($row['z']+$z)."')";  
        if ($y<4) $sql.=" OR ";  
    }    
    
    $q2="SELECT * FROM  map_xy t1  WHERE $sql ORDER BY z,y DESC,x";
    //echo $q;
    $row2=$this->db->getAll($q2); 

if (count($row2)<(44*5)){
    for ($z=-2;$z<=2; $z++)    
        for ($y=-4;$y<=4;$y++){
            for ($x=-2;$x<=2;$x++)
            {
                $this->FieldInsert($row['x']+$x,$row['y']+$y,$row['z']+$z);
            }
        }    
    }    
}
public function MapLoad5x7_v2_3D($p=array())
{

   // if ($p[0]['json']==1) { $p=$p[0]; $res=$p['json']; }
    $p['json']=(int)$p['json'];
    // вторая версия
      
    $q="SELECT * FROM  map_objects_xy  WHERE (id_user='{$_SESSION['user_id']}' AND id_object=0) AND vis>0";
    $row=$this->db->getRow($q);    
    $this->MapLoad5x7_3D(1);
    $sql='';
    for ($z=-2;$z<=2; $z++)
    for ($y=0;$y<=1;$y++)
    {
        $sql.="(t1.x>='".($row['x']-2)."' AND  t1.x<='".($row['x']+2)."' AND t1.y='".($row['y']+$y)."' AND t1.z='".($row['z']+$z)."')";  
        if ($y<4) $sql.=" OR ";      
    }    
    $q2="SELECT t1.* FROM  map_xy t1  WHERE  $sql id<1 ORDER BY t1.y DESC, t1.z, t1.x";
//   echo $q2;// exit;
    $row2=$this->getAll($q2); 

    
    $q3="
        SELECT t1.*, t2.id_object obj, t2.id_user gamer, t2.id_monster, t3.type as monster_type
        FROM  
            map_xy t1, 
            map_objects_xy t2 LEFT JOIN monster_bio t3 ON t3.id=t2.id_monster
        WHERE     
            ($sql t1.id<0) AND  (t2.x=t1.x AND t2.y=t1.y AND t2.z=t1.z) AND 
            t2.vis>0 AND (t2.id_object>0 OR (t2.id_user>0 AND t2.id_user<>'{$_SESSION['user_id']}') OR id_monster>0) 
        ORDER BY t1.z,  t1.y DESC, t1.x";
    //echo $q;
    $row3=$this->db->getAll($q3);     

    
    if ($_SESSION['pole']!=$row2) { $_SESSION['pole']=$row2;  $res['pole']=$row2; $res['pole_new']=1;} else {$res['pole_new']=0; }
    if ($_SESSION['objects']!=$row3 || $res['pole_new']==1) { $_SESSION['objects']=$row3; $res['objects']=$row3;$res['objects_new']=1;} else { $res['objects_new']=0;}
    
    $inventory=$this->GetBlockList();
    $indicator=$this->GetIndicatorList();

//print_r($indicator);
    $res=$res+$inventory+$indicator;
/*    
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return array($row2,$row3); 
        else {
            $row['pole']=$row2;
            $row['objects']=$row3;
            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row2;   2
*/
//print_r($res);
$this->SetOnline();
    $res[(error)]=0;
    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return $res;   
}  
}
/*
    INSERT INTO `mypurse`.`game_object` (`id`, `title`, `buy`, `power`, `power_kick`, `power_dig`, `life_full`, `type`, `vis`) VALUES ('18', 'Антенна', '1000', '', '', '', '', '', '1');
*/
?>