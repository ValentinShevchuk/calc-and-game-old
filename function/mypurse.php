<?
class MyPurse extends database
{
  public $mypurse_all_id_purse=0;
  public $filter=0; // Если 1 должен работать фильтр
  public $filter_date_from=0;
  public $filter_date_to=0;
  public $filter_purse=0;
  public $sql_lite=0; // упращенный sql=1
  public $ajax=0; // передается из файла ajax, значит обращение к функции из вне, значит учитываем POST или GET

  public $label_activities=Array(
      1=>array('title'=>'активность', 'score'=>'1'),
      2=>array('title'=>'запись в кошелёк', 'score'=>'5'),
      3=>array('title'=>'создание кошелька', 'score'=>'10'),
      4=>array('title'=>'заполнение анкеты', 'score'=>'15')

      );

  public function __construct(){

    if(isset($_GET['id_purse'])) $this->filter_purse=(int) $_GET['id_purse']; // если передан id_purse это значит фильтр
//echo $this->filter_purse;
//exit;
    if (isset($_GET['filter_date_from']) || isset($_GET['filter_date_to'])) // устанавливаем фильтр по дате
    {
        $this->filter=1;
        $this->filter_date_from=strtotime($_GET['filter_date_from']);
        $this->filter_date_to=strtotime($_GET['filter_date_to']);
    }
    parent :: __construct();
  }
  
private function inside_fun($arr=array(),$title=0, $hr=0, $hr_end=0)
{
        if (isset($_GET['test'])){
            echo "<br>";
            if ($hr=='1') echo "<hr>";
            if ($title!=0 || $title!='') echo "<strong>{$title}</strong><br>";
            if (is_array(empty($arr))) { echo "<pre>";print_r($arr); echo "</pre>"; } else
            echo $arr;
            if ($hr_end=='1') echo "<hr>";
        }
}
  public function mypurse_all() // выгрузка всех кошельков
  {

  $q="SELECT *, t1.title title, t2.title title_p, SUM(t2.value_credit) credit, SUM(t2.value_debet) debet, t1.id id, t1.vis vis
      FROM
          purse_name t1 LEFT JOIN purse_bill t2 on t2.id_purse=t1.id AND t2.vis>0 AND t2.status=1,
          currency_list t4,
          user t5
      WHERE t1.id_user='{$_SESSION['user_id']}' AND t1.vis>0 AND t1.currency=t4.id AND t1.id_user=t5.id GROUP by t1.id ORDER by t1.vis";

    $row=$this->getAll($q);
    foreach($row as $key=>$value)
    {
        if (empty($row[$key]['title']))  $row[$key]['title']='№'.$row[$key]['id'];
        $array=array(
            'value'=>($row[$key]['debet']-$row[$key]['credit'])
        ); 
        //exit;
        $this->update('purse_name',$array," WHERE id={$row[$key]['id']} AND id_user='{$_SESSION['user_id']}'");
    }


    return $row;
  }


  public function mypurse_ids() // выгрузка всех кошельков без инфы
  {
    $q="SELECT * FROM purse_name WHERE id_user='{$_SESSION[user_id]}' AND vis>0 ORDER by vis";
    $row=$this->getAll($q);
    foreach($row as $key=>$value)
    {
      if (empty($row[$key]['title'])) $row[$key]['title']="№".$row[$key]['id'];
    }
    return $row;
  }

  public function mypurse_one($id_purse=0) // выгрузка одного кощелька с инфой - без платежей!!!
  {
    $q="
      SELECT *,SUM(t3.value_credit) credit, SUM(t3.value_debet) debet, t1.title
      FROM
          purse_name t1,
          purse_bill t3,
          currency_list t2,
          user t4
      WHERE
        t1.id_user='{$_SESSION['user_id']}' AND
        t1.id=? AND
        t1.vis>0 AND
        t1.currency=t2.id AND
        t3.id_purse=t1.id AND
        t3.vis>0 AND
        t4.id=t1.id_user
      GROUP by t1.id";

    $row=$this->getRow($q,array($id_purse));
    return $row;
  }

  public $bill_all_only_count_records=0;
  
  public function bill_all($param=array()) // все платежи
  {
    // формируем параметры для функции
   // if (count($param)>0)
    
      /*запросы к категориям идут отдельно! так как нет смысла в одном запросе делать обращение к категорям*/
    $str_filter_purse=' AND t1.vis=1 ';
    $filtr_str='';
    
//---Эксперемент, посмотрим на сколько эффективно
  
    $page_ = isset($_GET['page']) ? intval($_GET['page']): 1;
    $limit = isset($_GET['limit'])?$_GET['limit']:10;       // set one page show 10 messages
    $offset = ($page_ >= 1) ? ($page_ - 1) * $limit : 0;
    $limit=$_GET['page']=='all'?'':"LIMIT {$offset}, {$limit}";

    //if (!isset($_GET['all']))  $filtr_str=" AND t1.start_date>=".strtotime(date("Y-m-01 00:00"))."";

    if ($this->filter==1) // фильтр по дате - переменные в конструкторе класса
    {
        $filtr_str='';
        if ($this->filter_date_from>0) $filtr_str=" AND t1.start_date>={$this->filter_date_from}";
        if ($this->filter_date_to>0)   $filtr_str.=" AND t1.start_date<={$this->filter_date_to}";
        //$limit='';
  }

   if ($this->filter_purse>0) $str_filter_purse=' AND t1.vis>0 AND t4.id='.$this->filter_purse;
   
   if ($c>0) $str_filter_purse='AND t1.vis>0 AND t4.id='.$c;
   $filter_category='';
   
   if (isset($_GET['id_category'])) // фильтр если ищется конкретная категория (нигде не используется пока)
   { $filter_category=' AND t3.id_category="'.$_GET['id_category'].'"';}
   
   
   if ($this->bill_all_only_count_records==0) // если не запрошено выдать только кол-во записей выдай все поля, иначе только кол-во
   { 
      $var='*, t1.id, t1.title, t3.title c_title, t3.id c_id, t4.title as purse_title, t4.id as purse_id'; 
    } 
    else { $limit=''; $var='COUNT(t1.id) count';} 
    
   $q="SELECT $var
          FROM
              purse_bill t1
              LEFT JOIN purse_user_category t3 on t3.id=t1.id_category AND t3.vis>0
              LEFT JOIN purse_name t4 on t4.id=t1.id_purse
              LEFT JOIN currency_list t2 ON  t4.currency=t2.id

          WHERE
            t1.id_user='{$_SESSION['user_id']}' $filtr_str $str_filter_purse $filter_category
          ORDER BY t1.start_date DESC $limit";

        $row=$this->getAll($q);
        //print_r($row);
        
         $this->bill_all_only_count_records=0; // обнуляем параметр
        
        if (isset($_POST['json']) || isset($_GET['json']) ){  

            $result =  json_encode(array('row'=>$row));
            echo "jsonpCallback(".$result.")";
        }
        else return $row;
  }

public function bill_all_json() // все платежи
  {
    $str_filter_purse=' AND t1.vis=1 ';
    $filtr_str='';
    $limit=' LIMIT 10';
   $q="SELECT t1.*, t3.title c_title, t3.id c_id, t4.title as purse_title, t4.id as purse_id
          FROM
              purse_bill t1
              LEFT JOIN purse_user_category t3 on t3.id=t1.id_category AND t3.vis>0
              LEFT JOIN purse_name t4 on t4.id=t1.id_purse
              LEFT JOIN currency_list t2 ON  t4.currency=t2.id

          WHERE
            t1.id_user='{$_SESSION['user_id']}' $filtr_str $str_filter_purse
          ORDER BY t1.start_date DESC, t1.id DESC $limit";


        //echo $q;  exit;
        $row=$this->getAll($q);
        //print_r($row);
        if (isset($_POST['json']) || isset($_GET['json']) ){  

            $result =  json_encode(array('row'=>$row));
            echo "jsonpCallback(".$result.")";
        }
        else return $row;
  }
public function billFieldEdit() // вносим изменение одной записи одного поля
{
    // передается название поля, содержимое поля, и ид записи
    if ($_POST['dataId']>0){
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd']
        );
        $this->update('purse_bill',$arr,"WHERE id=? ",array(intval($_POST['dataId'])));         
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'OK'));
            echo "jsonpCallback(".$result.")";
        }
    }
    if ($_POST['dataId']==0){
        
        if ($_POST['start_date']=='') $_POST['start_date']=time(); else $_POST['start_date']=strtotime($_POST['start_date']);
        if ($_POST['end_date']=='') $_POST['end_date']=$_POST['start_date']; else $_POST['end_date']=strtotime($_POST['end_date']);
        
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd'],
            'date_create'=>time(),
            'id_user'=>$_SESSION['user_id'],
            'start_date'=>time(),
            'end_date'=>time()
        );
        $id_bill=$this->insertdata('purse_bill',$arr);
        
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'NEW','newId'=>$id_bill));
            echo "jsonpCallback(".$result.")";
        }
    }  
}



public function billDelete($id_bill=0)
{
    // удаление платежа
    if (isset($_GET['id_bill']) || isset($_POST['id_bill'])) $id_bill=$_REQUEST['id_bill'];
    
    $array=array('vis'=>0);
    $this->Update('purse_bill',$array," WHERE id=?",array($id_bill)); 
    
    if (isset($_POST['json']) || isset($_GET['json']) ){      
        $result =  json_encode(array('res'=>'OK'));
        echo "jsonpCallback(".$result.")";
    } else return 'OK';
}

public function billFastAddRecord() // быстрое добавление записи
{

        $title=               $_POST['title'];
        $value=             $_POST['value'];        
        $credit=            $_POST['credit']; 
        $id_purse=       $_POST['id_purse'];
        
        
        if ($_POST['start_date']=='') $start_date=time(); else $start_date=strtotime($_POST['start_date']);
        if ($_POST['end_date']=='') $end_date=$_POST['start_date']; else $end_date=strtotime($_POST['end_date']);
        
        $arr=array(
            'title'=>$title,
            'value_credit'=>$credit==1?$value:0,
            'value_debet'=>$credit==1?0:$value,
            'credit'=>$credit==1?1:0,
            'date_create'=>time(),
            'id_purse'=>$id_purse,
            'id_user'=>$_SESSION['user_id'],
            'start_date'=>$start_date,
            'end_date'=>$end_date
        );
        $id_bill=$this->insertdata('purse_bill',$arr);
        
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'NEW','newId'=>$id_bill,'start_date'=>date("d.m.y",$start_date)));
            echo "jsonpCallback(".$result.")";
        }

}
public function purseFieldEdit() // вносим изменение кошелька
{
    /* передается название поля, содержимое поля, и ид записи
        
    $_POST['dataId'] --- ID записи, если 0 - создается новая запись
    $_POST['dataTdName'] --- Название поля в базе
    =>$_POST['dataTd'] --- Значение которое нужно вставить
    
   */
    if ($_POST['dataId']>0){
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd']
        );
        $this->update('purse_name',$arr,"WHERE id=? ",array(intval($_POST['dataId'])));         
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'OK'));
            echo "jsonpCallback(".$result.")";
        }
    }
    if ($_POST['dataId']==0){
        
        if ($_POST['start_date']=='') $_POST['start_date']=time(); else $_POST['start_date']=strtotime($_POST['start_date']);
        if ($_POST['end_date']=='') $_POST['end_date']=$_POST['start_date']; else $_POST['end_date']=strtotime($_POST['end_date']);
        
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd'],
            'date_create'=>time(),
            'id_user'=>$_SESSION['user_id'],
            'start_date'=>time(),
            'end_date'=>time()
        );
        $id_bill=$this->insertdata('purse_name',$arr);
        
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'NEW','newId'=>$id_bill));
            echo "jsonpCallback(".$result.")";
        }
    }  
}
public function categoryFieldEdit() // вносим изменение категории 
{
    /* передается название поля, содержимое поля, и ид записи
        
    $_POST['dataId'] --- ID записи, если 0 - создается новая запись
    $_POST['dataTdName'] --- Название поля в базе
    =>$_POST['dataTd'] --- Значение которое нужно вставить
    
   */
    if ($_POST['dataId']>0){
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd']
        );
        $this->update('purse_user_category',$arr,"WHERE id=? ",array(intval($_POST['dataId'])));         
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'OK'));
            echo "jsonpCallback(".$result.")";
        }
    }
    if ($_POST['dataId']==0){
        
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd'],
            'id_user'=>$_SESSION['user_id'],
            'vis'=>1

        );
        $id_bill=$this->insertdata('purse_user_category',$arr);
        
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'NEW','newId'=>$id_bill));
            echo "jsonpCallback(".$result.")";
        }
    }  
}
public function categoryEdit($param=array()) // вносим изменение категории 
{
    $event=$param[0]['event']; // create, edit
    
    if ($event=='create')
    {
        $arr=array(
            'title'=>$param[0]['title'],
            'descr'=>$param[0]['descr'],
            'icon'=>$param[0]['icon'],
            'color'=>$param[0]['color'],
            'id_user'=>$_SESSION['user_id'],
            'vis'=>1

        );
        $id_bill=$this->insertdata('purse_user_category',$arr);        
    }   
    
    if ($event=='save')
    {
        $arr=array(
            'title'=>$param[0]['title'],
            'descr'=>$param[0]['descr'],

        );
        $id_bill=$this->update('purse_user_category',$arr,"WHERE id='{$param[0]['id']}'");        
    }   
   /*    
    if ($_POST['dataId']>0){
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd']
        );
        $this->update('purse_user_category',$arr,"WHERE id=? ",array(intval($_POST['dataId'])));         
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'OK'));
            echo "jsonpCallback(".$result.")";
        }
    }

    if ($_POST['dataId']==0){
        
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd'],
            'id_user'=>$_SESSION['user_id'],
            'vis'=>1

        );
        $id_bill=$this->insertdata('purse_user_category',$arr);
*/       
    if ($param[0]['json']==1)  {
        $result =  json_encode( $result );
        echo "jsonpCallback(".$result.")";
    }
}
public function AnyFieldEdit() // вносим изменение категории 
{
    /* передается название поля, содержимое поля, и ид записи
        
    $_POST['dataId'] --- ID записи, если 0 - создается новая запись
    $_POST['dataTdName'] --- Название поля в базе
    =>$_POST['dataTd'] --- Значение которое нужно вставить
    
   */
   if (isset($_REQUEST['table'])){
       
   $table=$_REQUEST['table'];
   
    if ($_POST['dataId']>0){
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd']
        );
        $this->update($table,$arr,"WHERE id=? AND id_user=? ",array(intval($_POST['dataId']), $_SESSION['user_id']));         
       
        $result = array('res'=>'OK');
 
    }
    if ($_POST['dataId']==0){
        
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd'],
            'id_user'=>$_SESSION['user_id']

        );
        $id_bill=$this->insertdata($table,$arr);

            $result =  array('res'=>'NEW','newId'=>$id_bill);
    }  
    }
    else { }
    
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode( $result );
            echo "jsonpCallback(".$result.")";
        }    
    
}
public function AnyFieldEdit2($param=array()) // вносим изменение категории 
{
    /* передается название поля, содержимое поля, и ид записи
        
    $_POST['dataId'] --- ID записи, если 0 - создается новая запись
    $_POST['dataTdName'] --- Название поля в базе
    =>$_POST['dataTd'] --- Значение которое нужно вставить
    
   */
    $dataId=$param[0]['dataId']; // ID по котором будет искаться запись
    $dataTdName=$param[0]['dataTdName'];
    $dataTd=$param[0]['dataTd'];
    $table=$param[0]['table']; // таблица 
   
   
    if ($dataId>0)
    {
        $arr=array($dataTdName=> $dataTd);
        $this->update($table,$arr,"WHERE id=? AND id_user=? ",array(intval($dataId), $_SESSION['user_id']));                
        $result = array('res'=>'OK'); 
    }   
   
    
if ($param[0]['json']==1)  {
    $result =  json_encode( $result );
    echo "jsonpCallback(".$result.")";
}
    
}

public function categorySwitch() // вносим изменение кошелька
{
    /* передается название поля, содержимое поля, и ид записи
        
    $_POST['dataId'] --- ID записи, если 0 - создается новая запись
    $_POST['dataTdName'] --- Название поля в базе
    =>$_POST['dataTd'] --- Значение которое нужно вставить
    
   */
    if ($_POST['dataId']>0){
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd']
        );
        $this->update('purse_user_category',$arr,"WHERE id=? ",array(intval($_POST['dataId'])));         
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'OK'));
            echo "jsonpCallback(".$result.")";
        }
    }
    if ($_POST['dataId']==0){
        
        $arr=array(
            $_POST['dataTdName']=>$_POST['dataTd'],
            'id_user'=>$_SESSION['user_id'],
            'vis'=>1

        );
        $id_bill=$this->insertdata('purse_user_category',$arr);
        
        if (isset($_POST['json']) || isset($_GET['json']) ){  
    
            $result =  json_encode(array('res'=>'NEW','newId'=>$id_bill));
            echo "jsonpCallback(".$result.")";
        }
    }  
}

public function category_user_all($payment=0) // выгрузка используемых категорий во всех транзакциях
{
    if ($payment>0) $payment=" AND t2.id=".$payment;
    else $payment='';
    $q="SELECT  *, t3.title title,t3.id id
        FROM
            purse_bill_category t1,
            purse_bill t2,
            purse_user_category t3
        WHERE t1.id_bill=t2.id AND t2.id_user={$_SESSION['user_id']} AND t1.vis>0 AND t3.id=t1.id_category $payment";
    $row=$this->getAll($q);
    //print_r($row);
    return $row;
}

/*
  public function category_user_filter_purse() // выгрузка используемых категорий в одном кошельке
  {
     //if (!isset($_GET['all']))  $filtr_str=" AND t1.start_date>=".strtotime(date("Y-01-01 00:00"))."";
    $str_filter_purse='';
    $filtr_str='';

    if ($this->filter==1) {
        if ($this->filter_date_from>0) $filtr_str=" AND t2.start_date>={$this->filter_date_from}";
        if ($this->filter_date_to>0)   $filtr_str.=" AND t2.start_date<={$this->filter_date_to}";
  }

   if ($this->filter_purse>0) $str_filter_purse='AND t2.id='.$this->filter_purse;

  $q="SELECT  *, t3.title title,t3.id id, SUM(t2.value_credit) credits, SUM(t2.value_debet) debets
      FROM
          purse_bill_category t1,
          purse_bill t2,
          purse_user_category t3
      WHERE t1.id_bill=t2.id AND t2.id_user={$_SESSION['user_id']} AND t1.vis>0 AND t3.id=t1.id_category $str_filter_purse $filtr_str";
     echo $q;
    $row=$this->getAll($q);
    return $row;
  }*/


public function category_bill() // все  категориям с суммами за период
{
   
    $this->inside_fun($arr,'Вызвана функция:  public function category_bill()', 1, 0) ;
    
    $str_filter_purse='';
    $filtr_str='';
    if (!isset($_GET['all']))  $filtr_str=" AND t2.start_date>=".strtotime(date("Y-m-01 00:00"))."";


    if ($this->filter==1) { $filtr_str='';
        if ($this->filter_date_from>0) $filtr_str=" AND t2.start_date>={$this->filter_date_from}";
        if ($this->filter_date_to>0)   $filtr_str.=" AND t2.start_date<={$this->filter_date_to}";
  }

   if ($this->filter_purse>0) $str_filter_purse='AND t3.id='.$this->filter_purse;

   $q=" SELECT *, t1.title, t1.id, SUM(t2.value_credit) credits, SUM(t2.value_debet) debets
    FROM
        purse_user_category t1 LEFT JOIN purse_bill_category t6 on t6.id_category=t1.id AND t6.vis>0
								LEFT JOIN purse_bill t2 ON t2.id=t6.id_bill
                                LEFT JOIN purse_name t3 ON t3.id=t2.id_purse
                                LEFT JOIN currency_list t4 ON t4.id=t3.currency
    WHERE t1.id_user={$_SESSION[user_id]} AND t1.vis>0 $str_filter_purse $filtr_str  group by t1.id ORDER BY t1.title";
 
    $this->inside_fun($q,'Запрос $q', 0, 0) ;
    $row=$this->getAll($q);
    $this->inside_fun($row,'Вывод запроса', 0, 1) ;

        return $row;
  }


  public function category_stats($c=0) // все платежи без кошельков
  {
      $str_filter_purse='';
      $filtr_str='';

      //if (!isset($_GET['all']))  $filtr_str=" AND t2.start_date>=".strtotime(date("Y-M-01 00:00"))."";


      if ($this->filter==1) { $filtr_str='';
          if ($this->filter_date_from>0) $filtr_str=" AND t2.start_date>={$this->filter_date_from}";
          if ($this->filter_date_to>0)   $filtr_str.=" AND t2.start_date<={$this->filter_date_to}";
      }

      if ($this->filter_purse>0) $str_filter_purse='AND t2.id_purse='.$this->filter_purse;
      if ($c>0) $str_filter_purse='AND t2.id_purse='.$c;

      $q=" SELECT *, t1.title, t1.id, SUM(t2.value_credit) credits, SUM(t2.value_debet) debets, COUNT(t2.id) counts, t1.vis category_status
      FROM
          purse_user_category t1
          LEFT JOIN purse_bill_category t6 on t6.id_category=t1.id AND t6.vis>0
      LEFT JOIN purse_bill t2 ON t2.id=t6.id_bill
      WHERE t1.id_user={$_SESSION[user_id]} AND t1.vis>0 $str_filter_purse $filtr_str  GROUP BY t1.id ORDER BY t1.title";

      //echo $q;
      $row=$this->getAll($q);
      //print_r($row);
      return $row;
  }
  
  public function stats_bill_all() // все платежи
  {
      /*запросы к категориям идут отдельно! так как нет смысла в одном запросе делать обращение к категорям*/
    $str_filter_purse=' AND t1.vis=1 ';
    $filtr_str='';
    $limit=' LIMIT 10';
    //if (!isset($_GET['all']))  $filtr_str=" AND t1.start_date>=".strtotime(date("Y-m-01 00:00"))."";

    if ($this->filter==1) {
        $filtr_str='';
        if ($this->filter_date_from>0) $filtr_str=" AND t1.start_date>={$this->filter_date_from}";
        if ($this->filter_date_to>0)   $filtr_str.=" AND t1.start_date<={$this->filter_date_to}";
        $limit='';
    }

   if ($this->filter_purse>0) $str_filter_purse=' AND t1.vis>0 AND t4.id='.$this->filter_purse;
   if ($c>0) $str_filter_purse='AND t1.vis>0 AND t4.id='.$c;
   $filter_category='';
   if (isset($_GET['id_category'])){ $filter_category=' AND t5.id_category="'.$_GET['id_category'].'"';}

   $q="SELECT *, t1.id, t1.title, t4.title as purse_title, t4.id as purse_id
          FROM
              purse_bill t1
              LEFT JOIN purse_name t4 on t4.id=t1.id_purse
              LEFT JOIN currency_list t2 ON  t4.currency=t2.id
              LEFT JOIN purse_bill_category t5 ON t5.id_bill=t1.id
          WHERE
            t1.id_user='{$_SESSION['user_id']}' $filtr_str $str_filter_purse $filter_category
          ORDER BY t1.start_date , t1.id DESC $limit";
    //echo $q; exit;
    if ($this->sql_lite==1)
       $q="SELECT *, t1.id, t1.title,  t4.title as purse_title, t4.id as purse_id, t1.id_category
          FROM
              purse_bill t1

              LEFT JOIN purse_name t4 on t4.id=t1.id_purse
              LEFT JOIN currency_list t2 ON  t4.currency=t2.id

          WHERE t1.id_user='{$_SESSION['user_id']}'  $str_filter_purse  $filtr_str  $filter_category ORDER BY t1.start_date";

        //echo $q;  exit;
        $row=$this->getAll($q);
        //print_r($row);
        if (isset($_POST['json']) || isset($_GET['json']) ){  

            $result =  json_encode(array('row'=>$row));
            echo "jsonpCallback(".$result.")";
        }
        else return $row;
  }

  public function user_start($user=0) // Создание стартовых категорий для нового пользователя
  {
   if($user>0){ // нужно добавить короткие имена, исправить функцию статистика она грузит левые значения !!! это не ошибка, нужно восстановить таблицу с категориями
      $q="INSERT INTO purse_user_category (`id`, `title`, `descr`, `icon`, `color`, `id_user`, `vis`) VALUES
      (NULL, 'Автомобиль','личный транспорт (топливо, обслуживание)', 'fa-truck', '#E79C9C', $user, '1'),
      (NULL, 'Бизнес, проекты','', 'fa-briefcase', '#FFC69C', $user, '1'),
      (NULL, 'Бытовая техника','', 'fa-desktop', '#FFE79C', $user, '1'),
      (NULL, 'Гипермаркет','(продукты, товары для дома)', 'fa-shopping-cart', '#B5D6A5', $user, '1'),
      (NULL, 'Долги, кредиты','', 'fa-credit-card', '#A5C6CE', $user, '1'),
      (NULL, 'Дом','(квартира, дача)', 'fa-home', '#9CC6EF', $user, '1'),
      (NULL, 'Домашние животные','', 'fa-twitter', '#B5A5D6', $user, '1'),
      (NULL, 'Ипотека','', 'fa-home', '#D6A5BD', $user, '1'),
      (NULL, 'Кафе и рестораны','', 'fa-cutlery', '#630000', $user, '1'),
      (NULL, 'Коммунальные платежи','', 'fa-tint', '#7B3900', $user, '1'),
      (NULL, 'Красота и здоровье','', 'fa-eye', '#846300', $user, '1'),
      (NULL, 'Медицина, аптека','', 'fa-stethoscope', '#295218', $user, '1'),
      (NULL, 'Мобильная связь','(интернет, ТВ, телефон)', 'fa-phone', '#083139', $user, '1'),
      (NULL, 'Образование','(ВУЗы, курсы, семинары)', 'fa-flask', '#003163', $user, '1'),
      (NULL, 'Общественный транспорт','', 'fa-truck', '#21104A', $user, '1'),
      (NULL, 'Одежда и обувь','', 'fa-shopping-cart', '#4A1031', $user, '1'),
      (NULL, 'Путешествия','(Отпуск, путешествия, дальние поездки)', 'fa-plane', '#E76363', $user, '1'),
      (NULL, 'Досуг','ПО, книги и журналы, игры, музыка, фильмы', 'fa-book', '#E79439', $user, '1') ,
      (NULL, 'Развлечения и праздники',' (кино, театр, выходные, дни рождения)', 'fa-ticket', '#EFC631', $user, '1'),
      (NULL, 'Семья и дети','', 'fa-users', '#6BA54A', $user, '1'),
      (NULL, 'Строительство и ремонт','', 'fa-wrench', '#4A7B8C', $user, '1'),
      (NULL, 'Финансовые операции ','(накопления, депозиты, инвестиции, эл. деньги)', 'fa-btc', '#3984C6', $user, '1'),
      (NULL, 'Хобби и увлечения','', 'fa-coffee', '#634AA5', $user, '1'),
      (NULL, 'Штрафы, налоги, комиссии','', 'fa-rub', '#A54A7B', $user, '1'),
      (NULL, 'Я не помню','', 'fa-frown-o', '#94BD7B', $user, '1')";
      //echo $q; exit;

      $this->db->query($q);

      $q="INSERT INTO `purse_name` (`id`, `title`, `id_user`, `currency`, `vis`) VALUES
      (NULL, 'Наличка', $user, '1',  '1')";
      $this->db->query($q);

      }
  }

  public function user_level_set($val=1) // изменяем значение левела
  {
      $val=(int)$val;
      $q=" SELECT * FROM user WHERE id='{$_SESSION['user_id']}'";
      //echo $q;
      $row=$this->getRow($q);
      if ($row['level_count']>=$row['level_max'])

          $array=array(
              'level_max'=>round($row['level_max']*1.5),
              'level_count'=>0,
              'level'=>$row['level']+1
              );
      else
          $array=array(
              'level_count'=>$row['level_count']+$this->label_activities[$val]['score']
              );

      $this->Update('user',$array," WHERE id='{$_SESSION['user_id']}'");

      $arr=array(
              'id_user'=>$_SESSION['user_id'],
              'score'=>$this->label_activities[$val]['score'],
              'type_score'=>$val,
              'text_score'=>'+'.$this->label_activities[$val]['score'].' за '.$this->label_activities[$val]['title'],
              'datetime_int'=>time(),
              'datetime_str'=>date('Y-m-d H:i:s'),
              'vis'=>1
              );
     // print_r($arr);
             $this->insertdata('user_bonus_stats',$arr);
  }
  public function user_level_get() // ставим значение
  {
      $val=(int)$val;
      $q=" SELECT * FROM user WHERE id='{$_SESSION['user_id']}'";
      //echo $q;
      $row=$this->getRow($q);
      $row['level_percent']=round($row[level_count]/$row[level_max]*100);
      return $row;
  }
  public function set_category_bill_ajax()  // для передачи из ajax не городить условия с post
  {
    $this->set_category_bill($_POST['bill_id'],$_POST['cat_id'],$_POST['vis'],1);
  }

  public function set_category_bill($bill=0,$cat=0,$vis=1, $html=0){
      // функция для выставления категорий по одной штуке

    $q="SELECT vis FROM purse_bill_category WHERE id_bill='{$bill}' AND id_category='{$cat}'"; // узнаем есть ли такая категория
    $row=$this->getOne($q);

    if ($row=='') { // если нет категории добавляем
      $arr=array(
        'id_bill'=>$bill,
        'id_category'=>$cat,
        'vis'=>1
      );
      $this->insertdata('purse_bill_category',$arr);
    } else {$this->update('purse_bill_category',array('vis'=>$vis),"WHERE id_bill='{$bill}' AND  id_category='{$cat} '");  // если есть, но удаленна возможно, восстанавливаем
     }

    if ($html>0){ // html - выгружаем новый список категорий
      $row=$this->category_user_all($bill);
echo "<span> <a href='#' data-toggle='modal' data-target='.modal-flip-vertical' class='record_tags'><i class='fa  fa-tags'></i></a>";
      foreach($row as $row)
      {
        if ($html==1) echo " <a href='#'  class='text-aqua btn-xs'>{$row['title']}</a>, ";
      }
     if ($html==1) echo "</span>";
    }

  }

public function toUnloadTheExposedCategories($id_bill=0)// выгружаем выбранные категории для платежа
{
    
    
    if (isset($_POST))  $id_bill=$_POST['id_bill'];
    
    $q="SELECT *, t1.id, IF (t2.id_category is NULL, 0, 1) as selected FROM purse_user_category t1 INNER JOIN purse_bill_category t2 ON t2.id_bill='{$id_bill}' AND t2.id_category=t1.id AND t2.vis>0 WHERE t1.id_user='{$_SESSION[user_id]}' AND t1.vis>0 ORDER by t1.title";
    //echo $q;
    $row=$this->db->getAll($q);

    
    if (isset($_POST['json']) || isset($_GET['json']) ){  
        $result =  json_encode(array('row'=>$row));
        echo "jsonpCallback(".$result.")";
    } 
    else return $row;
    
    
}

public function regularPaymentsGetAll(){
    // выгружаем регулярные платежи
    
    if (isset($_POST))  $id_bill=$_POST['id_bill'];
    
    $q="
        SELECT t1.*, t2.start_date start_date_last
        FROM 
            purse_bill_regular t1 
            LEFT JOIN purse_bill t2 ON t2.regular_payment=t1.id AND t2.vis>0 
        WHERE t1.id_user='{$_SESSION[user_id]}' AND t1.vis>0 GROUP BY t2.regular_payment
        ORDER by t2.start_date DESC, t2.title";
    //echo $q;
    $row=$this->db->getAll($q);

    
    if (isset($_POST['json']) || isset($_GET['json']) ){  
        $result =  json_encode(array('row'=>$row));
        echo "jsonpCallback(".$result.")";
    } 
    else return $row;
    
    
}
public function regularPaymentsPay(){
    // выгружаем регулярные платежи - выставить платить
    
    if (isset($_POST))  $id_bill=$_POST['id_bill'];
    //$arr=array('status'=>1);
    //$this->update('purse_bill',$arr,"WHERE id=? ",array(intval($_POST['id_bill'])));      
    
    $this->regularPaymentsInvoice($_POST['id_bill_regular']);   
    
    if (isset($_POST['json']) || isset($_GET['json']) ){  
        $result =  json_encode(array('res'=>'ok'));
        echo "jsonpCallback(".$result.")";
    } 
    else return $row;
    
    
}
public function regularPaymentsDelete($id_bill=0)
{
    // удаление платежа
    if (isset($_GET['id_bill']) || isset($_POST['id_bill'])) $id_bill=$_REQUEST['id_bill'];
    
    $array=array('vis'=>0);
    $this->Update('purse_bill_regular',$array," WHERE id=?",array($id_bill)); 
    
    if (isset($_POST['json']) || isset($_GET['json']) ){      
        $result =  json_encode(array('res'=>'OK'));
        echo "jsonpCallback(".$result.")";
    } else return 'OK';
}


public function regularPaymentsInvoice($id_bill_regular=0){
    // выставляем счет на будущее
  
    if (isset($_POST['id_bill_regular']))  $id_bill_regular=$_POST['id_bill_regular'];
    
    $q="
        SELECT t1.*, t4.title purse_title
        FROM 
            purse_bill_regular t1
            LEFT JOIN purse_name t4 on t4.id=t1.id_purse
            LEFT JOIN currency_list t2 ON  t4.currency=t2.id 
        
        WHERE t1.id_user=? AND t1.vis>0 AND t1.id=?";
    //echo $q;
    $row=$this->db->getRow($q,array($_SESSION[user_id],$id_bill_regular));
    
// при создании руглярного платежа создается будущий платеж +1 day, +1 week 1 month  
        if ($row['regularity']==1) $start_date=strtotime("+ 1 day",time());
        if ($row['regularity']==2) $start_date=strtotime("+ 1 week",time());
        if ($row['regularity']==3) $start_date=strtotime("+ 1 month",time());
        if ($row['regularity']==4) $start_date=strtotime("+ 6 month",time());
        if ($row['regularity']==5) $start_date=strtotime("+ 1 year",time());
        $start_date=time();
            
        $array=array(
            'title'=>$row['title'],
            'date_create'=>time(),
            'id_user'=>$_SESSION['user_id'],
            'to_user'=>0,//$_POST['to_user'],
            'value_credit'=>$row['value_credit'],
            'value_debet'=>$row['value_debet'],
            'credit'=>$row['credit'],
            'status'=>1,
            'start_date'=>$start_date,
            'end_date'=>$start_date,
            'id_purse'=>$row['id_purse'],
            'id_category'=>$row['id_category'],
            'regular_payment'=>$id_bill_regular
            
        );
    
        $id_bill=$this->insertdata('purse_bill',$array);
        $row['id']=$id_bill;
        $row['start_date']=date('d.m.y',time());
      // $arr=array('status'=>1);
     //  $this->update('purse_bill',$arr,"WHERE id=? ",array(intval($_POST['id_bill'])));      
    
    if (isset($_POST['json']) || isset($_GET['json']) ){  
        $result =  json_encode($row);
        echo "jsonpCallback(".$result.")";
    } 
    else return $row;
    
    
}


public function PlanSave($id_plan=0,$id_user=0){
    
    // запись планирования, записываются все записи
     
   $id_plan=isset($_REQUEST['id_plan'])?$_REQUEST['id_plan']:$id_plan;
   $id_user=isset($_SESSION['user_id'])?$_SESSION['user_id']:$id_user;
   
   // удаляем предыдущие записи плана
    $arr=array('vis'=>0);
    $this->update('planning',$arr,"WHERE id_plan=? AND id_user=? ",array($id_plan,$id_user));  
   
 
   // формируем строки для записи
    $str='';
    foreach($_REQUEST['array'] as $row){
        $str.="(?,?,?,?,?,?,?)";
      //  $str.="('{$row['title']}','{$row['debet']}','{$row['credit']}','{$_SESSION['user_id']}','{$id_plan}')";
        $placeholder[]=$row['title'];
        $placeholder[]=$row['debet'];
        $placeholder[]=$row['credit'];
        $placeholder[]=$id_user;
        $placeholder[]=$id_plan;
        $placeholder[]=$row['month'];
        $placeholder[]=$row['year'];
        
       if (next($_REQUEST['array'])) $str.=',';
    }
    
     $q="INSERT INTO `planning` (title, debet, credit, id_user, id_plan, month, year) VALUES   $str "; 
     $this->query($q,$placeholder);
     
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return $row; 
        else {

            $result =  json_encode(print_r($placeholder));
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row;   
}
public function PlanLoad($id_plan=0,$id_user=0){
    
    // запись планирования, записываются все записи
     
   $id_plan=isset($_REQUEST['id_plan'])?$_REQUEST['id_plan']:$id_plan;
   $id_user=isset($_SESSION['user_id'])?$_SESSION['user_id']:$id_user;
 
    $q="SELECT * FROM planning WHERE id_user=? AND id_plan=? AND vis=1 ORDER by year, month";  
    $row=$this->getAll($q,array($id_user, $id_plan));
      
    if (isset($_POST['json']) || isset($_GET['json']) ){
        if ($inside==1)  return $row; 
        else {

            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        }
    } 
    else return $row;   
}



}
?>